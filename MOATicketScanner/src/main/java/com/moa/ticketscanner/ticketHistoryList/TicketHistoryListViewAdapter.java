package com.moa.ticketscanner.ticketHistoryList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.moa.ticketscanner.R;
import com.moa.ticketscanner.ui.TicketHistoryActivity;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TicketHistoryListViewAdapter extends BaseExpandableListAdapter {
	private Context _context;
	private List<TicketHistoryListHeader> _listDataHeader; // header titles
	// child data in format of header title, child title
	private HashMap<TicketHistoryListHeader, Integer> _childData;
	private Set<Integer> refundedUsages = new HashSet<Integer>();

	public TicketHistoryListViewAdapter(Context context, List<TicketHistoryListHeader> listDataHeader,
			HashMap<TicketHistoryListHeader, Integer> childData) {
		this._context = context;
		this._listDataHeader = listDataHeader;
		this._childData = childData;
	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return this._childData.get(this._listDataHeader.get(groupPosition));
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView,
			ViewGroup parent) {
		final int pointsToRefund = (Integer) getChild(groupPosition, 0);
		final TicketHistoryListHeader header = (TicketHistoryListHeader) getGroup(groupPosition);
		ViewHolder holder = new ViewHolder();

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.partial_ticket_history_list_view_item, null);
			holder.refundButton = (Button) convertView.findViewById(R.id.ticketHistoryRefundBtn);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.refundButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				refundedUsages.add(header.getUsageId());
				v.setEnabled(false);
				TicketHistoryActivity activity = (TicketHistoryActivity) _context;
				activity.refundPoints(header, pointsToRefund, (Button)v);
			}
		});
		
		if(refundedUsages.contains(header.getUsageId())) {
			holder.refundButton.setText("REFUNDED");
			holder.refundButton.setEnabled(false);
		} else {
			holder.refundButton.setText("REFUND " + pointsToRefund + " POINTS?");
			holder.refundButton.setEnabled(true);
		}
		
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return 1;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return this._listDataHeader.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return this._listDataHeader.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		TicketHistoryListHeader rideHeader = (TicketHistoryListHeader) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.partial_ticket_history_list_view_group, null);
		}
		
		convertView.setTag(rideHeader.isRefundable());
		if(rideHeader.isRefundable()) {
			convertView.findViewById(R.id.ticketHistoryArrow).setVisibility(View.VISIBLE);
		} else {
			convertView.findViewById(R.id.ticketHistoryArrow).setVisibility(View.INVISIBLE);
		}

		TextView rideNameView = (TextView) convertView.findViewById(R.id.ticketHistoryListViewGroupRideName);
		rideNameView.setText(rideHeader.getRideName());

		TextView rideDateTimeView = (TextView) convertView.findViewById(R.id.ticketHistoryListViewGroupRideDateTime);
		rideDateTimeView.setText(rideHeader.getRideDateTime());
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		
		return true;
	}
	
	private static class ViewHolder {
		Button refundButton;
	}
}