package com.moa.ticketscanner.ticketHistoryList;

public class TicketHistoryListHeader {

	private String rideName;
	private String rideDateTime;
	private int usageId;
	private boolean isRefundable;
	
	public TicketHistoryListHeader(String rideName, String rideDateTime, boolean isRefundable, int usageId) {
		this.rideName = rideName;
		this.rideDateTime = rideDateTime;
		this.isRefundable = isRefundable;
		this.usageId = usageId;
	}

	public String getRideName() {
		return rideName;
	}

	public String getRideDateTime() {
		return rideDateTime;
	}
	
	public int getUsageId() {
		return usageId;
	}

	public boolean isRefundable() {
		return isRefundable;
	}
}
