package com.moa.ticketscanner.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;

import com.moa.ticketscanner.R;
import com.moa.ticketscanner.communication.BaseMoaRequestListener;
import com.moa.ticketscanner.communication.MoaApiRequestDetails;
import com.moa.ticketscanner.communication.MoaApiSpiceRequest;
import com.moa.ticketscanner.domain.OfflineScan;
import com.moa.ticketscanner.dto.offline.OfflineScanDto;
import com.moa.ticketscanner.dto.offline.OfflineScansDto;
import com.moa.ticketscanner.settings.AppSettings;
import com.moa.ticketscanner.ui.MoaBaseActivity;
import com.octo.android.robospice.persistence.exception.SpiceException;

import org.springframework.http.HttpMethod;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class OfflineDataService {
	private MoaBaseActivity initiatingActivity;
	
	public OfflineDataService(MoaBaseActivity initiatingActivity) {
		this.initiatingActivity = initiatingActivity;
	}
	
	public void submitOfflineScans(List<OfflineScan> scans) {
		OfflineScansDto dto = new OfflineScansDto();
		List<OfflineScanDto> scanList = new ArrayList<OfflineScanDto>();
		for(OfflineScan scan : scans) {
			scanList.add(buildDto(scan));
		}
		dto.setScans(scanList);
		
		String uri = AppSettings.OFFLINE_UPLOAD_URI;
		MoaApiRequestDetails requestDetails = new MoaApiRequestDetails();
		requestDetails.setHttpMethod(HttpMethod.POST);
		requestDetails.setUri(uri);
		requestDetails.setRequestBody(dto);
		MoaApiSpiceRequest<Object> request = new MoaApiSpiceRequest<Object>(Object.class, requestDetails);
		initiatingActivity.getSpiceManager().execute(request, new OfflineUploadRequestListener(initiatingActivity));
	}
	
	private OfflineScanDto buildDto(OfflineScan scan) {
		OfflineScanDto dto = new OfflineScanDto();
		
		dto.setScanDate(scan.getScanDate());
		dto.setScanId(scan.getScanId());
		dto.setScanOperator(scan.getScanOperator());
		dto.setVisualId(scan.getVisualId());
		
		return dto;
	}
	
	private class OfflineUploadRequestListener extends BaseMoaRequestListener<Object> {
		public OfflineUploadRequestListener(MoaBaseActivity initiatingActivity) {
			super(initiatingActivity);
		}

		@Override
		public void handleSuccess(Object result) {
			 Notification n = new Notification.Builder(initiatingActivity)
			 								.setContentTitle("Upload Complete")
			 								.setContentText("Offline Data Successfully Uploaded.")
			 								.setSmallIcon(R.drawable.ic_launcher)
			 								.setAutoCancel(true)
			 								.build();
			
			 NotificationManager notificationManager =
					 (NotificationManager) initiatingActivity.getSystemService(Context.NOTIFICATION_SERVICE);
			 notificationManager.notify(0, n);
			 
			 File offlineModeFileToDelete = new File(initiatingActivity.getFilesDir().getPath() + "/" +
						 								AppSettings.OFFLINE_MODE_FILE_NAME);
			 offlineModeFileToDelete.delete();
		}
		
		@Override
		public void onRequestFailure(SpiceException spiceException) {
			Notification n = new Notification.Builder(initiatingActivity)
				.setContentTitle("Upload Failed")
				.setContentText("Offline Data Failed To Upload.")
				.setSmallIcon(R.drawable.ic_launcher)
				.setAutoCancel(true)
				.build();

			NotificationManager notificationManager =
					(NotificationManager) initiatingActivity.getSystemService(Context.NOTIFICATION_SERVICE);
			notificationManager.notify(0, n);
		}
	}
}
