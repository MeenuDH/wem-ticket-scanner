package com.moa.ticketscanner.dto.configuration;

import org.codehaus.jackson.annotate.JsonProperty;
import com.moa.ticketscanner.dto.tickets.FacilityDto;
import java.util.ArrayList;
import java.util.List;
public class PassPhotoDto {
	@JsonProperty("VisualID")
	private String visualId;
	@JsonProperty("AccessCode")
	private String accessCode;
	@JsonProperty("PhotoUrl")
	private String photoUrl;
	@JsonProperty("HasPhoto")
	private String hasPhoto;
	@JsonProperty("NeedsPhoto")
	private String needsPhoto;
	@JsonProperty("Error")
	private String error;
	@JsonProperty("ErrorMessage")
	private String errormessage;
	@JsonProperty("Facilities")
	private List<FacilityDto> facilities = new ArrayList<FacilityDto>();
	public PassPhotoDto() {
		// Default Constructor
	}

	public PassPhotoDto(String visualId, String accessCode, String photoUrl, String hasPhoto, String needsPhoto, String error, String errorMessage) {
		this.visualId = visualId;
		this.accessCode = accessCode;
		this.photoUrl = photoUrl;
		this.hasPhoto = hasPhoto;
		this.needsPhoto = needsPhoto;
		this.error = error;
		this.errormessage = errorMessage;
	}
	

	public String getVisualId() {
		return visualId;
	}
	public String getAccessCode() {
		return accessCode;
	}
	public String getPhotoUrl() {
		return photoUrl;
	}
	public String getHasPhoto() {
		return hasPhoto;
	}
	public String getNeedsPhoto() {
		return needsPhoto;
	}
	public String getError() {
		return error;
	}
	public String getErrorMessage() {
		return errormessage;
	}
	public List<FacilityDto> getFacilities(){
		return this.facilities;
	}
}
