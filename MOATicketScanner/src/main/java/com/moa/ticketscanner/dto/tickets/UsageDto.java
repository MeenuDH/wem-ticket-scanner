package com.moa.ticketscanner.dto.tickets;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.Comparator;
import java.util.Date;

@Root(name="Usage", strict=false)
public class UsageDto {
	@Element(name = "UsageID", required = false)
	private int usageId;
	
	@Element(name="UseTime", required=false)
	private Date useTime;
	
	@Element(name="AccessCode", required=false)
	private String accessCode;
	
	@Element(name="IDNo", required=false)
	private String idNo;
	
	@Element(name="Code", required=false)
	private int code;
	
	@Element(name="ACP", required=false)
	private int acp;
	
	@Path("Status")
	@Element(name="Status", required=false)
	private int status; // TODO: SH - Need to break this out into an object
	
	@Element(name="Qty", required=false)
	private Integer quantity;
	
	@Element(name="UseNo", required=false)
	private Integer useNo;
	
	@Element(name="Operator", required=false)
	private String operator;
	
	@Element(name="EntryMethod", required=false)
	private String entryMethod;
	
	@Element(name="SerialNo", required=false)
	private String serialNo;
	
	@Element(name="Override", required=false)
	private String override; // TODO: SH - Need to break this out into an object
	
	@Element(name="UsageCondition", required=false)
	private String usageCondition;
	
	@Element(name="BankNo", required=false)
	private String bankNo;
	
	@Element(name="BiometricStatus", required=false)
	private String biometricStatus;
	
	@Element(name="OperationID", required=false)
	private int operationID;
	
	@Element(name="Points", required=false)
	private int points;

	public int getUsageId() {
		return usageId;
	}
	
	public Date getUseTime() {
		return useTime;
	}

	public String getAccessCode() {
		return accessCode;
	}

	public String getIdNo() {
		return idNo;
	}

	public int getCode() {
		return code;
	}
	
	public void setCode(int code) {
		this.code = code;
	}

	public int getAcp() {
		return acp;
	}

	public int getStatus() {
		return status;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public Integer getUseNo() {
		return useNo;
	}

	public String getOperator() {
		return operator;
	}

	public String getEntryMethod() {
		return entryMethod;
	}

	public void setEntryMethod(String entryMethod) {
		this.entryMethod = entryMethod;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public String getOverride() {
		return override;
	}

	public String getUsageCondition() {
		return usageCondition;
	}

	public String getBankNo() {
		return bankNo;
	}

	public String getBiometricStatus() {
		return biometricStatus;
	}

	public int getOperationID() {
		return operationID;
	}
	
	public int getPonits() {
		return points;
	}

	public static class UsageComparator implements Comparator<UsageDto> {  
		@Override
		public int compare(UsageDto lhs, UsageDto rhs) {
			return rhs.getUseTime().compareTo(lhs.getUseTime());
		}
	}

}
