package com.moa.ticketscanner.dto.tickets;
import org.simpleframework.xml.Path;


public class ReverseUsageRequestDto {
	public static final String MESSAGE_TYPE = "ReverseUsage";
	
	@Path("ReverseUsage")
	private int UsageID;
	
	public int getUsageID() {
		return UsageID;
	}
	
	public void setUsageID(int usageId) {
		UsageID = usageId;
	}

}
