package com.moa.ticketscanner.dto.logon;

import org.simpleframework.xml.Path;

public class LogonRequestDto {
	public static final String MESSAGE_TYPE = "Logon";
	
	@Path("Logon")
	private String Username;
	@Path("Logon")
	private String Password;
	@Path("Logon")
	private int Facility;
	@Path("Logon/Operations")
	private int Operation;
	@Path("Logon")
	private int ACP;
	
	public String getUsername() {
		return Username;
	}
	public void setUsername(String username) {
		Username = username;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public int getFacility() {
		return Facility;
	}
	public void setFacility(int facility) {
		Facility = facility;
	}
	public int getOperation() {
		return Operation;
	}
	public void setOperation(int operation) {
		Operation = operation;
	}
	public int getACP() {
		return ACP;
	}
	public void setACP(int aCP) {
		ACP = aCP;
	}
}
