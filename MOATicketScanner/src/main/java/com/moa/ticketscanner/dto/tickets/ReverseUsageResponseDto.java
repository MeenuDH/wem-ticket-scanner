package com.moa.ticketscanner.dto.tickets;

import com.moa.ticketscanner.dto.BaseGalaxyResponseDto;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

@Root(name="Envelope", strict = false)
public class ReverseUsageResponseDto extends BaseGalaxyResponseDto {
	
	public static final String ERRORCODE_ALREADYREVERSED = "403";
	public static final String ERRORCODE_INVALIDUSAGEID = "402";
	
	
	@Path("Body/ReverseUsageResponse/Errors/Error")
	@Element(name="ErrorCode", required=false)
	private String errorCode;
	
	@Path("Body/ReverseUsageResponse/Errors/Error")
	@Element(name="ErrorText", required=false)
	private String errorText;
	
	@Path("Body/ReverseUsageResponse/Status")
	@Element(name = "Status")
	private int reversalStatus;;
	
	@Override
	public String getErrorCode() {
		return errorCode;
	}
	@Override
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;		
	}
	@Override
	public String getErrorText() {
		return errorText;
	}
	@Override
	public void setErrorText(String errorText) {
		this.errorText = errorText;
	}
	public int getReversalStatus() {
		return reversalStatus;
	}
	
}
