package com.moa.ticketscanner.dto.tickets;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;
@JsonIgnoreProperties(ignoreUnknown = true)
public class SuperTicketDto {
	@JsonProperty("VisualId")
	private String visualId;
	@JsonProperty("SubTickets")
	private List<SubTicketDto> subTickets = new ArrayList<SubTicketDto>();
	public String getVisualId() {
		return this.visualId;
	}
	public List<SubTicketDto> getSubTickets(){
		return this.subTickets;
	}

}
