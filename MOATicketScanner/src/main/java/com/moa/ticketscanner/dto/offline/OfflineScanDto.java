package com.moa.ticketscanner.dto.offline;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OfflineScanDto {
	@JsonProperty("ScanId")
	private String scanId;
	@JsonProperty("ScanDate")
	private Date scanDate;
	@JsonProperty("ScanOperator")
	private String scanOperator;
	@JsonProperty("VisualId")
	private String visualId;
	
	public String getScanId() {
		return scanId;
	}
	public void setScanId(String scanId) {
		this.scanId = scanId;
	}
	public Date getScanDate() {
		return scanDate;
	}
	public void setScanDate(Date scanDate) {
		this.scanDate = scanDate;
	}
	public String getScanOperator() {
		return scanOperator;
	}
	public void setScanOperator(String scanOperator) {
		this.scanOperator = scanOperator;
	}
	public String getVisualId() {
		return visualId;
	}
	public void setVisualId(String visualId) {
		this.visualId = visualId;
	}
}
