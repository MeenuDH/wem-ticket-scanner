package com.moa.ticketscanner.dto.tickets;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeclinedTicketDto {
	@JsonProperty("RideId")
	private String rideId;
	@JsonProperty("VisualId")
	private String visualId;
	@JsonProperty("Reason")
	private String reason;
	@JsonProperty("UserId")
	private String userId;
	
	public String getRideId() {
		return rideId;
	}
	public void setRideId(String rideId) {
		this.rideId = rideId;
	}
	public String getVisualId() {
		return visualId;
	}
	public void setVisualId(String visualId) {
		this.visualId = visualId;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
}
