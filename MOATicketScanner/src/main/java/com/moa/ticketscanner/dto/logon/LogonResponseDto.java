package com.moa.ticketscanner.dto.logon;

import com.moa.ticketscanner.dto.BaseGalaxyResponseDto;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;

public class LogonResponseDto extends BaseGalaxyResponseDto{
	@Path("Body/LogonResponse")
	@Element(name="Username", required=false)
	private String Username;
	@Path("Body/LogonResponse")
	@Element(name="Facility", required=false)
	private String Facility;
	@Path("Body/LogonResponse")
	@Element(name="ACP", required=false)
	private String ACP;
	@Path("Body/LogonResponse/Operations")
	@Element(name="Operation", required=false)
	private String operation;
	@Path("Body/LogonResponse/Errors/Error")
	@Element(name="ErrorCode", required=false)
	private String errorCode;
	@Path("Body/LogonResponse/Errors/Error")
	@Element(name="ErrorText", required=false)
	private String errorText;
	
	@Override
	public String getErrorCode() {
		return errorCode;
	}
	@Override
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;		
	}
	@Override
	public String getErrorText() {
		return errorText;
	}
	@Override
	public void setErrorText(String errorText) {
		this.errorText = errorText;
	}

}
	