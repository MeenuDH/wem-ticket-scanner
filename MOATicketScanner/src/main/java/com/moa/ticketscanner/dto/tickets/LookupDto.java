package com.moa.ticketscanner.dto.tickets;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

@Root(name = "Lookup", strict = false)
public class LookupDto {
	@Element(name = "VisualID", required = false)
	private String visualID;
	@Element(name = "Ticket", required = false)
	private TicketDto ticket;
	@Element(name = "DebitCard", required = false)
	private DebitCardDto debitCard;
	@ElementList(name = "Usages", required = false)
	private ArrayList<UsageDto> usages;

	public ArrayList<UsageDto> getUsages() {
		return usages;
	}

	public String getVisualID() {
		return visualID;
	}

	public TicketDto getTicket() {
		return ticket;
	}
	
	public DebitCardDto getDebitCard() {
		return debitCard;
	}
	
	public Collection<UsageDto> getFilteredUsages(List<Integer> rideAcpIds) {
		ArrayList<UsageDto> clonedUsages = (ArrayList<UsageDto>) usages.clone();
		Collections.sort(clonedUsages, new UsageDto.UsageComparator());
		LinkedHashMap<Date, UsageDto> filteredUsages = new LinkedHashMap<Date, UsageDto>();
		Calendar today = Calendar.getInstance();
		for(UsageDto usage : clonedUsages) {
			Date usageDate = usage.getUseTime();
			
			if((usageDate.getYear() + 1900) == today.get(Calendar.YEAR) &&
				usageDate.getMonth() == today.get(Calendar.MONTH) &&
				usageDate.getDate() == today.get(Calendar.DAY_OF_MONTH) &&
				rideAcpIds.contains(usage.getAcp())) {
				if(filteredUsages.containsKey(usageDate) && 
						(filteredUsages.get(usageDate).getCode() == 11 || 
						 usage.getCode() == 11)) {
					filteredUsages.remove(usageDate);
				} else if(usage.getCode() == 0 && usage.getStatus() == 0) {
					filteredUsages.put(usageDate, usage);
				}
			}
		}
		
		return filteredUsages.values();
	}
	
	public Collection<UsageDto> getFilteredReversals(List<Integer> rideAcpIds) {
		ArrayList<UsageDto> reversals = new ArrayList<UsageDto>();
		Calendar today = Calendar.getInstance();
		for(UsageDto usage : usages) {
			Date usageDate = usage.getUseTime();
			
			if((usageDate.getYear() + 1900) == today.get(Calendar.YEAR) &&
				usageDate.getMonth() == today.get(Calendar.MONTH) &&
				usageDate.getDate() == today.get(Calendar.DAY_OF_MONTH) &&
				rideAcpIds.contains(usage.getAcp()) &&
				usage.getCode() == 11) {
				reversals.add(usage);
			}
		}
		Collections.sort(reversals, new UsageDto.UsageComparator());
		return reversals;
	}
	
	public Collection<UsageDto> getAdminFilteredReversals() {
		ArrayList<UsageDto> reversals = new ArrayList<UsageDto>();
		for(UsageDto usage : usages) {
			if(usage.getCode() == 11) {
				reversals.add(usage);
			}
		}
		Collections.sort(reversals, new UsageDto.UsageComparator());
		return reversals;
	}
	
	public Collection<UsageDto> getAdminFilteredUsages() {
		ArrayList<UsageDto> clonedUsages = (ArrayList<UsageDto>) usages.clone();
		Collections.sort(clonedUsages, new UsageDto.UsageComparator());
		LinkedHashMap<Date, UsageDto> filteredUsages = new LinkedHashMap<Date, UsageDto>();
		for(UsageDto usage : clonedUsages) {
			Date usageDate = usage.getUseTime();
			
			if(filteredUsages.containsKey(usageDate) && 
					(filteredUsages.get(usageDate).getCode() == 11 || 
					 usage.getCode() == 11)) {
				filteredUsages.remove(usageDate);
			} else if(usage.getCode() == 0 && usage.getStatus() == 0) {
				filteredUsages.put(usageDate, usage);
			}
			
		}
		
		return filteredUsages.values();
	}
}