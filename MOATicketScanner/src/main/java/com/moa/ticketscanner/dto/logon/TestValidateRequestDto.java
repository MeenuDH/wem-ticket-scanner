package com.moa.ticketscanner.dto.logon;

import com.moa.ticketscanner.dto.tickets.TicketValidationRequestDto;

public class TestValidateRequestDto extends TicketValidationRequestDto{
	public static final String MESSAGE_TYPE = "TestValidate";
	
	public TestValidateRequestDto(String visualId) {
		super(visualId);
	}
}
