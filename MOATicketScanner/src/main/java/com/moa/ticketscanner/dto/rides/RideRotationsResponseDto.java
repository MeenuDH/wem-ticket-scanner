package com.moa.ticketscanner.dto.rides;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RideRotationsResponseDto {
	@JsonProperty("Rotations")
	private List<RideRotationDto> rotations;

	public List<RideRotationDto> getRotations() {
		return rotations;
	}

	public void setRotations(List<RideRotationDto> rotations) {
		this.rotations = rotations;
	}
}
