package com.moa.ticketscanner.dto.rides;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RideRotationDto {
	@JsonProperty("Id")
	private int id;
	@JsonProperty("DisplayIndex")
	private int displayIndex;
	@JsonProperty("Name")
	private String name;
	@JsonProperty("Rides")
	private List<RideDto> rides;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getDisplayIndex() {
		return displayIndex;
	}
	public void setDisplayIndex(int displayIndex) {
		this.displayIndex = displayIndex;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<RideDto> getRides() {
		return rides;
	}
	public void setRides(List<RideDto> rides) {
		this.rides = rides;
	}
}
