package com.moa.ticketscanner.dto.configuration;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by vvemula on 4/6/2016.
 */
public class NetworkStatusDto {
    @JsonProperty("status")
    private String status;

    public NetworkStatusDto() {
        // Default Constructor
    }

    public NetworkStatusDto(String status) {
        this.status = status;

    }


    public String getNetworkStatus() {
        return status;
    }

}
