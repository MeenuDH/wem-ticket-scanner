package com.moa.ticketscanner.dto;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="Envelope")
public class BaseGalaxyRequestDto<T> {

	@Element(name="Header")
	private HeaderDto header;
	@Element(name="Body", required=false)
	private T body;
	
	public BaseGalaxyRequestDto(HeaderDto requestHeader, T requestBody) {
		header = requestHeader;
		body = requestBody;
	}
}
