package com.moa.ticketscanner.dto.tickets;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;

public class TicketValidationRequestDto {
	private static final String YES_VALUE = "Yes";
	private static final String NO_VALUE = "No";
	
	public static final String MESSAGE_TYPE = "Validate";
	
	@Path("Validate")
	@Element(name = "VisualID")
	private String visualId;
	
	@Path("Validate")
	@Element(name = "OverrideInsufficientPointsBalance")
	private String override = NO_VALUE;
	
	public TicketValidationRequestDto(String visualId) {
		this.visualId = visualId;
	}
	
	public String getVisualId() {
		return visualId;
	}

	public boolean isOverride() {
		return NO_VALUE.equals(override);
	}

	public void setOverride(boolean override) {
		this.override = override ? YES_VALUE : NO_VALUE;
	}
}
