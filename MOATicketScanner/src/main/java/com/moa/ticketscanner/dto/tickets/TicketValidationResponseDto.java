package com.moa.ticketscanner.dto.tickets;

import com.moa.ticketscanner.dto.BaseGalaxyResponseDto;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

@Root(name="Envelope", strict=false)
public class TicketValidationResponseDto extends BaseGalaxyResponseDto {
	
	@Path("Body/ValidateResponse/ValidationResponse/ValidationStatus")
	@Element(name = "Status", required = true)
	private int ticketStatus;

	@Path("Body/ValidateResponse/ValidationResponse/ValidationStatus")
	@Element(name = "StatusText", required = true)
	private String ticketStatusText;

	@Path("Body/ValidateResponse/ValidationResponse")
	@Element(name = "PointsRemaining", required = false)
	private int pointsRemaining;
	
	@Path("Body/ValidateResponse/ValidationResponse")
	@Element(name = "SoundFile", required = false)
	private String soundFile;
	
	@Path("Body/ValidateResponse/ValidationResponse")
	@Element(name = "UsesLeftValid", required = false)
	private String usesLeftValid;
	
	@Path("Body/ValidateResponse/ValidationResponse")
	@Element(name = "UsesLeft", required = false)
	private int usesLeft;
	
	@Path("Body/ValidateResponse/ValidationResponse")
	@Element(name = "ValidityInfo", required = false)
	private String validityInfo;
	
	@Path("Body/ValidateResponse/Errors/Error")
	@Element(name="ErrorCode", required=false)
	private String errorCode;
	
	@Path("Body/ValidateResponse/Errors/Error")
	@Element(name="ErrorText", required=false)
	private String errorText;
	
	@Path("Body/ValidateResponse/ValidationResponse/AccessCode")
	@Element(name = "Code", required = false)
	private int accessCode;

	//Vivek
	@Path("Body/ValidateResponse/ValidationResponse")
	@Element(name = "Birthday", required = false)
	private String BirthDay;
	@Path("Body/ValidateResponse/ValidationResponse")
	@Element(name = "FirstName", required = false)
	private String FirstName;

	public int getAccessCode() {
		return accessCode;
	}

	public void setAccessCode(int accessCode) {
		this.accessCode = accessCode;
	}

	public int getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(int status) {
		this.ticketStatus = status;
	}

	public String getTicketStatusText() {
		return ticketStatusText;
	}

	public void setTicketStatusText(String statusText) {
		this.ticketStatusText = statusText;
	}

	public int getPointsRemaining() {
		return pointsRemaining;
	}
	
	public String getSoundFile() {
		return soundFile;
	}

	public void setPointsRemaining(int pointsRemaining) {
		this.pointsRemaining = pointsRemaining;
	}
	
	@Override
	public String getErrorCode() {
		return errorCode;
	}
	
	@Override
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;		
	}
	
	@Override
	public String getErrorText() {
		return errorText;
	}
	
	@Override
	public void setErrorText(String errorText) {
		this.errorText = errorText;
	}

	public boolean isTicketValid() {
		return ticketStatus == 0;
	}
	
	public boolean isUsesLeftValid() {
		return usesLeftValid.equalsIgnoreCase("YES");
	}

	public int getUsesLeft() {
		return usesLeft;
	}

	public String getValidityInfo() {
		return validityInfo;
	}
	//Vivek
	public boolean isBirthday() {
		return BirthDay.equalsIgnoreCase("YES");
	}
	public String getFirstName() {
		return FirstName;
	}
}
