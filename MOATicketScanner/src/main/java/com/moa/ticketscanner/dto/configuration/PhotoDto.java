package com.moa.ticketscanner.dto.configuration;

import com.google.gson.annotations.SerializedName;

import org.codehaus.jackson.annotate.JsonProperty;

public class PhotoDto {
	@JsonProperty("VisualID")
	@SerializedName("VisualID")
	private String visualId;
	@JsonProperty("AccessCode")
	@SerializedName("AccessCode")
	private String accessCode;
	@JsonProperty("PhotoUrl")
	@SerializedName("PhotoUrl")
	private String photoUrl;
	@JsonProperty("HasPhoto")
	@SerializedName("HasPhoto")
	private String hasPhoto;
	@JsonProperty("NeedsPhoto")
	@SerializedName("NeedsPhoto")
	private String needsPhoto;
	public String getVisualId() {
		return visualId;
	}
	public String getAccessCode() {
		return accessCode;
	}
	public String getPhotoUrl() {
		return photoUrl;
	}
	public String getHasPhoto() {
		return hasPhoto;
	}
	public String getVNeedsPhoto() {
		return needsPhoto;
	}
}
