package com.moa.ticketscanner.dto.tickets;

import org.joda.time.DateTime;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="Ticket", strict=false)
public class TicketDto {
	@Element(name="VisualID")
	private String visualId;
	@Element(name="RemainingValue")
	private float remainingValue;
	@Element(name="Expiration", required=false)
    private DateTime expiration;

	public String getVisualId() {
		return visualId;
	}
	
	public float getRemainingValue() {
		return remainingValue;
	}
    
    public DateTime getExpiration() {
    	return expiration;
    }
}
