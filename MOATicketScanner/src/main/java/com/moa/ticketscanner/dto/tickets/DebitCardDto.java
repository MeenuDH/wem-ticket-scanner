package com.moa.ticketscanner.dto.tickets;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "DebitCard", strict = false)
public class DebitCardDto {
	@Element(name = "PointsBalance", required = false)
	private int pointsBalance;
	
	public int getPointsBalance() {
		return pointsBalance;
	}
}
