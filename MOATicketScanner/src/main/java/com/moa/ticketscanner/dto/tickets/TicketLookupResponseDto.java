package com.moa.ticketscanner.dto.tickets;

import com.moa.ticketscanner.dto.BaseGalaxyResponseDto;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

@Root(name="Envelope", strict=false)
public class TicketLookupResponseDto extends BaseGalaxyResponseDto {

	@Path("Body/TicketLookupResponse")
	@ElementList(name="Lookups", required=false)
    private ArrayList<LookupDto> lookups;
	
	@Path("Body/TicketLookupResponse/Errors/Error")
	@Element(name="ErrorCode", required=false)
	private String errorCode;
	@Path("Body/TicketLookupResponse/Errors/Error")
	@Element(name="ErrorText", required=false)
	private String errorText;
	
	@Override
	public String getErrorCode() {
		return errorCode;
	}
	@Override
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;		
	}
	@Override
	public String getErrorText() {
		return errorText;
	}
	@Override
	public void setErrorText(String errorText) {
		this.errorText = errorText;
	}

    public ArrayList<LookupDto> getLookups() {
        return lookups;
    }
    
    public void setLookups(ArrayList<LookupDto> lookups) {
		this.lookups = lookups;
	}
}
