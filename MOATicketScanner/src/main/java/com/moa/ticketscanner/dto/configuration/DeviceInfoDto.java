package com.moa.ticketscanner.dto.configuration;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Date;
/**
 * Created by vvemula on 10/4/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeviceInfoDto {
    @JsonProperty("DeviceMacId")
    private String deviceMacId;
    @JsonProperty("RideId")
    private String rideId;
    @JsonProperty("UserId")
    private String userId;
    public String getDeviceMacId() {
        return deviceMacId;
    }
    public void setDeviceMacId(String deviceMacIdParam) {
        this.deviceMacId = deviceMacIdParam;
    }
    public String getRideId() {
        return rideId;
    }
    public void setRideId(String rideId) {
        this.rideId = rideId;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
}
