package com.moa.ticketscanner.dto.tickets;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.joda.time.DateTime;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubTicketDto {
	@JsonProperty("VisualID")
	private String visualId;
	@JsonProperty("PLU")
	private String type;

	public String getVisualId() {
		return this.visualId;
	}
	public String getType() {
		return this.type;
	}
}
