package com.moa.ticketscanner.dto.tickets;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;


public class TicketLookupRequestDto {
	public static final String MESSAGE_TYPE = "TicketLookup";
	
	@Path("TicketLookup")
	@Element(name="VisualID")
	private String visualId;
	
	public String getVisualID() {
		return visualId;
	}
	
	public void setVisualID(String visualID) {
		visualId = visualID;
	}

}
