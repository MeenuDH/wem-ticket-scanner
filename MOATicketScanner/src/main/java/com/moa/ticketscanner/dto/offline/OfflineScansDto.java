package com.moa.ticketscanner.dto.offline;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OfflineScansDto {
	@JsonProperty("Scans")
	private List<OfflineScanDto> scans;

	public List<OfflineScanDto> getScans() {
		return scans;
	}

	public void setScans(List<OfflineScanDto> scans) {
		this.scans = scans;
	}
}
