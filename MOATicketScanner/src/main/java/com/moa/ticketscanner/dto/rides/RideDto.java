package com.moa.ticketscanner.dto.rides;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RideDto {
	@JsonProperty("Id")
	private int id;
	@JsonProperty("ShortName")
	private String name;
	@JsonProperty("FacilityId")
	private int facilityId;
	@JsonProperty("PrimaryAcpId")
	private int primaryAcpId;
	@JsonProperty("SecondaryAcpId")
	private int secondaryAcpId;
	@JsonProperty("TertiaryAcpId")
	private int tertiaryAcpId;
	//Vivek
	@JsonProperty("OfflineAcpId")
	private int offlineAcpId;
	@JsonProperty("OperationId")
	private int operationId;
	@JsonProperty("ScanId")
	private String scanId;
	@JsonProperty("RideRules")
	private String rideRules;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getFacilityId() {
		return facilityId;
	}
	public void setFacilityId(int facilityId) {
		this.facilityId = facilityId;
	}
	public int getPrimaryAcpId() {
		return primaryAcpId;
	}
	public void setPrimaryAcpId(int acpId) {
		this.primaryAcpId = acpId;
	}
	public int getOperationId() {
		return operationId;
	}
	public void setOperationId(int operationId) {
		this.operationId = operationId;
	}
	public int getSecondaryAcpId() {
		return secondaryAcpId;
	}
	public void setSecondaryAcpId(int secondaryAcpId) {
		this.secondaryAcpId = secondaryAcpId;
	}
	public int getTertiaryAcpId() {
		return tertiaryAcpId;
	}
	public void setTertiaryAcpId(int tertiaryAcpId) {
		this.tertiaryAcpId = tertiaryAcpId;
	}
	//Vivek
	public int getOfflineAcpId() {
		return offlineAcpId;
	}
	public void setOfflineAcpId(int offlineAcpId) {
		this.offlineAcpId = offlineAcpId;
	}
	public String getScanId() {
		return scanId;
	}
	public void setScanId(String scanId) {
		this.scanId = scanId;
	}
	public String getRideRules() {
		return rideRules;
	}
	public void setRideRules(String rideRules) {
		this.rideRules = rideRules;
	}
}
