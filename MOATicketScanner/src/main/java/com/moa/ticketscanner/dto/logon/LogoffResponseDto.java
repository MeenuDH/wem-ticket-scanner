package com.moa.ticketscanner.dto.logon;

import com.moa.ticketscanner.dto.BaseGalaxyResponseDto;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;


public class LogoffResponseDto extends BaseGalaxyResponseDto {
	@Path("Body")
	@Element(name="LogoffResponse", required=false)
	private String response;
	
//	@Path("Body/LogoffResponse/Errors/Error")
//	@Element(name="ErrorCode", required=false)
	private String errorCode;
	
//	@Path("Body/LogoffResponse/Errors/Error")
//	@Element(name="ErrorText", required=false)
	private String errorText;
	
	@Override
	public String getErrorCode() {
		return errorCode;
	}
	
	@Override
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;		
	}
	
	@Override
	public String getErrorText() {
		return errorText;
	}
	
	@Override
	public void setErrorText(String errorText) {
		this.errorText = errorText;
	}
}
