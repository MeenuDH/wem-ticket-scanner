package com.moa.ticketscanner.dto.configuration;

import org.codehaus.jackson.annotate.JsonProperty;

public class TicketStatusCodeDto {
	@JsonProperty("Code")
	private int code;
	@JsonProperty("DisplayText")
	private String displayText;
	@JsonProperty("CanOverride")
	private boolean canOverride;
	
	public TicketStatusCodeDto() {
		// Default Constructor
	}
	
	public TicketStatusCodeDto(String displayText, boolean canOverride) {
		this.displayText = displayText;
		this.canOverride = canOverride;
	}
	
	public String getDisplayText() {
		return displayText;
	}
	public void setDisplayText(String displayText) {
		this.displayText = displayText;
	}
	public boolean isCanOverride() {
		return canOverride;
	}
	public void setCanOverride(boolean canOverride) {
		this.canOverride = canOverride;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	
}
