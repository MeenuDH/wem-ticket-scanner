package com.moa.ticketscanner.dto;

import org.apache.commons.lang3.StringUtils;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

@Root(name="Envelope", strict=false)
public abstract class BaseGalaxyResponseDto {

	@Element(name="Header")
	private HeaderDto header;
	
	@Path("Body/Status")
	@Element(name="StatusCode", required=false)
	private String statusCode;
	@Path("Body/Status")
	@Element(name="StatusText", required=false)
	private String statusText;

	public HeaderDto getHeader() {
		return header;
	}

	public void setHeader(HeaderDto header) {
		this.header = header;
	}
	
	public abstract String getErrorCode();
	
	public abstract void setErrorCode(String errorCode);

	public abstract String getErrorText();

	public abstract void setErrorText(String errorText);

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getTicketStatusText() {
		return statusText;
	}

	public void setTicketStatusText(String statusText) {
		this.statusText = statusText;
	}

	@Override
	public String toString()
	{
		return String.format("SessionID: %s", header.getSessionID());
	}
	
	public boolean hasErrors() {
		return StringUtils.isNotEmpty(getErrorCode());
	}
	
	public boolean hasInvalidStatus() {
		return StringUtils.isNotEmpty(statusCode) && !statusCode.equalsIgnoreCase("0");
	}
}
