package com.moa.ticketscanner.dto.tickets;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.joda.time.DateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FacilityDto {
	@JsonProperty("FacilityId")
	private int facilityId;

	@JsonProperty("Name")
	private String name;
	@JsonProperty("Description")
	private String description;
	public int getFacilityId() {
		return this.facilityId;
	}
	public String getName() {
		return this.name;
	}
	public String getDescription() {
		return this.description;
	}
}
