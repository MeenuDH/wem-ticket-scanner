package com.moa.ticketscanner.dto.configuration;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class TicketStatusCodesDto {
	@JsonProperty("StatusCodes")
	private List<TicketStatusCodeDto> statusCodes;

	public List<TicketStatusCodeDto> getStatusCodes() {
		return statusCodes;
	}

	public void setStatusCodes(List<TicketStatusCodeDto> statusCodes) {
		this.statusCodes = statusCodes;
	}
	
}
