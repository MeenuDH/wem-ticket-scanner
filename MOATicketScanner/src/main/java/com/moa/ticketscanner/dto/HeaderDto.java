package com.moa.ticketscanner.dto;

import android.content.Context;
import android.provider.Settings.Secure;

import org.simpleframework.xml.Element;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class HeaderDto {
	private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

	@Element(name="SourceID")
	private String SourceID;
	@Element(name="MessageID")
	private int MessageID;
	@Element(name="MessageType")
	private String MessageType;
	@Element(name="TimeStamp")
	private String TimeStamp;
	@Element(name="EchoData", required=false)
	private String EchoData;
	@Element(name="SystemFields", required=false)
	private String SystemFields;
	@Element(name="SessionID", required=false)
	private String SessionID;
	
	public String getSourceId() {
		return SourceID;
	}
	public void setSourceId(String sourceId) {
		this.SourceID = sourceId;
	}
	public int getMessageId() {
		return MessageID;
	}
	public void setMessageId(int messageId) {
		this.MessageID = messageId;
	}
	public String getMessageType() {
		return MessageType;
	}
	public void setMessageType(String messageType) {
		this.MessageType = messageType;
	}
	public String getTimeStamp() {
		return TimeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.TimeStamp = timeStamp;
	}
	
	public String getSessionID() {
		return SessionID;
	}
	public void setSessionID(String SessionID) {
		this.SessionID = SessionID;
	}

	public static HeaderDto Create(Context context, String messageType) {
		HeaderDto headerDto = new HeaderDto();
		headerDto.setSourceId(Secure.getString(context.getContentResolver(), Secure.ANDROID_ID));
		headerDto.setMessageId(0);
		headerDto.setMessageType(messageType);
		headerDto.setTimeStamp(dateFormatter.format(new Date()));
		
		return headerDto;
	}

	
}
