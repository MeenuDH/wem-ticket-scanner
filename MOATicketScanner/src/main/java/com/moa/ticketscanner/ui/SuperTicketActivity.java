package com.moa.ticketscanner.ui;




import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;



import com.moa.ticketscanner.R;
import com.moa.ticketscanner.communication.MoaApiRequestDetails;
import com.moa.ticketscanner.communication.MoaApiSpiceRequest;


import com.moa.ticketscanner.dto.tickets.SubTicketDto;
import com.moa.ticketscanner.dto.tickets.SuperTicketDto;
import com.moa.ticketscanner.settings.AppSettings;
import com.moa.ticketscanner.settings.RuntimeSettings;

import com.octo.android.robospice.SpiceManager;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import org.springframework.http.HttpMethod;


import java.util.List;


public class SuperTicketActivity extends MoaBaseActivity {

    SpiceManager spiceManager;
    SharedPreferences mPrefs;
    RuntimeSettings runtimeSettings;
    List<SubTicketDto> subTicketsList;
    public LinearLayout layout;
    public LinearLayout.LayoutParams p;
    public RadioGroup radioGroup;
    public Button btnOk;
    public Button btnCancel;
    private String visualId = "";
    public String selectedVisualId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subtickets);
        runtimeSettings = RuntimeSettings.getInstance();
        spiceManager = super.getSpiceManager();

        mPrefs =  getSharedPreferences(AppSettings.APPLICATION_NAME, MODE_PRIVATE);

        visualId = getIntent().getStringExtra("visualId");
        if(runtimeSettings.isOnline())
        {
            retrieveSuperTicket();

        }
        else
        {
            goBacktoTicketHistory(2);
        }


    }
    public void onClick(View view) {
        try {
            String s = ((RadioButton) view).getText().toString();
            Toast.makeText(SuperTicketActivity.this, "This is: " + s,
                    Toast.LENGTH_LONG).show();
        }
        catch (Exception e1) {
            e1.printStackTrace();
        }
    }
    public void addOkButton() {

        //radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);
        btnOk = (Button) findViewById(R.id.btnOk);

        btnOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                RadioButton selectRadio = (RadioButton) findViewById(radioGroup
                        .getCheckedRadioButtonId());
                String type = selectRadio.getText().toString();

                for(SubTicketDto subticket: subTicketsList)
                {
                    if(subticket.getType() == type)
                    {
                        selectedVisualId = subticket.getVisualId();
                        runtimeSettings.setSelectedSubTicket(subticket.getVisualId());
                    }
                }
                goBacktoTicketHistory(1);
            }

        });

    }
    public void addCancelButton() {

        //radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);
        btnCancel = (Button) findViewById(R.id.btnCancel);

        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                goBacktoTicketScanning();
                //finish();

            }

        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.splash_screen, menu);
        return true;
    }


    private void retrieveSuperTicket(){
        MoaApiRequestDetails requestDetails = new MoaApiRequestDetails();
        //String visualIdTest = "029000001";
        requestDetails.setHttpMethod(HttpMethod.GET);
        //requestDetails.setUri(AppSettings.TEST_API_DOMAIN+AppSettings.GET_SUB_TICKETS + visualId.substring(1,3));
        //House Keeping Task requestDetails.setUri(AppSettings.TEST_API_DOMAIN+AppSettings.GET_SUB_TICKETS + visualId);
        requestDetails.setUri(AppSettings.GET_SUB_TICKETS + visualId);
       MoaApiSpiceRequest<SuperTicketDto> request = new MoaApiSpiceRequest<SuperTicketDto>(
               SuperTicketDto.class, requestDetails);
//        TestApiSpiceRequest<SuperTicketDto> request = new TestApiSpiceRequest<SuperTicketDto>(
//                SuperTicketDto.class, requestDetails);

        spiceManager.execute(request,new SuperTicketRequestListener());


    }

    private class SuperTicketRequestListener implements
            RequestListener<SuperTicketDto> {

        @Override
        public void onRequestSuccess(SuperTicketDto result) {

            layout = (LinearLayout) findViewById(R.id.layout);
            radioGroup = new RadioGroup(context);
            p = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.FILL_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT

            );
            layout.addView(radioGroup, p);
            subTicketsList = result.getSubTickets();
            for(SubTicketDto subticket : subTicketsList)
            {
                Log.d("SubTicket Type", subticket.getType());
                RadioButton radioButtonView = new RadioButton(context);
                radioButtonView.setText(subticket.getType());
                radioButtonView.setTextSize(20.0f);
                //radioButtonView.setOnClickListener(mThisButtonListener);
                radioGroup.addView(radioButtonView, p);
            }
            if(radioGroup.getChildCount() > 0)
            {
                RadioButton rButton = (RadioButton) radioGroup.getChildAt(0);
                rButton.setChecked(true);
            }
            addOkButton();
            addCancelButton();

        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            goBacktoTicketHistory(2);
        }


    }
    private View.OnClickListener mThisButtonListener = new View.OnClickListener() {
        public void onClick(View v) {


           /* String s = ((RadioButton) v).getText().toString();

            for(SubTicketDto subticket: subTicketsList)
            {
                if(subticket.getType() == s)
                {
                    selectedVisualId = subticket.getVisualId();
                    runtimeSettings.setSelectedSubTicket(subticket.getVisualId());
                }
            }
            Toast.makeText(SuperTicketActivity.this, "Hello from " + selectedVisualId,
                    Toast.LENGTH_LONG).show();
            goBacktoTicketHistory(1);*/

        }
    };
    public void goBacktoTicketHistory(int resultCode)
    {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("SubTicketVisualId", selectedVisualId);
        setResult(resultCode, returnIntent);
        finish();
    }
    public void goBacktoTicketScanning()
    {
        Intent intent = new Intent(this, TicketScanningActivity.class);
//		intent.putExtra("visualId",ticketScanInput.getText().toString());
        startActivityForResult(intent, 500);
        finish();
    }

}
