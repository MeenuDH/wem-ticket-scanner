package com.moa.ticketscanner.ui;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import com.google.gson.Gson;
import com.moa.ticketscanner.R;
import com.moa.ticketscanner.communication.BaseMoaRequestListener;
import com.moa.ticketscanner.communication.MoaApiRequestDetails;
import com.moa.ticketscanner.communication.MoaApiSpiceRequest;
import com.moa.ticketscanner.dto.configuration.TicketStatusCodeDto;
import com.moa.ticketscanner.dto.configuration.TicketStatusCodesDto;
import com.moa.ticketscanner.dto.rides.RideRotationsResponseDto;
import com.moa.ticketscanner.settings.AppSettings;
import com.moa.ticketscanner.settings.RuntimeSettings;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.HttpClientErrorException;


public class SplashScreenActivity extends MoaBaseActivity {

	SpiceManager spiceManager;

	Boolean rideListComplete = false;
	Boolean ticketListComplete = false;
	SharedPreferences mPrefs;
	RuntimeSettings runtimeSettings;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);
		runtimeSettings = RuntimeSettings.getInstance();
		spiceManager = super.getSpiceManager();
		//Vivek VPN Check

		retrieveRides();
		retrieveStatusCodes();
		mPrefs = getSharedPreferences(AppSettings.APPLICATION_NAME, MODE_PRIVATE);
		if(!runtimeSettings.isServerReachable())
		{
			Log.d("VPN Check Splash","Network Error! Try again later!");
			//super.showErrorDialogAndTransfer("VPN Check","Network Error! Try again later!",LoginActivity.class);
			super.showErrorDialog("VPN Check Splash","Network Error! Try again later!");
			checkGoToLogin();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash_screen, menu);
		return true;
	}
	@Override
	public void onResume(){
		super.onResume();

	}
	private void retrieveRides() {MoaApiRequestDetails requestDetails = new MoaApiRequestDetails();
	requestDetails.setHttpMethod(HttpMethod.GET);
	requestDetails.setUri(AppSettings.RIDES_BY_ROTATION_URI);
		MoaApiSpiceRequest<RideRotationsResponseDto> request = new MoaApiSpiceRequest<RideRotationsResponseDto>(
				RideRotationsResponseDto.class,
				requestDetails);
		spiceManager.execute(request, new RideRetrievalListener(this));
	}

	private void retrieveStatusCodes() {
		MoaApiRequestDetails requestDetails = new MoaApiRequestDetails();
		requestDetails.setHttpMethod(HttpMethod.GET);
		requestDetails.setUri(AppSettings.STATUS_CODE_URI);
		MoaApiSpiceRequest<TicketStatusCodesDto> request = new MoaApiSpiceRequest<TicketStatusCodesDto>(
				TicketStatusCodesDto.class, requestDetails);
		spiceManager.execute(request, new StatusCodeRequestListener());
	}

	public void checkGoToLogin() {
		if (ticketListComplete && rideListComplete) {
			Intent intent = new Intent(getApplicationContext(),
					LoginActivity.class);
			startActivity(intent);
			finish();
		}
	}

	public void cacheRideData() {
		String json = new Gson().toJson(runtimeSettings.getRideRotations());

		if (AppSettings.debug) {
			Log.d("WEM", "Ride Names: " + json);
		}

		// save data to disk
		Editor cache = mPrefs.edit();
		cache.putString(AppSettings.RIDE_NAME_CACHE, json);
		cache.commit();
	}

	public void cacheCodesData() {
		String json = new Gson().toJson(runtimeSettings.getTicketStatusCodes());

		if (AppSettings.debug) {
			Log.d("WEM", "Ticket Status Codes: " + json);
		}

		// save data to disk
		Editor cache = mPrefs.edit();
		cache.putString(AppSettings.TICKET_STATUS_CODE_CACHE, json);
		cache.commit();
	}
//Vivek. Check VPN Changed
	/*private class RideRetrievalListener implements
			RequestListener<RideRotationsResponseDto> {

		@Override
		public void onRequestSuccess(RideRotationsResponseDto result) {
			// hold rides in memory
			runtimeSettings.setRideRotations(result.getRotations());
			// save to device
			cacheRideData();
			rideListComplete = true;
			//Vivek VPN Check
			Log.d("GetRidesList","Connection Successful");
			checkGoToLogin();
		}

		@Override
		public void onRequestFailure(SpiceException spiceException) {
			rideListComplete = true;
			//Vivek VPN Check
			Log.d("GetStatusCodes","Network Error");
			//ShowAlert( "Network Error! Try again Later");
			checkGoToLogin();
		}

	}*/
	private class RideRetrievalListener extends BaseMoaRequestListener<RideRotationsResponseDto> {
		public RideRetrievalListener(MoaBaseActivity initiatingActivity){
			super(initiatingActivity);
		}
		@Override
		public void handleSuccess(RideRotationsResponseDto result) {
			// hold rides in memory
			runtimeSettings.setRideRotations(result.getRotations());
			runtimeSettings.setServerReachable(true);
			// save to device
			cacheRideData();
			rideListComplete = true;
			//Vivek VPN Check
			Log.d("GetRidesList","Connection Successful");
			checkGoToLogin();
		}

		@Override
		public void onRequestFailure(SpiceException spiceException) {
			rideListComplete = true;
			//Vivek VPN Check
			runtimeSettings.setServerReachable(false);
			Log.d("GetRidesList","Network Error");
			Log.d("GetRidesList",spiceException.getMessage());
			//ShowAlert( "Network Error! Try again Later");
			showCommunicationError();
			checkGoToLogin();
		}
		@Override
		protected void handleResourceNotFoundError(HttpClientErrorException exception){
			rideListComplete = true;
			runtimeSettings.setServerReachable(false);
			Log.d("GetRidesList","Network Error");
			Log.d("GetRidesList",exception.getMessage());
			showCommunicationError();
			checkGoToLogin();
		}
		@Override
		protected void showCommunicationError() {
			ShowAlert("Network Error! Restart the Ticket Scanner App later!");
		}

	}
	private class StatusCodeRequestListener implements
			RequestListener<TicketStatusCodesDto> {
		@Override
		public void onRequestSuccess(TicketStatusCodesDto result) {
			runtimeSettings.setTicketStatusCodes(result.getStatusCodes());
			runtimeSettings.setServerReachable(true);
			cacheCodesData();
			ticketListComplete = true;
			//Vivek VPN Check
			Log.d("GetStatusCodes","Connection Successful");
			checkGoToLogin();
		}

		@Override
		public void onRequestFailure(SpiceException spiceException) {
			ticketListComplete = true;
			//Vivek VPN Check
			runtimeSettings.setServerReachable(false);
			Log.d("GetStatusCodes","Network Error");
			//ShowAlert( "An error occurred calling the service");
			//checkGoToLogin();
		}
	}
	private void ShowAlert(String test)
	{
		AlertDialog alertDialog = new AlertDialog.Builder(SplashScreenActivity.this).create();
		alertDialog.setTitle("Communication Error");
		alertDialog.setMessage(test);
		alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						//checkGoToLogin();
						dialog.dismiss();
						finish();
					}
				});
		alertDialog.show();
	}

}
