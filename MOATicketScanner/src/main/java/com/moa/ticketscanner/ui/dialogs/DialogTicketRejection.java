package com.moa.ticketscanner.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;

import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

import android.widget.TextView;

import com.moa.ticketscanner.R;

import org.apache.commons.lang3.StringUtils;

public class DialogTicketRejection extends Dialog implements OnClickListener {

	private Activity activity;
	private String message;
	private String subMessage;
	private boolean canOverride;

	private Button overrideButton;
	private Button dismissButton;
	private Dialog rejectTicketDialog;
	private android.view.View.OnClickListener overrideListener;
	private android.view.View.OnClickListener dismissListener;

	public DialogTicketRejection(Activity activity, String message, String subMessage, boolean canOverride) {
		super(activity);
		this.activity = activity;
		this.message = message;
		this.subMessage = subMessage;
		this.canOverride = canOverride;

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_ticket_rejected);
		dismissButton = (Button) findViewById(R.id.dialogRejectDismiss);
		overrideButton = (Button) findViewById(R.id.dialogRejectOverride);
		overrideButton.setVisibility(canOverride ? View.VISIBLE : View.GONE);
		TextView errorText = (TextView) findViewById(R.id.rejectedReasonText);
		errorText.setText(message);

		TextView subText = (TextView) findViewById(R.id.subMessageTextView);
		if(StringUtils.isNotBlank(subMessage)) {
			subText.setText(subMessage);
			subText.setVisibility(View.VISIBLE);
		} else {
			subText.setVisibility(View.GONE);
		}

		dismissButton.setOnClickListener(dismissListener == null ? this : dismissListener);
		overrideButton.setOnClickListener(overrideListener == null ? this : overrideListener);

		// play sound
		MediaPlayer mp;
		mp = MediaPlayer.create(activity, R.raw.invalid);
		mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
		mp.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				mp.release();
			}

		});
		mp.start();

	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_VOLUME_DOWN || keyCode == KeyEvent.KEYCODE_VOLUME_MUTE) {
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	
	public void setOverrideListener(android.view.View.OnClickListener listener) {
		overrideListener = listener;
	}
	
	public void setDismissListener(android.view.View.OnClickListener listener) {
		dismissListener = listener;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.dialogRejectDismiss:
			this.dismiss();
			
			break;
		case R.id.dialogRejectOverride:
			this.dismiss();
			break;
		}

	}

}