package com.moa.ticketscanner.ui.widgets;

public class ScanInputParameters {
	private int background;
	private int hint;
	private int textAppearence;
	private int height;
	private int width;
	private int hintTextColor;
	private boolean alignRight = false;
	
	public ScanInputParameters(int background, int hint, int textAppearence,
			int height, int width, int hintTextColor, boolean alignRight) {
		this.background = background;
		this.hint = hint;
		this.textAppearence = textAppearence;
		this.height = height;
		this.width = width;
		this.hintTextColor = hintTextColor;
		this.alignRight = alignRight;
	}
	
	public int getBackground() {
		return background;
	}
	public void setBackground(int background) {
		this.background = background;
	}
	public int getHint() {
		return hint;
	}
	public void setHint(int hint) {
		this.hint = hint;
	}
	public int getTextAppearence() {
		return textAppearence;
	}
	public void setTextAppearence(int textAppearence) {
		this.textAppearence = textAppearence;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}

	public int getHintTextColor() {
		return hintTextColor;
	}

	public void setHintTextColor(int hintTextColor) {
		this.hintTextColor = hintTextColor;
	}

	public boolean isAlignRight() {
		return alignRight;
	}

	public void setAlignRight(boolean alignRight) {
		this.alignRight = alignRight;
	}
}
