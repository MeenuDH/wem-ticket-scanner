package com.moa.ticketscanner.ui;




import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;


import com.google.gson.Gson;
import com.moa.ticketscanner.R;
import com.moa.ticketscanner.communication.BaseMoaRequestListener;
import com.moa.ticketscanner.communication.MoaApiRequestDetails;
import com.moa.ticketscanner.communication.MoaApiSpiceRequest;

import com.moa.ticketscanner.dto.configuration.PassPhotoDto;
import com.moa.ticketscanner.settings.AppSettings;
import com.moa.ticketscanner.settings.RuntimeSettings;

import com.octo.android.robospice.SpiceManager;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.HttpClientErrorException;

import java.io.IOException;


public class PassPhotoDataActivity extends MoaBaseActivity {

	SpiceManager spiceManager;
	SpiceManager spiceManager1;
	Boolean passPhotoComplete = false;
	SharedPreferences mPrefs;
	RuntimeSettings runtimeSettings;
	private String visualId = "";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pass_photo_data_screen);
		runtimeSettings = RuntimeSettings.getInstance();
		spiceManager = super.getSpiceManager();

		mPrefs =  getSharedPreferences(AppSettings.APPLICATION_NAME, MODE_PRIVATE);
		visualId = getIntent().getStringExtra("visualId");
		if(runtimeSettings.isOnline())
		{
			retrievePassPhotos();
		}
		else
		{
			goBacktoTicketScanning(2);
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash_screen, menu);
		return true;
	}


	private void retrievePassPhotos(){
		MoaApiRequestDetails requestDetails = new MoaApiRequestDetails();
		//String visualIdTest = "029000001";
		requestDetails.setHttpMethod(HttpMethod.GET);
		requestDetails.setUri(AppSettings.CHECK_PHOTO_URI + visualId);
		//requestDetails.setUri(AppSettings.CHECK_PHOTO_URI + visualIdTest);
		MoaApiSpiceRequest<PassPhotoDto> request = new MoaApiSpiceRequest<PassPhotoDto>(
				PassPhotoDto.class, requestDetails);

		spiceManager.execute(request,new PassPhotoRequestListener(this));


	}


	public void cachePassPhotoData() {
		String json = new Gson().toJson(runtimeSettings.getPassPhoto());

		if (AppSettings.debug) {
			Log.d("WEM", "Pass Photo Data: " + json);
		}

		// save data to disk
		Editor cache = mPrefs.edit();
		cache.putString(AppSettings.PHOTO_PASS_CACHE, json);
		cache.commit();
	}

//Vivek VPN Check
	/*private class PassPhotosRequestListener implements
			RequestListener<PassPhotoDto> {

		@Override
		public void onRequestSuccess(PassPhotoDto result) {
			runtimeSettings.setPassPhoto(result);
			//cachePassPhotoData();
			passPhotoComplete = true;
			goBacktoTicketScanning(1);

		}

		@Override
		public void onRequestFailure(SpiceException spiceException) {
			passPhotoComplete = false;
			Log.d("PassPhoto","Network Error");
			goBacktoTicketScanning(2);

		}


	}*/
	private class PassPhotoRequestListener extends BaseMoaRequestListener<PassPhotoDto>{
		public PassPhotoRequestListener(MoaBaseActivity initiatingActivity){
			super(initiatingActivity);
		}
		@Override
		public void handleSuccess(PassPhotoDto result){
			runtimeSettings.setPassPhoto(result);
			passPhotoComplete = true;
			Log.d("Pass Photo Data","Retrieved Pass Photo Data Successfully");
			goBacktoTicketScanning(1);
		}
		@Override
		protected void handleResourceNotFoundError(HttpClientErrorException exception){
			Log.d("Pass Photo Data","Communication Error");
			Log.d("Pass Photo Data",exception.getMessage());
			passPhotoComplete = false;
			goBacktoTicketScanning(2);
		}
		@Override
		public void onRequestFailure(SpiceException spiceException) {
			Log.d("Pass Photo Data","Communication Error");
			Log.d("Pass Photo Data", spiceException.getMessage());
			passPhotoComplete = false;
			goBacktoTicketScanning(2);
		}
	}

	public void goBacktoTicketScanning(int resultCode)
	{
		Intent returnIntent = new Intent();
		returnIntent.putExtra("MESSAGE", "ACTIVITY COMPLETED");
		setResult(resultCode, returnIntent);
		finish();
	}
	public String connectServer() {
		Runtime runtime = Runtime.getRuntime();
		try {
			Process  mIpAddrProcess = runtime.exec("ping -c 1 "+"10.1.3.85");
			int mExitValue = mIpAddrProcess.waitFor();
			if(mExitValue==0){

				return "Connected";

			}else{

				return "Network Error";
			}
		}
		catch (InterruptedException ignore)
		{

			return "Network Error";
		}
		catch (IOException except)
		{

			return "Network Error";
		}

	}
}
