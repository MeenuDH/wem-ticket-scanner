package com.moa.ticketscanner.ui.dialogs;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.moa.ticketscanner.R;
import com.moa.ticketscanner.ui.RideSelectionActivity;

public class DialogRideSelectionConfirmation extends Dialog implements
		android.view.View.OnClickListener {

	private RideSelectionActivity activity;
	private String message;
	private Button cancel, confirm;
	private String leftButtonText;
	private Bitmap image;
	private boolean noRideSelected = true;

	public DialogRideSelectionConfirmation(RideSelectionActivity activity,
			String message, String leftButtonText) {
		super(activity);
		this.activity = activity;
		this.message = message;
		this.leftButtonText = leftButtonText;
	}

	public DialogRideSelectionConfirmation(RideSelectionActivity activity,
			String message, Bitmap image) {
		super(activity);
		this.activity = activity;
		this.message = message;
		this.image = image;
		this.noRideSelected = false;

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_ride_selection_confirmation);

		// set buttons
		confirm = (Button) findViewById(R.id.dialogRejectDismiss);
		confirm.setOnClickListener(this);

		cancel = (Button) findViewById(R.id.dialogCancel);
		// if we have no image then we are missing a ride selection
		if (noRideSelected) {
			ViewGroup layout = (LinearLayout) findViewById(R.id.dialogButtonContainer);
			layout.removeView(cancel);
			Button leftButton = (Button) findViewById(R.id.dialogRejectDismiss);
			leftButton.setText(leftButtonText);
		} else {
			cancel.setOnClickListener(this);
		}

		// set message
		TextView textView = (TextView) findViewById(R.id.dialogText);
		textView.setText(message);

		ImageView im = (ImageView) findViewById(R.id.dialogImage);
		im.setImageBitmap(this.image);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.dialogRejectDismiss:
			if (!noRideSelected) {
				activity.confirmSelection();

			}
			dismiss();
			break;
		case R.id.dialogCancel:
			dismiss();
			break;
		}
	}
}