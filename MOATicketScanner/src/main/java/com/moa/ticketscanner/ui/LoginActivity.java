package com.moa.ticketscanner.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.moa.ticketscanner.BuildConfig;
import com.moa.ticketscanner.R;
import com.moa.ticketscanner.communication.BaseMoaRequestListener;
import com.moa.ticketscanner.communication.MoaApiRequestDetails;
import com.moa.ticketscanner.communication.MoaApiSpiceRequest;
import com.moa.ticketscanner.debug.DebugLoginActivity;
import com.moa.ticketscanner.domain.OfflineScan;
import com.moa.ticketscanner.domain.UserRole;
import com.moa.ticketscanner.dto.configuration.TicketStatusCodeDto;
import com.moa.ticketscanner.dto.logon.UserRoleDto;
import com.moa.ticketscanner.dto.rides.RideRotationDto;
import com.moa.ticketscanner.services.OfflineDataService;
import com.moa.ticketscanner.settings.AppSettings;
import com.moa.ticketscanner.settings.RuntimeSettings;
import com.moa.ticketscanner.ui.widgets.ScanInputPageFragment.ScanInputScanListener;
import com.moa.ticketscanner.ui.widgets.ScanInputView;


import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.HttpClientErrorException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends MoaBaseActivity {
	private AppSettings as = new AppSettings();
	private SharedPreferences mPrefs;
	private RuntimeSettings runtimeSettings;
	private OfflineDataService offlineDataService = new OfflineDataService(this);
	private ScanInputView userIdScanInput;
	private ScanInputView passwordScanInput;
	private TextView txtApkDetails;
	private TextView txtDeviceId;
	private String currentScanInput = "";



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		runtimeSettings = RuntimeSettings.getInstance();
		mPrefs = getSharedPreferences(AppSettings.APPLICATION_NAME, MODE_PRIVATE);

		// Initialize UI elements
		txtApkDetails = (TextView)findViewById(R.id.text_version);
		txtDeviceId = (TextView)findViewById(R.id.text_deviceId);
		userIdScanInput = (ScanInputView)findViewById(R.id.scan_input_userId);
		passwordScanInput = (ScanInputView)findViewById(R.id.scan_input_password);
		passwordScanInput.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
		String apkDetails;
		if(AppSettings.MOA_API_DOMAIN == "http://10.1.3.85:90" && AppSettings.GALAXY_API_DOMAIN == "http://10.1.3.40:3051" )
		{
			apkDetails = "Production ";
		}
		else
		{
			apkDetails = "Staging ";
		}

		txtApkDetails.setText(apkDetails + BuildConfig.VERSION_NAME.toString());
		txtDeviceId.setText(runtimeSettings.getDeviceId());

		// bug fix for password field (hint text formatting)
//		passwordField.setTypeface(Typeface.SANS_SERIF, R.style.EntryText);
//		passwordField.setTransformationMethod(new PasswordTransformationMethod());

		// check for ride data. If none exist pull it from cache
		if (runtimeSettings.getRideRotations() == null) {
			if (AppSettings.debug)
				Log.d("WEM", "No Ride Data Pulling From Cache");
			getRideData();
		}

		// check for ticket data. If none exist pull it from cache
		if (runtimeSettings.getTicketStatusCodes() == null) {
			if (AppSettings.debug)
				Log.d("WEM", "No Ticket Data Pulling From Cache");
			getTicketData();
		}
		
		userIdScanInput.addScanListener(new ScanInputScanListener() {

			@Override
			public void onScanStarted() {
				currentScanInput = "userid";
			}

			@Override
			public void onScanStopped() {
				currentScanInput = "";
			}
			
		});
		
		passwordScanInput.addScanListener(new ScanInputScanListener() {

			@Override
			public void onScanStarted() {
				currentScanInput = "password";
			}

			@Override
			public void onScanStopped() {
				currentScanInput = "";
			}
			
		});

	}

	public void getRideData() {
		String rideJson = mPrefs.getString(AppSettings.RIDE_NAME_CACHE, "");
		if (rideJson != "") {
			Type type = new TypeToken<List<RideRotationDto>>() {
			}.getType();
			List<RideRotationDto> rideList = new Gson().fromJson(rideJson, type);
			runtimeSettings.setRideRotations(rideList);
		}
	}

	public void getTicketData() {
		String ticketJson = mPrefs.getString(AppSettings.TICKET_STATUS_CODE_CACHE, "");
		Type type = new TypeToken<List<TicketStatusCodeDto>>() {
		}.getType();
		List<TicketStatusCodeDto> ticketCodeList = new Gson().fromJson(ticketJson, type);
		runtimeSettings.setTicketStatusCodes(ticketCodeList);
	}

	@Override
	public void onResume() {
		super.onResume();
		userIdScanInput.setText("");
		passwordScanInput.setText("");

		// if offline push to offline mode
		if (!RuntimeSettings.getInstance().isOnline()) {
			goOffline();
			// otherwise check for online and if we are push up offline data if
			// it
			// exists
		} else {
			File offlineModeFile = new File(getFilesDir().getPath() + "/" + AppSettings.OFFLINE_MODE_FILE_NAME);
			if (offlineModeFile.exists()) {
				if (AppSettings.debug) {
					Log.d("WEM", "Offline Data File Exists");
				}
				try {
					List<OfflineScan> offlineData = getOfflineData(offlineModeFile);
					offlineDataService.submitOfflineScans(offlineData);

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	private List<OfflineScan> getOfflineData(File offlineModeFile) throws IOException {
		if (AppSettings.debug) {
			Log.d("WEM", "Offline File Location: " + offlineModeFile.getPath());
		}
		ObjectMapper jsonMapper = new ObjectMapper();
		List<OfflineScan> scans = new ArrayList<OfflineScan>();
		
		// read file in
		InputStream is = new FileInputStream(offlineModeFile);
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line = "";
		
		for(line = br.readLine(); line != null; line = br.readLine()) {
			OfflineScan scan = jsonMapper.readValue(line, OfflineScan.class);
			scans.add(scan);
		}

		is.close();
		isr.close();
		br.close();
		return scans;
	}

	@Override
	public void onPause() {
		super.onPause();

	}

	public void goOffline() {
		Intent intent = new Intent(this, OfflineTicketScannerActivity.class);
		startActivity(intent);
	}

	@Override
	public void onScanIntent(Intent intent) {

		String data = intent.getStringExtra(DATA_STRING_TAG);
		if (data != null) {
			if(currentScanInput.equals("userid")) {
				userIdScanInput.setText(data);
			} else if(currentScanInput.equals("password")) {
				passwordScanInput.setText(data);
			} else if(StringUtils.isBlank(userIdScanInput.getText())) {
				userIdScanInput.setText(data);
			} 
			else if (passwordScanInput.getText().toString().isEmpty()) {
				passwordScanInput.setText(data);
			}
			
//			if (idField.getText().toString().isEmpty()) {
//				idField.setText(data);
//
//				// // Pop the user straight into entering their password
//				// passwordField.requestFocus();
//				// InputMethodManager inputMethodManager = (InputMethodManager)
//				// getSystemService(Context.INPUT_METHOD_SERVICE);
//				// inputMethodManager.showSoftInput(passwordField,
//				// InputMethodManager.SHOW_IMPLICIT);
//			} else if (passwordField.getText().toString().isEmpty()) {
//				passwordField.setText(data);
//			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	public void continueBtn(View v) {
		// if we are online then continue with login
		if (RuntimeSettings.getInstance().isOnline()) {
			if (StringUtils.isEmpty(userIdScanInput.getText())) {
				showErrorDialog("Invalid Credentials", "Please enter your User ID");
				return;
			} else if (StringUtils.isEmpty(passwordScanInput.getText())) {
				showErrorDialog("Invalid Credentials", "Please enter your password");
				return;
			}
			getUserRole(userIdScanInput.getText());

		}
		// else go to offLine mode
		else {
			goOffline();
		}
	}

	public void developmentMode(View v) {
		if (AppSettings.debug_screens_available) {
			as.addClick();

			if (as.getClick() >= as.debugCount) {
				Intent intent = new Intent(this, DebugLoginActivity.class);
				startActivity(intent);
			}
		}
	}

	private void getUserRole(String userId) {
		String uri = String.format(AppSettings.USER_ROLE_URI_FORMAT, userId);
		MoaApiRequestDetails requestDetails = new MoaApiRequestDetails();
		requestDetails.setHttpMethod(HttpMethod.GET);
		requestDetails.setUri(uri);
		MoaApiSpiceRequest<UserRoleDto> request = new MoaApiSpiceRequest<UserRoleDto>(UserRoleDto.class, requestDetails);
		//Vivek VPN Check

		super.checkNetworkConnection();
		if(runtimeSettings.isServerReachable()){
			Log.d("LoginActivity", "VPN Connection Successful");
			getSpiceManager().execute(request, new UserRoleRequestListener(this));
		}
		else
		{
			super.showErrorDialog("VPN Check Login","Network Error. Try again later");
			//finish();
		}

	}

	private Bundle buildExtras(UserRole role) {
		Bundle extras = new Bundle();
		extras.putString(AppSettings.USER_ID_KEY, userIdScanInput.getText());
		extras.putString(AppSettings.USER_PASSWORD_KEY, passwordScanInput.getText());
		extras.putString(AppSettings.USER_ROLE_KEY, role.toString());
		return extras;
	}



	private class UserRoleRequestListener extends BaseMoaRequestListener<UserRoleDto> {
		public UserRoleRequestListener(MoaBaseActivity initiatingActivity) {
			super(initiatingActivity);
			initiatingActivity.checkNetworkConnection();
		}

		@Override
		public void handleSuccess(UserRoleDto result) {
			UserRole userRole = UserRole.getByRoleValue(result.getRole());
			if (userRole == null) {
				showErrorDialog("Unauthorized User",
						"Your User ID was not found. Please re-enter your User ID and try again. Otherwise please check with a Supervisor to ensure you have rights to scan tickets.");
			} else {
				runtimeSettings.setUserRole(userRole);
				runtimeSettings.setUserId(userIdScanInput.getText());
				transferToActivity(RideSelectionActivity.class, buildExtras(userRole));
			}
		}

		@Override
		protected void handleResourceNotFoundError(HttpClientErrorException exception) {
			showErrorDialog("Unauthorized User",
					"Your User ID was not found. Please re-enter your User ID and try again. Otherwise please check with a Supervisor to ensure you have rights to scan tickets.");
		}
	}



}
