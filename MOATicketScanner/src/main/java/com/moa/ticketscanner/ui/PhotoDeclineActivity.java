package com.moa.ticketscanner.ui;



import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.moa.ticketscanner.R;
import com.moa.ticketscanner.communication.BaseMoaRequestListener;
import com.moa.ticketscanner.communication.MoaApiRequestDetails;
import com.moa.ticketscanner.communication.MoaApiSpiceRequest;
import com.moa.ticketscanner.dto.tickets.DeclinedTicketDto;
import com.moa.ticketscanner.dto.tickets.SubTicketDto;
import com.moa.ticketscanner.settings.AppSettings;
import com.moa.ticketscanner.settings.RuntimeSettings;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;

import org.springframework.http.HttpMethod;

import java.util.List;


public class PhotoDeclineActivity extends MoaBaseActivity {

    SpiceManager spiceManager;

    SharedPreferences mPrefs;
    RuntimeSettings runtimeSettings;

    public LinearLayout layout;
    public LinearLayout.LayoutParams p;
    public RadioGroup radioGroup;
    public Button btnOk;
    public Button btnCancel;
    private String visualId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photodecline_reasons);
        runtimeSettings = RuntimeSettings.getInstance();
        spiceManager = super.getSpiceManager();

        mPrefs =  getSharedPreferences(AppSettings.APPLICATION_NAME, MODE_PRIVATE);

        visualId = getIntent().getStringExtra("visualId");
        addOkButton();
        addCancelButton();

    }
    public void onClick(View view) {
        try {
            String s = ((RadioButton) view).getText().toString();
            Toast.makeText(PhotoDeclineActivity.this, "This is: " + s,
                    Toast.LENGTH_LONG).show();
        }
        catch (Exception e1) {
            e1.printStackTrace();
        }
    }
    public void addOkButton() {

        //radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);
        btnOk = (Button) findViewById(R.id.btnOk);

        btnOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                radioGroup = (RadioGroup) findViewById(R.id.radioDeclineReasons) ;
                RadioButton selectRadio = (RadioButton) findViewById(radioGroup
                        .getCheckedRadioButtonId());
                String reason = selectRadio.getText().toString();
                runtimeSettings.setPhotoActivityStatus("Done");
                uploadDeclinedTicket(reason.replace(' ','_'));
                goBackToPhotoActivity(200,reason);
            }

        });

    }
    public void addCancelButton() {

        //radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);
        btnCancel = (Button) findViewById(R.id.btnCancel);

        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

               finish();

            }

        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.splash_screen, menu);
        return true;
    }

    public void goBackToPhotoActivity(int resultCode, String reason)
    {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("DeclineReason", reason);
        setResult(resultCode, returnIntent);
        finish();
    }
    private void ShowAlert(String test)
    {
        AlertDialog alertDialog = new AlertDialog.Builder(PhotoDeclineActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage(test);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();


    }
    public void uploadDeclinedTicket(String declinedReason){
       try{

           DeclinedTicketDto declinedTicketDto = new DeclinedTicketDto();
           declinedTicketDto.setRideId(runtimeSettings.getCurrentRide().getScanId());
           declinedTicketDto.setVisualId(visualId);
           declinedTicketDto.setReason(declinedReason);
           declinedTicketDto.setUserId(runtimeSettings.getUserId());
           String uri = AppSettings.UPLOAD_DECLINED_PHOTO_URI + visualId;
           MoaApiRequestDetails requestDetails = new MoaApiRequestDetails();
           requestDetails.setHttpMethod(HttpMethod.POST);
           requestDetails.setUri(uri);
           requestDetails.setRequestBody(declinedTicketDto);
           MoaApiSpiceRequest<Object> request = new MoaApiSpiceRequest<Object>(Object.class, requestDetails);
           spiceManager.execute(request,new PhotoDeclineListener(this));
       }
       catch(Exception ex)
       {
           ShowAlert(ex.getMessage());
       }

    }
    private class PhotoDeclineListener extends BaseMoaRequestListener<Object> {
        public PhotoDeclineListener(MoaBaseActivity initiatingActivity) {
            super(initiatingActivity);
        }
        @Override
        public void handleSuccess(Object result) {
           Log.d("PhotoDecline","Photo Delcine Ticked Uploaded");
            ShowAlert("DeclineSuccess");

        }
        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Log.d("PhotoDecline","Photo Delcine Ticked Upload Failed");
            ShowAlert("DeclineFailure");
            ShowAlert(spiceException.getMessage());
        }
    }
}
