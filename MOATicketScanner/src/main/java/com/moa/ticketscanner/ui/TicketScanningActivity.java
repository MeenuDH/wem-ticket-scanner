package com.moa.ticketscanner.ui;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;


import com.moa.ticketscanner.R;
import com.moa.ticketscanner.communication.BaseGalaxyRequestListener;
import com.moa.ticketscanner.communication.GalaxyApiSpiceRequest;
import com.moa.ticketscanner.domain.AcceptanceAudio;
import com.moa.ticketscanner.domain.Ride;
import com.moa.ticketscanner.dto.BaseGalaxyRequestDto;
import com.moa.ticketscanner.dto.HeaderDto;

import com.moa.ticketscanner.dto.configuration.PassPhotoDto;
import com.moa.ticketscanner.dto.configuration.TicketStatusCodeDto;
import com.moa.ticketscanner.dto.logon.LogoffRequestDto;
import com.moa.ticketscanner.dto.logon.LogoffResponseDto;
import com.moa.ticketscanner.dto.tickets.FacilityDto;
import com.moa.ticketscanner.dto.tickets.TicketValidationRequestDto;
import com.moa.ticketscanner.dto.tickets.TicketValidationResponseDto;
import com.moa.ticketscanner.settings.AppSettings;
import com.moa.ticketscanner.settings.RuntimeSettings;
import com.moa.ticketscanner.ui.dialogs.DialogAllowRideConfirmation;
import com.moa.ticketscanner.ui.dialogs.DialogPhotoDisplay;
import com.moa.ticketscanner.ui.dialogs.DialogRideSelectionConfirmation;
import com.moa.ticketscanner.ui.dialogs.DialogTicketAcceptance;
import com.moa.ticketscanner.ui.dialogs.DialogTicketRejection;
import com.moa.ticketscanner.ui.widgets.ScanInputView;
import com.octo.android.robospice.SpiceManager;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class TicketScanningActivity extends MoaBaseActivity {
	private RuntimeSettings runtimeSettings;
	private Context context;
	private int cycleCount = 0;
	private ScanInputView ticketScanInput;
	private TextView pointsRemainingNumber;
	private TextView pointsRemainingText;
	private TextView cycleCounter;
	private Boolean enableScans = true;
	private String message = "TEST";
	private RuntimeSettings settings = RuntimeSettings.getInstance();
	private String checkHasPhoto = "false";
	private String checkNeedsPhoto = "false";
	private Boolean allowTicket = true;
	private Boolean confirmAllowTicket = true;
	private String photoUrl = "";
	private List<FacilityDto> facilities = new ArrayList<FacilityDto>();
	private WebView webView;
	SpiceManager spiceManager;
	private static final String IMAGE_DIRECTORY_NAME = "TicketScanner";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ticket_scanning);
		context = this;
		runtimeSettings = RuntimeSettings.getInstance();

		// initialize ui handles
		ticketScanInput = (ScanInputView) findViewById(R.id.ticketScanInput);
		pointsRemainingNumber = (TextView) findViewById(R.id.ticketScanningPointsRemainingNumber);
		pointsRemainingText = (TextView) findViewById(R.id.ticketScanningPointsRemaining);
		cycleCounter = (TextView) findViewById(R.id.ticketScanningCycleCounterNumber);
		webView = (WebView) findViewById(R.id.webView);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setUseWideViewPort(true);
		webView.setBackgroundColor(Color.TRANSPARENT);
		//String customHtml1 = "<html><body><p>Cyclone</p><ul><li>Confirm Height 48&#34 (122 cm) minimum</li><li>Minimum &#39weight&#39 88lbs (40kg)</li><li>Minimum weight 88lbs (40kg)</li><li>Minimum weight 88lbs (40kg)</li><li>Minimum weight 88lbs (40kg)</li><li>Minimum weight 88lbs (40kg)</li><li>Minimum weight 88lbs (40kg)</li><li>Minimum weight 88lbs (40kg)</li></ul></body></html>";
		String customHtml1 = "<html><body><p></p></body></html>";
		String customHtml = runtimeSettings.getCurrentRide().getRideRules();

		if(customHtml != null) {
			Log.d("RideRule", customHtml);
			webView.loadData(customHtml, "text/html", "UTF-8");
		}
		else
		{
			Log.d("RideRule", "Null");
			webView.loadData(customHtml1, "text/html", "UTF-8");

		}

		// Set ride name in action bar
		ActionBar actionBar = getActionBar();
		if (runtimeSettings.getCurrentRide() != null)
			actionBar.setTitle(runtimeSettings.getCurrentRide().getRideName());

		// setup done button for ticket
		ticketScanInput
				.setOnEditorActionListener(new OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
												  KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_DONE) {

							//This validation is if a ride bar code is scanned in stead of Ticket
							if (v.getText().toString().length() < 16) {
								enableScans = true;
								ShowAlert("Invalid. Scan a valid Ticket");
							} else {
								CheckNetworkConnection();
								//Work Flow: On Scanning Or Entering the Day Pass number Get the Pass Photo Data
								transferToPassPhotoData();
							}

						}
						return false;
					}
				});
	}
	private void CheckNetworkConnection(){
		super.checkNetworkConnection();
	}
	private void ShowAlert(String test)
	{
		AlertDialog alertDialog = new AlertDialog.Builder(TicketScanningActivity.this).create();
		alertDialog.setTitle("Alert");
		alertDialog.setMessage(test);
		alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		alertDialog.show();
	}
	private void ShowConfirmationAlert()
	{

		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.MOATheme));
		alertBuilder.setTitle(R.string.ticket_scanning_logout_confirmation_title);
		alertBuilder.setMessage(R.string.ticket_scanning_logout_confirmation_text);
		alertBuilder.setPositiveButton(R.string.ticket_scanning_logout_confirmation_yes, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				confirmAllowTicket = true;
				dialog.dismiss();
			}
		});
		alertBuilder.setNegativeButton(R.string.ticket_scanning_logout_confirmation_no, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				confirmAllowTicket = false;
				dialog.dismiss();
			}
		});
		alertBuilder.create().show();

	}
	@Override
	public void onBackPressed()
	{
		// super.onBackPressed(); // Comment this super call to avoid calling finish()
	}
	/*@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK) && webView!=null && webView.canGoBack()) {
			webView.goBack();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}*/
	public void showLogoutConfirmation(View v) {
		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.MOATheme));
		alertBuilder.setTitle(R.string.ticket_scanning_logout_confirmation_title);
		alertBuilder.setMessage(R.string.ticket_scanning_logout_confirmation_text);
		alertBuilder.setPositiveButton(R.string.ticket_scanning_logout_confirmation_yes, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				logout();
			}
		});
		alertBuilder.setNegativeButton(R.string.ticket_scanning_logout_confirmation_no, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		alertBuilder.create().show();
	}

	public void logout() {
		HeaderDto header = HeaderDto.Create(context, LogoffRequestDto.MESSAGE_TYPE);
		header.setSessionID(runtimeSettings.getGatewaySessionId());
		GalaxyApiSpiceRequest<LogoffResponseDto> request = new GalaxyApiSpiceRequest<LogoffResponseDto>(LogoffResponseDto.class, new BaseGalaxyRequestDto<LogoffRequestDto>(header, null));

		getSpiceManager().execute(request, new LogoffRequestListener(this));
	}

	public void ticketHistory(View view) {
		Intent intent = new Intent(this, TicketHistoryActivity.class);
		startActivity(intent);
	}

	public void setPointsRemaining(int points) {
		pointsRemainingNumber.setText(String.valueOf(points));
		pointsRemainingNumber.setVisibility(View.VISIBLE);
		pointsRemainingText.setVisibility(View.VISIBLE);
	}

	public void updateCounter() {
		cycleCounter.setText(String.valueOf(cycleCount));
	}

	public void incrementCounter() {
		cycleCount++;
		updateCounter();
	}

	public void resetCounter(View view) {
		if (AppSettings.debug)
			Log.d("WEM", "Resetting Cycle Counter");
		cycleCount = 0;
		updateCounter();
	}
	/*public void buildAllowRideDialog() {
		// build dialog and show
		String message1 = "This Wristband was";
		String message2 = "used at another Facility !";

		DialogAllowRideConfirmation dialog = new DialogAllowRideConfirmation(this, message1,message2);

		dialog.show();
	}*/
	public void confirmSelection() {
		if (AppSettings.debug)
			Log.d("WEM", "Ride Selection Confirmed");
		confirmAllowTicket = true;
		if(checkHasPhoto == "true")
		{
			//Workflow Case 1: If the Photo exists Show Photo with OK Cancel
			showPassPhotoDialog();
		}
		else
		{

			if(checkNeedsPhoto == "true")
			{
				transferToPhoto();
			}
			else
			{
				submitTicket(false);
			}
		}
	}
	public void submitLock() {
		//confirmAllowTicket = false;
		//ShowAlert("Cancel Clicked");
		return;
	}

	public void submit(View viw) {
		confirmAllowTicket = true;
	}
	public void submitTicket(boolean override) {
		pointsRemainingNumber.setVisibility(View.INVISIBLE);
		pointsRemainingText.setVisibility(View.INVISIBLE);

		HeaderDto header = HeaderDto.Create(context, TicketValidationRequestDto.MESSAGE_TYPE);
		header.setSessionID(runtimeSettings.getGatewaySessionId());
		TicketValidationRequestDto validationDto = new TicketValidationRequestDto(ticketScanInput.getText());
		validationDto.setOverride(override);
		BaseGalaxyRequestDto<TicketValidationRequestDto> requestDto = new BaseGalaxyRequestDto<TicketValidationRequestDto>(header, validationDto);
		GalaxyApiSpiceRequest<TicketValidationResponseDto> request = new GalaxyApiSpiceRequest<TicketValidationResponseDto>(TicketValidationResponseDto.class, requestDto);

		getSpiceManager().execute(request, new TicketValidationRequestListener(this));
	}

	@Override
	public void onScanIntent(Intent intent) {
		if (ticketScanInput != null && enableScans) {
			String data = intent.getStringExtra(DATA_STRING_TAG);

			if (data != null) {
				ticketScanInput.setText(data);
				if(data.length() < 16){
					enableScans = true;
					ShowAlert("Invalid. Scan a valid Ticket");

				}
				else
				{
						transferToPassPhotoData();
				}

			}

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ticket_scanning, menu);
		return true;
	}

	private void transferToLogin() {
		runtimeSettings.setGatewaySessionId(null);
		Intent intent = new Intent(this, LoginActivity.class);
		startActivity(intent);
		finish();
	}
	private void transferToPhoto() {

		Intent intent = new Intent(this, PhotoActivity.class);
//		intent.putExtra("visualId",ticketScanInput.getText().toString());
		startActivityForResult(intent, 2);
		//finish();
	}
	private void transferToPassPhotoData() {

		Intent intent = new Intent(TicketScanningActivity.this, PassPhotoDataActivity.class);
		intent.putExtra("visualId", ticketScanInput.getText().toString().trim());
		startActivityForResult(intent, 1);
		//finish();
	}

	private void showPassPhotoDialog()
	{
		final DialogPhotoDisplay dialog1 = new DialogPhotoDisplay(this, photoUrl);
		dialog1.setCancelListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				dialog1.dismiss();
			}
		});
		dialog1.setOkListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.d("WEM", "OK Button Clicked");
				submitTicket(false);
				dialog1.dismiss();
				//showTestDialog(v);
			}
		});
		dialog1.show();
	}
	private void ticketSuccessful(final TicketValidationResponseDto result) {
		incrementCounter();

		if(Integer.toString(result.getAccessCode()).startsWith("41") || Integer.toString(result.getAccessCode()).startsWith("61") ||Integer.toString(result.getAccessCode()).startsWith("71")) {
			setPointsRemaining(result.getPointsRemaining());
		}
		String audioFile = result.getSoundFile();
		boolean isAnnualPassWinner = audioFile.equalsIgnoreCase("apwin.wav");
		boolean isChaperone = isChaperon(result.getAccessCode());
		String chaperoneType = getChaperoneType(result.getAccessCode());

		if(isChaperone)
		{
			audioFile = "chaperone.wav";
		}
		AcceptanceAudio audio = AcceptanceAudio.findByAudioName(audioFile);
		final DialogTicketAcceptance dialog = new DialogTicketAcceptance(this, audio, isAnnualPassWinner, isChaperone, chaperoneType);
		if(isAnnualPassWinner) {
			dialog.setDialogText(getResources().getString(R.string.dialog_string_annual_pass_winner));
			disableScanning();
			dialog.setDismissListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					enableScans = true;
					enableScanning();
					dialog.dismiss();
				}
			});
		}
		//Vivek Test First Name For Release 1.2
//		Log.d("WEM-Test- BD", Boolean.toString(result.isBirthday()));
//		Log.d("WEM-Test- FirstName", result.getFirstName());
		Log.d("WEM", "Ticket Validated - Accepted");
		dialog.show();
		ticketScanInput.setText("");
	}

	private void rejectTicket(TicketValidationResponseDto validationResponse) {

		TicketStatusCodeDto matchedStatusCode = null;
		List<TicketStatusCodeDto> statusCodes = runtimeSettings.getTicketStatusCodes();
		for(TicketStatusCodeDto code : statusCodes) {
			if(code.getCode() == validationResponse.getTicketStatus()) {
				matchedStatusCode = code;
				break;
			}
		}

		if(matchedStatusCode == null) {
			String rejectionText = String.format("Error %s: %s", validationResponse.getTicketStatus(), validationResponse.getTicketStatusText());
			matchedStatusCode = new TicketStatusCodeDto(rejectionText, false);
			matchedStatusCode.setCode(validationResponse.getTicketStatus());
		}

		Log.d("WEM", "Ticket Validated - Rejected");
		showRejectionDialog(matchedStatusCode, validationResponse);
	}

	private void showRejectionDialog(TicketStatusCodeDto rejectionStatus, TicketValidationResponseDto validationResponse) {
		String subMessage = null;
		boolean canOverride = rejectionStatus.isCanOverride();
		if(rejectionStatus.getCode() == 45) {
			subMessage = String.format("%s points remaining", validationResponse.getPointsRemaining());
			canOverride = (validationResponse.getPointsRemaining() < 1) ? false : canOverride;
		} else if(rejectionStatus.getCode() == 1) {
			if(validationResponse.isUsesLeftValid()) {
				subMessage = String.format("%s Uses Left", validationResponse.getUsesLeft());
			} else {
				subMessage = validationResponse.getValidityInfo();
			}
		} else if(rejectionStatus.getCode() == 2) {
			subMessage = validationResponse.getValidityInfo();
		}

		disableScanning();

		final DialogTicketRejection dialog = new DialogTicketRejection(this, rejectionStatus.getDisplayText(), subMessage, canOverride);
		dialog.setOverrideListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				submitTicket(true);
				enableScans = true;
				enableScanning();
				dialog.dismiss();
			}
		});
		dialog.setDismissListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				enableScans = true;
				enableScanning();
				dialog.dismiss();
			}
		});
		enableScans = false;
		dialog.show();
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		String description = null;
		if (requestCode == 1)
		{
			//if(resultCode == Activity.RESULT_OK){
			//isPhotoactivityCompleted = data.getBooleanExtra("PhotoActivityFinished", false);
			//message = data.getStringExtra("MESSAGE");
			//}
			//if (resultCode == Activity.RESULT_CANCELED) {
			//Write your code if there's no result
			//}
			if(resultCode == 2)
			{
				final DialogTicketRejection dialog = new DialogTicketRejection(this, "Network Error", null, false);
				dialog.setDismissListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						ticketScanInput.setText("");
						enableScans = true;
						transferToLogin();
						dialog.dismiss();
					}
				});
				enableScans = true;
				dialog.show();

			}

			if(resultCode == 1)
			{
				PassPhotoDto chkPhotoDto = settings.getPassPhoto();
				checkHasPhoto = chkPhotoDto.getHasPhoto();
				checkNeedsPhoto = chkPhotoDto.getNeedsPhoto();
				photoUrl = chkPhotoDto.getPhotoUrl();
				facilities = chkPhotoDto.getFacilities();


				if(chkPhotoDto.getError() == "true")
				{
					final DialogTicketRejection dialog = new DialogTicketRejection(this, chkPhotoDto.getErrorMessage(), null, false);
					dialog.setDismissListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							enableScans = true;
							dialog.dismiss();
						}
					});
					enableScans = false;
					dialog.show();
				}
				else
				{
					if(facilities == null)
					{
						allowTicket = true;
						Log.d("WEM", "Facility Null");
					}
					else
					{
						for(FacilityDto facility : facilities){

							if(facility.getFacilityId() !=settings.getCurrentRide().getFacilityId())
							{
								description = facility.getDescription() + " ";
								allowTicket = false;
								Log.d("WEM", "Used at the Other Facility");
							}
							else
							{
								if(allowTicket != false)
									allowTicket = true;
								Log.d("WEM", "Used at the Same Facility");
							}
						}
					}
					if(allowTicket == false)
					{
						DialogAllowRideConfirmation dialog = new DialogAllowRideConfirmation(this,"This wristband was", "used at" , description.trim());
						dialog.show();
						/*if(confirmAllowTicket == false)
						{
							return;
						}*/

					}
					else
					{
						if(checkHasPhoto == "true")
						{
							//Workflow Case 1: If the Photo exists Show Photo with OK Cancel
							showPassPhotoDialog();
						}
						else
						{

							if(checkNeedsPhoto == "true")
							{
								transferToPhoto();
							}
							else
							{
								submitTicket(false);
							}
						}
					}

				}
			}

		}
		if(requestCode == 2 && resultCode == RESULT_OK) {
			if (settings.getPhotoActivityStatus() == "Done")
			{
				try{
					DeletePhoto();
					submitTicket(false);
				}catch(Exception e)
				{
					ShowAlert("Ticket Not Submitted");
				}

			}
		}

		super.onActivityResult(requestCode, resultCode, data);
	}//onActivityResult
	private void DeletePhoto() throws Exception {

//		File mediaStorageDir = new File(
//				Environment
//						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//				IMAGE_DIRECTORY_NAME);
		File dir = new File(Environment.getExternalStorageDirectory()+"/"+Environment.DIRECTORY_PICTURES+"/"+IMAGE_DIRECTORY_NAME);
		if (dir.isDirectory())
		{
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++)
			{
				new File(dir, children[i]).delete();
			}
		}

	}
	public boolean isChaperon(int accessCode){
		boolean result;
		switch(accessCode)
		{
			case 3105 :
			case 3106 :
			case 3110 :
			case 3113 :
			case 3123 :
			case 3124 :
			case 3205 :
			case 3206 :
			case 3210 :
			case 3295 :
				result = true;
				break;
			default :
				result = false;
		}
		return result;
	}
	public String getChaperoneType(int accessCode)
	{
		String chaperoneType;
		switch(accessCode)
		{
			case 3105 :
				chaperoneType = "Under 43 Chaperone";
				break;
			case 3106 :
			case 3206 :
				chaperoneType = "2 and Under";
				break;
			case 3110 :
				chaperoneType = "2 and Under Chaperone";
				break;
			case 3113 :
			case 3210 :
				chaperoneType = "Toddler Time Chaperone";
				break;
			case 3123 :
			case 3295 :
				chaperoneType = "Complimentary Chaperone";	
				break;
			case 3124 :
				chaperoneType = "Guest With Disability Chaperone";
				break;
			case 3205 :
				chaperoneType = "Chaperone";
				break;
			default :
				chaperoneType = "";
				break;
		}

		return chaperoneType;
	}

	private class TicketValidationRequestListener extends BaseGalaxyRequestListener<TicketValidationResponseDto> {
		public TicketValidationRequestListener(
				MoaBaseActivity initiatingActivity) {
			super(initiatingActivity);
		}

		@Override
		public void handleSuccess(TicketValidationResponseDto result) {

			if(result.isTicketValid()) {
				ticketSuccessful(result);
			} else {
				rejectTicket(result);
			}
		}

		@Override
		protected void handleResponseErrors(TicketValidationResponseDto result) {
			if(result.getErrorCode().equalsIgnoreCase("100")) {
				rejectTicket(result);
			} else {
				super.handleResponseErrors(result);
			}
		}
	}

	private class LogoffRequestListener extends BaseGalaxyRequestListener<LogoffResponseDto> {

		public LogoffRequestListener(MoaBaseActivity initiatingActivity) {
			super(initiatingActivity);
		}

		@Override
		public void handleSuccess(LogoffResponseDto result) {
			transferToLogin();
		}
	}

}
