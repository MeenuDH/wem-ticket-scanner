package com.moa.ticketscanner.ui;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.moa.ticketscanner.R;
import com.moa.ticketscanner.communication.BaseGalaxyRequestListener;
import com.moa.ticketscanner.communication.GalaxyApiSpiceRequest;
import com.moa.ticketscanner.domain.Ride;
import com.moa.ticketscanner.domain.UserRole;
import com.moa.ticketscanner.dto.BaseGalaxyRequestDto;
import com.moa.ticketscanner.dto.HeaderDto;
import com.moa.ticketscanner.dto.configuration.PassPhotoDto;
import com.moa.ticketscanner.dto.rides.RideDto;
import com.moa.ticketscanner.dto.rides.RideRotationDto;
import com.moa.ticketscanner.dto.tickets.DebitCardDto;
import com.moa.ticketscanner.dto.tickets.LookupDto;
import com.moa.ticketscanner.dto.tickets.ReverseUsageRequestDto;
import com.moa.ticketscanner.dto.tickets.ReverseUsageResponseDto;
import com.moa.ticketscanner.dto.tickets.TicketDto;
import com.moa.ticketscanner.dto.tickets.TicketLookupRequestDto;
import com.moa.ticketscanner.dto.tickets.TicketLookupResponseDto;
import com.moa.ticketscanner.dto.tickets.UsageDto;
import com.moa.ticketscanner.settings.RuntimeSettings;
import com.moa.ticketscanner.ticketHistoryList.TicketHistoryListHeader;
import com.moa.ticketscanner.ticketHistoryList.TicketHistoryListViewAdapter;
import com.moa.ticketscanner.ui.widgets.ScanInputView;
import com.octo.android.robospice.persistence.exception.SpiceException;


import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class TicketHistoryActivity extends MoaBaseActivity {

	private ScanInputView ticketScanInput;
	private TextView pointsRemainingNumber;
	private TextView pointsRemainingText;
	private TextView noHistoryText;
	private ExpandableListAdapter listAdapter;
	private ExpandableListView expListView;
	private List<TicketHistoryListHeader> listDataHeader;
	private HashMap<TicketHistoryListHeader, Integer> childData;
	private static DateFormat DATE_FORMATTER = new SimpleDateFormat("MM-dd-yyyy @ hh:mm a");
	private String scannedTicket;
	private String subTicketVisualId;
	private RuntimeSettings runtimeSettings = RuntimeSettings.getInstance();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ticket_history);

		ticketScanInput = (ScanInputView) findViewById(R.id.ticketHistoryScanInput);
		pointsRemainingNumber = (TextView) findViewById(R.id.ticketHistoryPointsRemainingNumber);
		pointsRemainingText = (TextView) findViewById(R.id.ticketHistoryPointsRemaining);
		noHistoryText = (TextView) findViewById(R.id.ticketHistoryNoHistory);
		expListView = (ExpandableListView) findViewById(R.id.historyListView);

		ActionBar actionBar = getActionBar();
		actionBar.setTitle("Ticket History");

		// setup done button for ticket
		ticketScanInput.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					scannedTicket = v.getText().toString().trim();
					if( v.getText().toString().length() < 16 )
					{
						showErrorDialog("Invalid Ticket",scannedTicket);
					}
					else {
						if(v.getText().toString().length() == 17)
						{
							transferToSuperTicketActivity();
						}
						else {
							getTicketHistory(v.getText().toString().trim());
						}

					}
				}
				return false;
			}
		});
	}
	
	private void buildListView() {
		listAdapter = new TicketHistoryListViewAdapter(this, listDataHeader, childData);

		// setting list adapter
		expListView.setAdapter(listAdapter);

		// when listview is expanded we need to do some things
		expListView.setOnGroupExpandListener(new OnGroupExpandListener() {

			@Override
			public void onGroupExpand(int groupPosition) {
				View view = expListView.getChildAt(groupPosition);
				if (view != null) {
					ImageView arrow = (ImageView) view.findViewById(R.id.ticketHistoryArrow);
					if (arrow != null) {
						arrow.setImageDrawable(getResources().getDrawable(R.drawable.arrow_up));
					}
				} else {
					Log.d("", "");
				}
			}
		});

		// when the listview is collapsed we need to do some things
		expListView.setOnGroupCollapseListener(new OnGroupCollapseListener() {

			@Override
			public void onGroupCollapse(int groupPosition) {
				View view = expListView.getChildAt(groupPosition);
				if(view!=null) {
					ImageView arrow = (ImageView) view.findViewById(R.id.ticketHistoryArrow);
					if(arrow != null) {
						arrow.setImageDrawable(getResources().getDrawable(R.drawable.arrow_down));
					}
				} else {
					Log.d("","");
				}
			}
		});
		
		expListView.setOnGroupClickListener(new OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
				boolean isRefundable = (Boolean) v.getTag();
				return !isRefundable;
			}
		});
	}
	private void transferToSuperTicketActivity() {

		Intent intent = new Intent(TicketHistoryActivity.this, SuperTicketActivity.class);
		intent.putExtra("visualId", ticketScanInput.getText().toString().trim());
		startActivityForResult(intent, 1);
		//finish();
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == 1)
		{

			if(resultCode == 2)
			{

			}

			if(resultCode == 1)
			{
				//subTicketVisualId = getIntent().getStringExtra("SubTicketVisualId");
				subTicketVisualId = RuntimeSettings.getInstance().getSelectedSubTicket();
				Log.d("Selected Sub Ticket", subTicketVisualId);
				getTicketHistory(subTicketVisualId);
			}

		}

		super.onActivityResult(requestCode, resultCode, data);
	}//onActivityResult
	private void getTicketHistory(String ticketId) {
		HeaderDto headerDto = HeaderDto.Create(context, TicketLookupRequestDto.MESSAGE_TYPE);
		headerDto.setSessionID(RuntimeSettings.getInstance().getGatewaySessionId());
		TicketLookupRequestDto requestBody = new TicketLookupRequestDto();
		requestBody.setVisualID(ticketId);
		GalaxyApiSpiceRequest<TicketLookupResponseDto> request = 
				new GalaxyApiSpiceRequest<TicketLookupResponseDto>(TicketLookupResponseDto.class, new BaseGalaxyRequestDto<TicketLookupRequestDto>(headerDto, requestBody));
		getSpiceManager().execute(request, new TicketHistoryRequestListener(this));
	}

	public void refundPoints(TicketHistoryListHeader header, int pointsToRefund, Button refundButton) {
		HeaderDto headerDto = HeaderDto.Create(context, ReverseUsageRequestDto.MESSAGE_TYPE);
		headerDto.setSessionID(RuntimeSettings.getInstance().getGatewaySessionId());
		ReverseUsageRequestDto dto = new ReverseUsageRequestDto();
		dto.setUsageID(header.getUsageId());
		GalaxyApiSpiceRequest<ReverseUsageResponseDto> request = 
				new GalaxyApiSpiceRequest<ReverseUsageResponseDto>(ReverseUsageResponseDto.class, new BaseGalaxyRequestDto<ReverseUsageRequestDto>(headerDto, dto));
		getSpiceManager().execute(request, new ReverseUsageRequestListener(this, refundButton));
	}

	public void changeArrow(View view) {
		ImageView arrow = (ImageView) findViewById(R.id.ticketHistoryArrow);

		if (arrow.getDrawable() == this.getResources().getDrawable(R.drawable.arrow_down)) {
			arrow.setImageDrawable(getResources().getDrawable(R.drawable.arrow_up));
		} else {
			arrow.setImageDrawable(getResources().getDrawable(R.drawable.arrow_down));
		}

	}

	@Override
	public void onScanIntent(Intent intent) {
		if (ticketScanInput != null) {
			String data = intent.getStringExtra(DATA_STRING_TAG);

			if (data != null) {
				scannedTicket = data;
				ticketScanInput.setText(data);

				if(data.length() < 16 )
				{
					showErrorDialog("Invalid Ticket",scannedTicket);
				}
				else
				{
					if(data.length() == 17)
					{
						transferToSuperTicketActivity();
					}
					else {
						getTicketHistory(data);
					}
				}

			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ticket_history, menu);
		return true;
	}

	public void ticketScan(View view) {
		finish();
	}

	public void creditHistory(View view) {
		Intent intent = new Intent(this, TicketCreditHistoryActivity.class);
		startActivity(intent);
		finish();
	}

	private void populateListData(TicketLookupResponseDto ticketData) {
		listDataHeader = new ArrayList<TicketHistoryListHeader>();
		childData = new HashMap<TicketHistoryListHeader, Integer>();
		UserRole role = RuntimeSettings.getInstance().getUserRole();
		PassPhotoDto chkPhotoDto = RuntimeSettings.getInstance().getPassPhoto();
		boolean isPointPass = false;
		if( ticketData.getLookups().get(0).getVisualID().startsWith("41") ||  ticketData.getLookups().get(0).getVisualID().startsWith("61") || ticketData.getLookups().get(0).getVisualID().startsWith("71"))
		{
			isPointPass = true;
		}

		for(LookupDto lookup : ticketData.getLookups()) {
			Collection<UsageDto> usages;
			if(role == UserRole.ADMINISTRATOR || role == UserRole.SUPERVISOR) {
				usages = lookup.getAdminFilteredUsages();
			} else {
				Ride currentRide = RuntimeSettings.getInstance().getCurrentRide();
				List<Integer> acpIds = Arrays.asList(currentRide.getPrimaryAcpId(), currentRide.getSecondaryAcpId(), currentRide.getTertiaryAcpId(), currentRide.getOfflineAcpId());
				usages = lookup.getFilteredUsages(acpIds);
			}
			
			for(UsageDto usage : usages) {
				TicketHistoryListHeader listHeader = new TicketHistoryListHeader(lookupRideName(usage.getAcp()), DATE_FORMATTER.format(usage.getUseTime()), isPointPass, usage.getUsageId());
				childData.put(listHeader, usage.getPonits());
				listDataHeader.add(listHeader);
			}
		}
		
		if(listDataHeader.size() > 0) {
			noHistoryText.setVisibility(View.GONE);
			expListView.setVisibility(View.VISIBLE);
			// need to add 4 blank to the bottom so that everything is usable
			TicketHistoryListHeader blank1 = new TicketHistoryListHeader("", "", false, 0);
			listDataHeader.add(blank1);
			childData.put(blank1, 0);
			TicketHistoryListHeader blank2 = new TicketHistoryListHeader("", "", false, 0);
			listDataHeader.add(blank2);
			childData.put(blank2, 0);
			TicketHistoryListHeader blank3 = new TicketHistoryListHeader("", "", false, 0);
			listDataHeader.add(blank3);
			childData.put(blank3, 0);
			TicketHistoryListHeader blank4 = new TicketHistoryListHeader("", "", false, 0);
			listDataHeader.add(blank4);
			childData.put(blank4, 0);
		} else {
			if(role == UserRole.ADMINISTRATOR || role == UserRole.SUPERVISOR) {
				noHistoryText.setText(R.string.ticket_history_no_history_unfiltered);
			} else {
				noHistoryText.setText(R.string.ticket_history_no_history_filtered);
			}
			noHistoryText.setVisibility(View.VISIBLE);
			expListView.setVisibility(View.GONE);
		}
		
		buildListView();
	}
	
	private String lookupRideName(int acpId) {
		List<RideRotationDto> rideRotations = RuntimeSettings.getInstance().getRideRotations();
		for(RideRotationDto rotation : rideRotations) {
			for(RideDto ride : rotation.getRides()) {
				if(ride.getPrimaryAcpId() == acpId || ride.getSecondaryAcpId() == acpId || ride.getTertiaryAcpId() == acpId || ride.getOfflineAcpId() == acpId) {
					return ride.getName();
				}
			}
		}
		
		return "Unknown Ride";
	}

	private void setPointsRemaining(TicketLookupResponseDto result, int points) {
		pointsRemainingNumber.setText(Integer.toString(points));
		pointsRemainingNumber.setTextSize(40);
		pointsRemainingText.setTextSize(25);
		pointsRemainingText.setText(R.string.ticket_scanning_ticket_points_remaining);
		pointsRemainingText.setVisibility(View.VISIBLE);
		pointsRemainingNumber.setVisibility(View.VISIBLE);
	}
	
	private void setTimeRemaining(TicketDto ticket) {
		DateTime now = new DateTime();
		DateTime expiration = ticket.getExpiration();
		String timeRemainingText = "";
		if(expiration.getYear() == 1899) {
			timeRemainingText = "All Day Wristband";
		} else if(now.isAfter(expiration)) {
			timeRemainingText = "Ticket Expired";
		} else {
			Duration expirationDuration = new Duration(now, expiration);
			long days = expirationDuration.getStandardDays();
			long hours = expirationDuration.getStandardHours() % 24;
			long minutes = expirationDuration.getStandardMinutes() % 60;
			if(days > 0) {
				timeRemainingText += String.format("%s day(s) ", days);
			}
			if(hours > 0) {
				timeRemainingText += String.format("%s hour(s) ", hours);
			}
			if(minutes > 0) {
				timeRemainingText += String.format("%s minute(s)", minutes);
			}
		}
		
		pointsRemainingNumber.setText(timeRemainingText);
		pointsRemainingNumber.setTextSize(15);
		pointsRemainingText.setTextSize(15);
		pointsRemainingText.setText(R.string.ticket_scanning_ticket_time_remaining);
		pointsRemainingNumber.setVisibility(View.VISIBLE);
		pointsRemainingText.setVisibility(View.VISIBLE);
	}
	
	private class TicketHistoryRequestListener extends BaseGalaxyRequestListener<TicketLookupResponseDto> {

		public TicketHistoryRequestListener(MoaBaseActivity initiatingActivity) {
			super(initiatingActivity);
		}

		@Override
		public void handleSuccess(TicketLookupResponseDto result) {
				try{
					LookupDto firstLookup = result.getLookups().get(0);
					DebitCardDto debitCard = firstLookup.getDebitCard();
					TicketDto ticket = firstLookup.getTicket();
					if(firstLookup != null)
					{

						if((firstLookup.getVisualID().startsWith("41") || firstLookup.getVisualID().startsWith("61")||firstLookup.getVisualID().startsWith("71")) && debitCard != null) {
							setPointsRemaining(result, debitCard.getPointsBalance());
						} else if(ticket != null && ticket.getExpiration() != null) {
							setTimeRemaining(ticket);
						} else {
							pointsRemainingText.setVisibility(View.GONE);
							pointsRemainingNumber.setVisibility(View.GONE);
						}

						populateListData(result);
					}
					else
					{
						if(runtimeSettings.getSelectedSubTicket() != "")
						{
							showErrorDialog("Ticket History Not Found", subTicketVisualId);
						}
						else {
							showErrorDialog("Ticket History Not Found", scannedTicket);
						}
					}
				}
				catch(Exception ex)
				{

					if(runtimeSettings.getSelectedSubTicket() != "")
					{
						showErrorDialog("Ticket History Not Found", subTicketVisualId);
					}
					else {
						showErrorDialog("Ticket History Not Found", scannedTicket);
					}
				}


		}
		@Override
		public void onRequestFailure(SpiceException spiceException)
		{
			if(runtimeSettings.getSelectedSubTicket() != "")
			{
				showErrorDialog("Ticket History Not Found", subTicketVisualId);
			}
			else {
				showErrorDialog("Ticket History Not Found", scannedTicket);
			}
		}
	}
	
	private class ReverseUsageRequestListener extends BaseGalaxyRequestListener<ReverseUsageResponseDto> {
		private Button refundButton;
		
		public ReverseUsageRequestListener(MoaBaseActivity initiatingActivity, Button refundButton) {
			super(initiatingActivity);
			this.refundButton = refundButton;
		}

		@Override
		public void handleSuccess(ReverseUsageResponseDto result) {
			if(result.getReversalStatus() == 0) {
				refundButton.setText("Refunded");
			}
		}
		
		@Override
		protected void handleResponseErrors(ReverseUsageResponseDto result) {
			showErrorDialog("Communication Error", result.getErrorText());
		}

	}
}
