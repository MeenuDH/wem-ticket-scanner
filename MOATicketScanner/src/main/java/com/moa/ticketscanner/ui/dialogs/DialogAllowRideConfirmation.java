package com.moa.ticketscanner.ui.dialogs;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.moa.ticketscanner.R;
import com.moa.ticketscanner.ui.RideSelectionActivity;
import com.moa.ticketscanner.ui.TicketScanningActivity;

public class DialogAllowRideConfirmation extends Dialog implements
		View.OnClickListener {

	private TicketScanningActivity activity;
	private String message1;
	private String message2;
	private String message3;
	private Button cancel, confirm;
	private String leftButtonText;



	public DialogAllowRideConfirmation(TicketScanningActivity activity,
                                       String message1, String message2, String message3) {
		super(activity);
		this.activity = activity;
		this.message1 = message1;
		this.message2 = message2;
		this.message3 = message3;

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_allow_ride_confirmation);

		// set buttons
		confirm = (Button) findViewById(R.id.dialogRejectDismiss);
		confirm.setOnClickListener(this);

		cancel = (Button) findViewById(R.id.dialogCancel);
		cancel.setOnClickListener(this);
		TextView textView = (TextView) findViewById(R.id.dialogText);
		textView.setText(message1);
		TextView textView1 = (TextView) findViewById(R.id.dialogText1);
		textView1.setText(message2);
		TextView textView2 = (TextView) findViewById(R.id.dialogText2);
		textView2.setText(message3);
		textView2.setGravity(View.TEXT_ALIGNMENT_GRAVITY);
		this.setCancelable(false);


	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.dialogRejectDismiss:
			activity.confirmSelection();
			dismiss();
			break;
		case R.id.dialogCancel:
			activity.submitLock();
			dismiss();
			break;
		}
	}
}