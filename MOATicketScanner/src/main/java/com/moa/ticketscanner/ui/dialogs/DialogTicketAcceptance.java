package com.moa.ticketscanner.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.moa.ticketscanner.R;
import com.moa.ticketscanner.domain.AcceptanceAudio;
import com.moa.ticketscanner.settings.AppSettings;

public class DialogTicketAcceptance extends Dialog implements OnClickListener {

	private Activity activity;
	private Handler mHandler = new Handler();
	private AcceptanceAudio audio;
	private boolean forceDismiss = false;
	private boolean isChaperon = false;
	private String dialogText = getContext().getResources().getString(
			R.string.dialog_string_acceptance);
	private TextView dialogTextView;
	private TextView dialogChaperoneTextView;
	private android.view.View.OnClickListener dismissListener = this;
	private String chperoneType = "";

	public DialogTicketAcceptance(Activity activity, AcceptanceAudio audio,
			boolean forceDismiss, boolean isChaperonParam, String chaperoneTypeParam) {
		super(activity);
		this.activity = activity;
		this.audio = audio;
		this.forceDismiss = forceDismiss;
		this.isChaperon = isChaperonParam;
		this.chperoneType = chaperoneTypeParam;
	}

	public DialogTicketAcceptance(Activity activity, AcceptanceAudio audio) {
		this(activity, audio, false, false,"");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		if(isChaperon)
		{
			setContentView(R.layout.dialog_ticket_acceptance_chap);

		}
		else
		{
			setContentView(R.layout.dialog_ticket_acceptance);
		}
		dialogChaperoneTextView = (TextView) findViewById(R.id.dialogChaperoneText);
		dialogChaperoneTextView.setText(chperoneType);
		dialogTextView = (TextView) findViewById(R.id.dialogText);
		dialogTextView.setText(dialogText);

		// play sound
		MediaPlayer mp;
		if (audio == null) {
			mp = MediaPlayer.create(activity, R.raw.valid);
		} else {
			mp = MediaPlayer.create(activity, audio.getResourceId());
		}

		mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
		mp.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				mp.release();
			}

		});
		mp.start();

		if (forceDismiss) {
			Button dismissButton = (Button) findViewById(R.id.dialogRejectDismiss);
			dismissButton.setVisibility(View.VISIBLE);
			dismissButton.setOnClickListener(dismissListener);
		} else {
			mHandler.postDelayed(new Runnable() {
				public void run() {
					postWait();
				}
			}, AppSettings.ACCEPTED_SCREEN_TIMEOUT);
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_VOLUME_DOWN || keyCode == KeyEvent.KEYCODE_VOLUME_MUTE) {
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.dialogRejectDismiss:
			this.dismiss();

			break;
		}

	}

	public void setDialogText(String text) {
		dialogText = text;
	}

	public void setDismissListener(android.view.View.OnClickListener listener) {
		dismissListener = listener;
	}

	private void postWait() {
		this.dismiss();
	}
}