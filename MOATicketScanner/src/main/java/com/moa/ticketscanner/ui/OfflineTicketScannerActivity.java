package com.moa.ticketscanner.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.moa.ticketscanner.R;
import com.moa.ticketscanner.domain.AcceptanceAudio;
import com.moa.ticketscanner.domain.OfflineScan;
import com.moa.ticketscanner.dto.rides.RideDto;
import com.moa.ticketscanner.dto.rides.RideRotationDto;
import com.moa.ticketscanner.settings.AppSettings;
import com.moa.ticketscanner.settings.RuntimeSettings;
import com.moa.ticketscanner.ui.dialogs.DialogTicketAcceptance;
import com.moa.ticketscanner.ui.dialogs.DialogTicketRejection;
import com.moa.ticketscanner.ui.widgets.ScanInputPageFragment.ScanInputScanListener;
import com.moa.ticketscanner.ui.widgets.ScanInputView;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class OfflineTicketScannerActivity extends MoaBaseActivity {

	private static final String USER_ID = "userId";
	private static final String TICKET_NUMBER = "ticketNumber";
	private static final String RIDE_ID = "rideId";
	private RuntimeSettings runtimeSettings;
	private ScanInputView rideIdField;
	private TextView rideNameField;
	private ScanInputView userIdField;
	private ScanInputView ticketNumberField;
	private TextView cycleCounter;
	private Boolean currentRideIsValid = false;
	private int cycleCount = 0;
	private Boolean enableScans = true;
	private String currentScan = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_offline_ticket_scanner);
		runtimeSettings = RuntimeSettings.getInstance();

		ticketNumberField = (ScanInputView) findViewById(R.id.offlineTicketScanInput);
		cycleCounter = (TextView) findViewById(R.id.ticketScanningOfflineCycleCounterNumber);
		userIdField = (ScanInputView) findViewById(R.id.offlineUserIdScanInput);
		rideIdField = (ScanInputView) findViewById(R.id.offlineRideIdScanInput);
		rideNameField = (TextView) findViewById(R.id.ticketScanningOfflineRideName);

		// setup edit listener on rideIdField
		rideIdField.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				validateRideId();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

		ticketNumberField.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					submitTicket(false);
				}
				return false;
			}
		});
		
		userIdField.addScanListener(new ScanInputScanListener() {

			@Override
			public void onScanStarted() {
				currentScan = USER_ID;
			}

			@Override
			public void onScanStopped() {
				currentScan = "";
			}
			
		});
		
		ticketNumberField.addScanListener(new ScanInputScanListener() {

			@Override
			public void onScanStarted() {
				currentScan = TICKET_NUMBER;
			}

			@Override
			public void onScanStopped() {
				currentScan = "";
			}
			
		});
		
		rideIdField.addScanListener(new ScanInputScanListener() {

			@Override
			public void onScanStarted() {
				currentScan = RIDE_ID;
			}

			@Override
			public void onScanStopped() {
				currentScan = "";
			}
			
		});
	}

	@Override
	public void onResume() {
		super.onResume();
		if (runtimeSettings.getUserId() != null) {
			userIdField.setText(runtimeSettings.getUserId());
		}
		if (runtimeSettings.getCurrentRide() != null) {
			rideIdField.setText(runtimeSettings.getCurrentRide().getScanId());
		}

		// check to see if we are back online
		RuntimeSettings rs = RuntimeSettings.getInstance();
		ConnectivityManager conMngr = (ConnectivityManager) this.getSystemService(CONNECTIVITY_SERVICE);
		android.net.NetworkInfo wifi = conMngr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		if (wifi.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {
			rs.setOnline(true);
			// we are online so move to login
			runtimeSettings.setGatewaySessionId(null);
			runtimeSettings.setUserRole(null);
			runtimeSettings.setUserId(null);
			finish();
		}
		
		// Clear any active scans
		currentScan = "";
	}

	@Override
	public void onBackPressed() {
		// super.onBackPressed(); // Comment this super call to avoid calling
		// finish()
	}

	public void updateCounter() {
		cycleCounter.setText(String.valueOf(cycleCount));
	}

	public void incrementCounter() {
		cycleCount++;
		updateCounter();
	}

	public void resetCounter(View view) {
		if (AppSettings.debug)
			Log.d("WEM", "Resetting Cycle Counter");
		cycleCount = 0;
		updateCounter();
	}

	public void showLogoutConfirmation(View v) {
		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.MOATheme));
		alertBuilder.setTitle(R.string.ticket_scanning_logout_confirmation_title);
		alertBuilder.setMessage(R.string.ticket_scanning_logout_confirmation_text);
		alertBuilder.setPositiveButton(R.string.ticket_scanning_logout_confirmation_yes, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				logout();
			}
		});
		alertBuilder.setNegativeButton(R.string.ticket_scanning_logout_confirmation_no, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		alertBuilder.create().show();
	}

	public void logout() {
		cycleCount = 0;
		updateCounter();
		rideIdField.setText("");
		userIdField.setText("");
		ticketNumberField.setText("");
		currentRideIsValid = false;
		rideNameField.setText("");
	}

	public void validateRideId() {		
		String scanId = rideIdField.getText().toString();
		List<RideRotationDto> rideRotations = runtimeSettings.getRideRotations();
		for(RideRotationDto rotation : rideRotations) {
			for(RideDto ride : rotation.getRides()) {
				if (ride.getScanId().equals(scanId)) {
					rideNameField.setText(ride.getName());
					currentRideIsValid = true;
					return;
				}
			}
		}
		
		rideNameField.setText("Unknown Ride");
		currentRideIsValid = false;
	}

	public void submitTicket(boolean override) {
		validateRideId();
		String ticket = ticketNumberField.getText().toString();
		String userId = userIdField.getText().toString();
		// if we don't have a ride or user selected fail the ticket scan
		if (userId.length() < 2 && userId.length() > 9) {
			final DialogTicketRejection dialog = new DialogTicketRejection(this, "User Id Invalid", null, false);
			dialog.setDismissListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					enableScans = true;
					dialog.dismiss();
				}
			});

			enableScans = false;
			dialog.show();
		} else if (!currentRideIsValid) {
			final DialogTicketRejection dialog = new DialogTicketRejection(this, "Ride Id Invalid", null, false);
			dialog.setDismissListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					enableScans = true;
					dialog.dismiss();
				}
			});
			enableScans = false;
			dialog.show();
		}
		// validate ticket as best we can
		// make sure it's not empty
		else if (ticket.isEmpty()) {
			final DialogTicketRejection dialog = new DialogTicketRejection(this, "Invalid Ticket", null, false);
			dialog.setDismissListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					enableScans = true;
					dialog.dismiss();
				}
			});
			enableScans = false;
			dialog.show();
		}
		// make sure it doesnt' start with 0
		else if (ticket.charAt(0) == '0') {
			final DialogTicketRejection dialog = new DialogTicketRejection(this, "Invalid Ticket", null, false);
			dialog.setDismissListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					enableScans = true;
					dialog.dismiss();
				}
			});
			enableScans = false;
			dialog.show();
		}
		// make sure its 16 or 17 chars long
		else if (ticket.length() < 16) {
			final DialogTicketRejection dialog = new DialogTicketRejection(this, "Invalid Ticket", null, false);
			dialog.setDismissListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					enableScans = true;
					dialog.dismiss();
				}
			});
			enableScans = false;
			dialog.show();
		}
		// make sure its not a super ticket
		/*else if (ticket.length() != 16) {
			final DialogTicketRejection dialog = new DialogTicketRejection(this, "Invalid Ticket", null, false);
			dialog.setDismissListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					enableScans = true;
					dialog.dismiss();
				}
			});
			enableScans = false;
			dialog.show();
		}*/
		// else we are good to go
		else {
			// append ticket data to file
			OfflineScan scan = new OfflineScan();
			scan.setScanDate(new Date());
			scan.setScanId(rideIdField.getText().toString());
			scan.setScanOperator(userId);
			scan.setVisualId(ticket);
			addOfflineData(scan);
			incrementCounter();
			DialogTicketAcceptance dialog = new DialogTicketAcceptance(this, AcceptanceAudio.Valid);
			dialog.show();
		}
		// clear input
		ticketNumberField.setText("");

	}

	public void addOfflineData(OfflineScan scan) {
		File offlineModeFile = new File(getFilesDir().getPath() + "/" + AppSettings.OFFLINE_MODE_FILE_NAME);

		if (AppSettings.debug) {
			Log.d("WEM", "Offline Data File Path: " + offlineModeFile.getPath());
		}

		// if file does exist create it
		if (!offlineModeFile.exists()) {
			try {
				offlineModeFile.createNewFile();
				if (AppSettings.debug) {
					Log.d("WEM", "Creating Offline Data File");
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// open file and append to it
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(offlineModeFile, true));
			ObjectMapper jsonMapper = new ObjectMapper();
			String dataToWrite = jsonMapper.writeValueAsString(scan);
			bw.write(dataToWrite);
			bw.newLine();
			bw.flush();
			bw.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void onScanIntent(Intent intent) {

		String data = intent.getStringExtra(DATA_STRING_TAG);
		if (data != null && enableScans) {
			if(currentScan.equals(USER_ID)) {
				userIdField.setText(data);
			} else if(currentScan.equals(RIDE_ID)) {
				try {
				rideIdField.setText(data);
				} catch(Exception exc) {
					Log.d("tag", exc.toString());
				}
			} else if(currentScan.equals(TICKET_NUMBER)) {
				ticketNumberField.setText(data);
				submitTicket(false);
			} else if (userIdField.getText().toString().isEmpty()) {
				userIdField.setText(data);
			} else if (rideIdField.getText().toString().isEmpty()) {
				rideIdField.setText(data);
			} else if (ticketNumberField.getText().toString().isEmpty()) {
				ticketNumberField.setText(data);
				submitTicket(false);
			}
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.offline_ticket_scanner, menu);
		return true;
	}

}
