package com.moa.ticketscanner.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.widget.Toast;

import com.moa.ticketscanner.R;
import com.moa.ticketscanner.communication.BaseGalaxyRequestListener;

import com.moa.ticketscanner.communication.BaseMoaRequestListener;

import com.moa.ticketscanner.communication.GalaxyApiSpiceRequest;
import com.moa.ticketscanner.communication.MoaApiRequestDetails;
import com.moa.ticketscanner.communication.MoaApiSpiceRequest;
import com.moa.ticketscanner.communication.MoaSpiceService;

import com.moa.ticketscanner.dto.BaseGalaxyRequestDto;
import com.moa.ticketscanner.dto.HeaderDto;
import com.moa.ticketscanner.dto.configuration.NetworkStatusDto;
import com.moa.ticketscanner.dto.logon.TestValidateRequestDto;
import com.moa.ticketscanner.dto.logon.TestValidateResponseDto;
import com.moa.ticketscanner.settings.AppSettings;
import com.moa.ticketscanner.settings.RuntimeSettings;


import com.octo.android.robospice.SpiceManager;


import com.octo.android.robospice.persistence.exception.SpiceException;


import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class MoaBaseActivity extends FragmentActivity {
	private static final String SCANNER_ENABLE_PLUGIN = "ENABLE_PLUGIN";
	private static final String SCANNER_DISABLE_PLUGIN = "DISABLE_PLUGIN";
	private static final String SCANNER_EXTRA_PARAMETER = "com.motorolasolutions.emdk.datawedge.api.EXTRA_PARAMETER";
	private static final String SCANNER_INPUT_ACTION = "com.motorolasolutions.emdk.datawedge.api.ACTION_SCANNERINPUTPLUGIN";
	private static final String SCAN_INTENT_FILTER_NAME = "com.moa.ticketscanner.SCAN";
	protected final Context context = this;
	protected static final String DATA_STRING_TAG = "com.motorolasolutions.emdk.datawedge.data_string";
	protected static final String MSR_DATA_TAG = "com.motorolasolutions.emdk.datawedge.msr_data";
	private ReceiveScansReceiver scanReceiver = null;
	private ReceiveWifiStatus wirelessReceiver = null;
	private Boolean isScanReceiverRegistered = false;
	private Boolean forceMoveToOffline = false;
	private SpiceManager spiceManager = new SpiceManager(MoaSpiceService.class);

	private RuntimeSettings runtimeSettings =  RuntimeSettings.getInstance();


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		// Create and Register the Scan Receiver
		scanReceiver = new ReceiveScansReceiver();
		wirelessReceiver = new ReceiveWifiStatus();
	}

	@Override
	protected void onStart() {
		spiceManager.start(this);
		super.onStart();

	}

	@Override
	protected void onPause() {
		super.onPause();
		if (isScanReceiverRegistered) {
			unregisterReceiver(scanReceiver);
			unregisterReceiver(wirelessReceiver);
			isScanReceiverRegistered = false;
		}
	}
	public static String getMACAddress(Activity activity) {
		return ((WifiManager) activity.getSystemService(Context.WIFI_SERVICE)).getConnectionInfo().getMacAddress();
	}
	@Override
	protected void onResume() {
		super.onResume();
		runtimeSettings.setCurrentActivity(this.getClass());
		registerScanReceiver();
		registerWifiReceiver();
		// check for connectivity
		ConnectivityManager conMngr = (ConnectivityManager) this.getSystemService(CONNECTIVITY_SERVICE);
		android.net.NetworkInfo wifi = conMngr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		if (wifi.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {
			//Vivek - Work on Mac
			// Code Added For Release 1.2
			runtimeSettings.setDeviceId(getMACAddress(this));
			//checkNetworkConnection();
			runtimeSettings.setOnline(true);

			validateGatewaySession();
			if (AppSettings.debug)
				Log.d("WEM", "Resume Set Online");

		}
		// we are offline
		else {

			runtimeSettings.setOnline(false);
			//runtimeSettings.setServerReachable(false);
			if (AppSettings.debug)
				Log.d("WEM", "Resume Set Offline");
		}

		// if we are not online and we should finish in offline mode, do so
		if (!runtimeSettings.isOnline()  && runtimeSettings.getCurrentActivity() != OfflineTicketScannerActivity.class) {
			transferToActivity(OfflineTicketScannerActivity.class);
		}

		// if we are online and we should finish in online mode, do so
		if (runtimeSettings.isOnline()  && runtimeSettings.getCurrentActivity() == OfflineTicketScannerActivity.class) {
			transferToActivity(LoginActivity.class);
		}
		
		// Set volume to max
		AudioManager audio = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
		audio.setStreamVolume(AudioManager.STREAM_MUSIC, audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);

	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_VOLUME_DOWN || keyCode == KeyEvent.KEYCODE_VOLUME_MUTE) {
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	
	private void validateGatewaySession() {
		if(StringUtils.isNotBlank(runtimeSettings.getGatewaySessionId())) {
			HeaderDto header = HeaderDto.Create(context, TestValidateRequestDto.MESSAGE_TYPE);
			header.setSessionID(runtimeSettings.getGatewaySessionId());
			TestValidateRequestDto validationDto = new TestValidateRequestDto("0");
			BaseGalaxyRequestDto<TestValidateRequestDto> requestDto = new BaseGalaxyRequestDto<TestValidateRequestDto>(header, validationDto);
			GalaxyApiSpiceRequest<TestValidateResponseDto> request = new GalaxyApiSpiceRequest<TestValidateResponseDto>(TestValidateResponseDto.class, requestDto);
			
			getSpiceManager().execute(request, new TestValidateRequestListener(this));
		}
	}

	@Override
	protected void onStop() {
		spiceManager.shouldStop();
		//Vivek VPN Check
		//spiceManagerVpn.shouldStop();
		super.onStop();
	}

	protected void transferToActivity(Class<?> activityCls) {
		transferToActivity(activityCls, null);
	}

	protected void transferToActivity(Class<?> activityCls, Bundle extras) {
		Intent intent = new Intent(this, activityCls);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		if (extras != null) {
			intent.putExtras(extras);
		}
		startActivity(intent);
	}

	public SpiceManager getSpiceManager() {
		return spiceManager;
	}
	//Vivek VPN
	//public SpiceManager getSpiceManagerVpn() {
		//return spiceManagerVpn;
	//}
	protected void onScanIntent(Intent intent) {

	}
	
	protected void disableScanning() {
		Intent i = new Intent();
		i.setAction(SCANNER_INPUT_ACTION);
		i.putExtra(SCANNER_EXTRA_PARAMETER, SCANNER_DISABLE_PLUGIN);
		sendBroadcast(i);
	}
	
	protected void enableScanning() {
		Intent i = new Intent();
		i.setAction(SCANNER_INPUT_ACTION);
		i.putExtra(SCANNER_EXTRA_PARAMETER, SCANNER_ENABLE_PLUGIN);
		sendBroadcast(i);
	}

	public void showErrorDialog(String title, String message) {
		showErrorDialog(title, message, null);
	}
	
	public void showErrorDialog(String title, String message, OnClickListener clickListener) {
		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.MOATheme));
		alertBuilder.setTitle(title);
		alertBuilder.setMessage(message);
		alertBuilder.setCancelable(false);
		if(clickListener != null) {
			alertBuilder.setPositiveButton(R.string.login_dialog_ok, clickListener);
		} else {
			alertBuilder.setPositiveButton(R.string.login_dialog_ok, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
	
			});
		}
		alertBuilder.create().show();
	}

	public void showErrorDialogAndTransfer(String title, String message, final Class<?> activityCls) {
		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.MOATheme));
		alertBuilder.setTitle(title);
		alertBuilder.setMessage(message);
		alertBuilder.setCancelable(false);
		alertBuilder.setPositiveButton(R.string.login_dialog_ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				transferToActivity(activityCls);
			}

		});
		alertBuilder.create().show();
	}

	private void registerScanReceiver() {
		if (!isScanReceiverRegistered) {
			registerReceiver(scanReceiver, new IntentFilter(SCAN_INTENT_FILTER_NAME));
			isScanReceiverRegistered = true;
		}
	}

	public class ReceiveScansReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equalsIgnoreCase(SCAN_INTENT_FILTER_NAME)) {
				onScanIntent(intent);
			}
		}

	}

	private void registerWifiReceiver() {
		final IntentFilter filters = new IntentFilter();
		filters.addAction("android.net.wifi.STATE_CHANGE");
		registerReceiver(wirelessReceiver, filters);
	}

	public class ReceiveWifiStatus extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			NetworkInfo networkInfo = (NetworkInfo) intent.getExtras().get("networkInfo");
			if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
				if (networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {
					runtimeSettings.setOnline(true);
					if (runtimeSettings.getCurrentActivity() == OfflineTicketScannerActivity.class) {
						runtimeSettings.setGatewaySessionId(null);
						runtimeSettings.setUserRole(null);
						runtimeSettings.setUserId(null);
						transferToActivity(LoginActivity.class);
					}
				} else {
					runtimeSettings.setOnline(false);
					if (runtimeSettings.getCurrentActivity() != OfflineTicketScannerActivity.class) {
						transferToActivity(OfflineTicketScannerActivity.class);
					}
				}
			}

		}

	}

	public Boolean getForceMoveToOffline() {
		return forceMoveToOffline;
	}

	public void setForceMoveToOffline(Boolean forceMoveToOffline) {
		this.forceMoveToOffline = forceMoveToOffline;
	}

	private class TestValidateRequestListener extends BaseGalaxyRequestListener<TestValidateResponseDto> {
		public TestValidateRequestListener(MoaBaseActivity initiatingActivity) {
			super(initiatingActivity);
		}
		
		@Override
		public void handleSuccess(TestValidateResponseDto result) {

		}
		
		@Override
		protected void showCommunicationError() {
			//showErrorDialog("Error","Network Error!");
			super.showCommunicationError();
		}
		
	}

	public void checkNetworkConnection(){
		try{
			MoaApiRequestDetails moaRequestDetails = new MoaApiRequestDetails();
			moaRequestDetails.setHttpMethod(HttpMethod.GET);
			String uri = AppSettings.CHECK_CONNECTION;
			moaRequestDetails.setUri(uri);
			MoaApiSpiceRequest<NetworkStatusDto> request = new MoaApiSpiceRequest<NetworkStatusDto>(NetworkStatusDto.class,moaRequestDetails);
			spiceManager.execute(request,new NetworkConnectionListener(this));
		}
		catch(Exception ex){
			Log.d("VPN Check Chk",ex.getMessage());
		}
	}
	private class NetworkConnectionListener extends BaseMoaRequestListener<NetworkStatusDto>{
		public NetworkConnectionListener(MoaBaseActivity initiatingActivity) {
			super(initiatingActivity);
		}
		final CountDownLatch latch = new CountDownLatch(1);
		private RuntimeSettings runtimeSettings1 =  RuntimeSettings.getInstance();
		@Override
		public void handleSuccess(NetworkStatusDto result) {
			try{
				latch.countDown();
				latch.await();
				runtimeSettings1.setServerReachable(true);
				Log.d("VPN Check MoaBase", "Network Connection Successful");
			}
			catch (InterruptedException ex){
				Log.d("VPN Check Catch Suc",ex.getMessage());
			}

		}
		@Override
		public void onRequestFailure(SpiceException spiceException) {


			try{
				latch.countDown();
				latch.await();
				runtimeSettings1.setServerReachable(false);
				Throwable cause = spiceException.getCause();
				if(cause instanceof HttpClientErrorException) {
					HttpClientErrorException exception = (HttpClientErrorException) cause;
					if(exception.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
						handleResourceNotFoundError(exception);
					}
				} else {
					Log.d("VPN Check MoaBase","Network Error - On Request Failure");
					showCommunicationError();
				}
			}
			catch (InterruptedException ex){
				Log.d("VPN Check Catch ReqF",ex.getMessage());
			}


		}
		@Override
		protected void handleResourceNotFoundError(HttpClientErrorException exception) {
			try{

				latch.countDown();
				latch.await();
				runtimeSettings1.setServerReachable(false);
				showCommunicationError();
			}
			catch (InterruptedException ex){
				Log.d("VPN Check Catch Res",ex.getMessage());
			}

		}
		@Override
		protected void showCommunicationError() {
			runtimeSettings1.setServerReachable(false);
			showErrorDialog("VPN Check MoaBase","Network Error! Try again Later");
		}


	}



}
