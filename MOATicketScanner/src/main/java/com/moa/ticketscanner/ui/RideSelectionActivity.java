package com.moa.ticketscanner.ui;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.Filter.FilterListener;
import android.widget.GridView;

import com.moa.ticketscanner.R;
import com.moa.ticketscanner.communication.BaseGalaxyRequestListener;
import com.moa.ticketscanner.communication.BaseMoaRequestListener;
import com.moa.ticketscanner.communication.GalaxyApiSpiceRequest;
import com.moa.ticketscanner.communication.MoaApiRequestDetails;
import com.moa.ticketscanner.communication.MoaApiSpiceRequest;
import com.moa.ticketscanner.domain.Ride;
import com.moa.ticketscanner.domain.RideImageMapping;
import com.moa.ticketscanner.domain.UserRole;
import com.moa.ticketscanner.dto.BaseGalaxyRequestDto;
import com.moa.ticketscanner.dto.HeaderDto;
import com.moa.ticketscanner.dto.configuration.DeviceInfoDto;
import com.moa.ticketscanner.dto.logon.LogonRequestDto;
import com.moa.ticketscanner.dto.logon.LogonResponseDto;
import com.moa.ticketscanner.dto.rides.RideDto;
import com.moa.ticketscanner.dto.rides.RideRotationDto;
import com.moa.ticketscanner.ridegrid.RidesGridViewAdapter;
import com.moa.ticketscanner.settings.AppSettings;
import com.moa.ticketscanner.settings.RuntimeSettings;
import com.moa.ticketscanner.ui.dialogs.DialogRideSelectionConfirmation;
import com.moa.ticketscanner.ui.widgets.ScanInputView;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;

import android.app.AlertDialog;
import org.springframework.http.HttpMethod;

import java.util.ArrayList;
import java.util.List;

public class RideSelectionActivity extends MoaBaseActivity {

	private String userId;
	private String userPassword;
	private UserRole userRole;
	private GridView ridesGrid;
	private ArrayList<Ride> gridArray = new ArrayList<Ride>();
	private RidesGridViewAdapter customGridAdapter;
	private int currentRide = -1;
	private Ride selectedRide;
	//private EditText searchField;
	private ScanInputView searchView;
	private boolean wasAutoSelected = false;
	private boolean lockOnLogon = false;
	private RuntimeSettings settings = RuntimeSettings.getInstance();
	private int logOnAttempts = 0;
	SpiceManager spiceManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ride_selection_activity);
		initializeFields();

		buildRideGrid(RuntimeSettings.getInstance().getRideRotations());
		spiceManager = super.getSpiceManager();
		initializeFieldSearch();

		// If the user is not a team lead or supervisor, honor the locked state
		if (userRole == UserRole.USER && settings.isRideLocked()) {
			selectedRide = settings.getCurrentRide();
			lockOnLogon = true;
			logOn();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ride_selection_acitivity, menu);
		return true;
	}

	public void submitLock(View view) {
		selectRide(true);
	}

	public void submit(View viw) {
		selectRide(false);
	}

	public void buildRideSelectDialog() {
		// build dialog and show
		String message = "Select:\n" + selectedRide.getRideName() + "?";

		DialogRideSelectionConfirmation dialog = new DialogRideSelectionConfirmation(this, message,
				selectedRide.getRideImage());

		dialog.show();
	}

	// gets called from popup this is were validation and api calls to login
	// should take place
	public void confirmSelection() {
		if (AppSettings.debug)
			Log.d("WEM", "Ride Selection Confirmed");

		logOn();
	}

	@Override
	public void onScanIntent(Intent intent) {
		if (searchView != null) {
			String data = intent.getStringExtra(DATA_STRING_TAG);
			if (data != null) {
				searchView.setText(data);
			}
		}
	}

	private void selectRide(boolean lock) {
		if (currentRide == -1) {
			DialogRideSelectionConfirmation dialog = new DialogRideSelectionConfirmation(this, "Please Select A Ride",
					"Continue");
			dialog.show();
		} else {
			buildRideSelectDialog();
		}
		lockOnLogon = lock;
	}

	private void logOn() {
		// create header
		HeaderDto headerDto = HeaderDto.Create(context, LogonRequestDto.MESSAGE_TYPE);

		// create logonBody
		LogonRequestDto logonBody = new LogonRequestDto();
		logonBody.setFacility(selectedRide.getFacilityId());
		logonBody.setOperation(selectedRide.getOperationId());
		logonBody.setUsername(userId);
		logonBody.setPassword(userPassword);

		if (logOnAttempts == 0) {
			logonBody.setACP(selectedRide.getPrimaryAcpId());
		} else if (logOnAttempts == 1) {
			logonBody.setACP(selectedRide.getSecondaryAcpId());
		} else if (logOnAttempts == 2) {
			logonBody.setACP(selectedRide.getTertiaryAcpId());
		} else {
			throw new RuntimeException("Too many failed logons with no error handling.");
		}

		GalaxyApiSpiceRequest<LogonResponseDto> request = new GalaxyApiSpiceRequest<LogonResponseDto>(
				LogonResponseDto.class, new BaseGalaxyRequestDto<LogonRequestDto>(headerDto, logonBody));
		getSpiceManager().execute(request, new LogonRequestListener(this));
	}

	private void initializeFieldSearch() {
		searchView.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
				// When user changed the Text
				customGridAdapter.getFilter().filter(cs, new FilterListener() {

					@Override
					public void onFilterComplete(int count) {
						// if we have only one ride then auto select it
						if (customGridAdapter.getCount() == 1) {
							currentRide = 0;
							selectedRide = customGridAdapter.getItem(0);
							selectedRide.setSelected(true);
							wasAutoSelected = true;
							ridesGrid.setVisibility(View.VISIBLE);
						}
						// if we had auto-selected but now we have more results
						// de-select (for safety)
						else {
							ridesGrid.setVisibility(userRole == UserRole.USER ? View.GONE : View.VISIBLE);
							if (wasAutoSelected) {
								currentRide = -1;
								for (int i = 0; i < customGridAdapter.getCount(); i++) {
									customGridAdapter.getItem(i).setSelected(false);
								}
								wasAutoSelected = false;
							}
						}
					}

				});
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
			}

			@Override
			public void afterTextChanged(Editable arg0) {

			}
		});
	}

	private void buildRideGrid(List<RideRotationDto> rideRotations) {
		if (userRole == UserRole.ADMINISTRATOR || userRole == UserRole.SUPERVISOR) {
			ridesGrid.setVisibility(View.VISIBLE);
			searchView.setHint(R.string.ride_selection_scan_search_ride_label_admin);
		} else {
			searchView.setHint(R.string.ride_selection_scan_search_ride_label_user);
			ridesGrid.setVisibility(View.GONE);
		}

		for (RideRotationDto rotation : rideRotations) {
			for (RideDto rideDto : rotation.getRides()) {
				Integer imageId = RideImageMapping.getRideImageId(rideDto.getId());
				if (imageId == null) {
					imageId = R.drawable.login_logo;
				}
				Bitmap image = BitmapFactory.decodeResource(getResources(), imageId);
				Ride ride = new Ride(rideDto.getName(), image);
				ride.setFacilityId(rideDto.getFacilityId());
				ride.setOperationId(rideDto.getOperationId());
				ride.setPrimaryAcpId(rideDto.getPrimaryAcpId());
				ride.setSecondaryAcpId(rideDto.getSecondaryAcpId());
				ride.setTertiaryAcpId(rideDto.getTertiaryAcpId());
				ride.setOfflineAcpId(rideDto.getOfflineAcpId());
				ride.setScanId(rideDto.getScanId());
				ride.setRideRules(rideDto.getRideRules());
				gridArray.add(ride);
			}
		}
		
		gridArray.add(new Ride("", null, false));
		gridArray.add(new Ride("", null, false));

		customGridAdapter = new RidesGridViewAdapter(this, R.layout.partial_ride_selection_custom_grid_view_element,
				gridArray, userRole);

		ridesGrid.setAdapter(customGridAdapter);
		ridesGrid.setChoiceMode(GridView.CHOICE_MODE_SINGLE);

		ridesGrid.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int position, long lng) {
				Ride selection = (Ride) adapter.getItemAtPosition(position);
				if(!selection.isSelectable()) {
					return;
				}
				
				// remove last selected highlight
				if (currentRide > -1) {
					((Ride) adapter.getItemAtPosition(currentRide)).setSelected(false);
					customGridAdapter.notifyDataSetChanged();
				}
				currentRide = position;
				selectedRide = selection;
				selectedRide.setSelected(true);
				customGridAdapter.notifyDataSetChanged();
			}
		});
	}

	private void initializeFields() {
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			userId = extras.getString(AppSettings.USER_ID_KEY);
			userPassword = extras.getString(AppSettings.USER_PASSWORD_KEY);
			userRole = UserRole.valueOf(extras.getString(AppSettings.USER_ROLE_KEY));
		}
		logOnAttempts = 0;
		((Button) this.findViewById(R.id.submitLock)).setVisibility(userRole == UserRole.USER ? View.GONE : View.VISIBLE);
		ridesGrid = (GridView) this.findViewById(R.id.ridesGrid);
		searchView = (ScanInputView) this.findViewById(R.id.rideSearchScanInput);
	}
	
	private void failLogon(String explanation) {
		logOnAttempts = 0;
		showErrorDialog("Logon Failed", explanation, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				transferToActivity(LoginActivity.class);
				finish();
			}
		});
	}

	private class LogonRequestListener extends BaseGalaxyRequestListener<LogonResponseDto> {

		public LogonRequestListener(MoaBaseActivity initiatingActivity) {
			super(initiatingActivity);
		}

		@Override
		public void handleSuccess(LogonResponseDto result) {
			settings.setCurrentRide(selectedRide);
			settings.setRideLocked(lockOnLogon);
			settings.setGatewaySessionId(result.getHeader().getSessionID());
			//Vivek Add Code to Upload The Device Info
			uploadDeviceInfo();
			transferToActivity(TicketScanningActivity.class);
		}

		@Override
		protected void handleInvalidLogonError(LogonResponseDto result) {


			if(result.getErrorCode().equalsIgnoreCase("111")) {
				failLogon(result.getErrorText());
			} else {
				logOnAttempts++;

				if (logOnAttempts <= 2) {
					logOn();
				} else {
					//Vivek
					//failLogon("All ACP's already in use.");
					failLogon(result.getErrorText());
				}
			}
			
		}
		@Override
		protected void showCommunicationError() {
			ShowAlert("Network Error! Try again later!");
		}
	}

	//Upload the Device Info
	public void uploadDeviceInfo(){
		try{
			DeviceInfoDto deviceInfoDto = new DeviceInfoDto();
			deviceInfoDto.setDeviceMacId(settings.getDeviceId());
			deviceInfoDto.setRideId(settings.getCurrentRide().getScanId());
			deviceInfoDto.setUserId(settings.getUserId());
			Log.d("DeviceInfo MacId",deviceInfoDto.getDeviceMacId());
			Log.d("DeviceInfo RideId",deviceInfoDto.getRideId());
			Log.d("DeviceInfo UserId",deviceInfoDto.getUserId());
			String uri = AppSettings.UPLOAD_DEVICE_INFO_URI + deviceInfoDto.getUserId();
			MoaApiRequestDetails requestDetails = new MoaApiRequestDetails();
			requestDetails.setHttpMethod(HttpMethod.POST);
			requestDetails.setUri(uri);
			requestDetails.setRequestBody(deviceInfoDto);
			MoaApiSpiceRequest<Object> request = new MoaApiSpiceRequest<Object>(Object.class, requestDetails);
			spiceManager.execute(request,new DeviceInfoListener(this));
		}
		catch(Exception ex)
		{
			Log.d("DeviceInfoError","Entered Catch Code");

		}
	}
	private void ShowAlert(String test)
	{
		AlertDialog alertDialog = new AlertDialog.Builder(RideSelectionActivity.this).create();
		alertDialog.setTitle("Alert");
		alertDialog.setMessage(test);
		alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						transferToActivity(LoginActivity.class);
						dialog.dismiss();
					}
				});
		alertDialog.show();


	}
	private class DeviceInfoListener extends BaseMoaRequestListener<Object> {
		public DeviceInfoListener(MoaBaseActivity initiatingActivity) {
			super(initiatingActivity);
		}
		@Override
		public void handleSuccess(Object result) {
			Log.d("DeviceInfo","Device Info Uploaded");
			//ShowAlert("DeviceInfoSuccess");
		}
		@Override
		public void onRequestFailure(SpiceException spiceException) {
			Log.d("DeviceInfo","Device Info not Uploaded");
			//ShowAlert("DeviceInfoFailed");
			Log.d("DeviceInfoError","Communication Error");
			//ShowAlert(spiceException.getMessage());
		}
	}
}
