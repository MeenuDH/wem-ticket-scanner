package com.moa.ticketscanner.ui;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.moa.ticketscanner.R;
import com.moa.ticketscanner.communication.BaseGalaxyRequestListener;
import com.moa.ticketscanner.communication.GalaxyApiSpiceRequest;
import com.moa.ticketscanner.creditHistoryList.CreditHistoryListItem;
import com.moa.ticketscanner.creditHistoryList.CreditHistoryListViewAdapter;
import com.moa.ticketscanner.domain.Ride;
import com.moa.ticketscanner.domain.UserRole;
import com.moa.ticketscanner.dto.BaseGalaxyRequestDto;
import com.moa.ticketscanner.dto.HeaderDto;
import com.moa.ticketscanner.dto.rides.RideDto;
import com.moa.ticketscanner.dto.rides.RideRotationDto;
import com.moa.ticketscanner.dto.tickets.LookupDto;
import com.moa.ticketscanner.dto.tickets.TicketLookupRequestDto;
import com.moa.ticketscanner.dto.tickets.TicketLookupResponseDto;
import com.moa.ticketscanner.dto.tickets.UsageDto;
import com.moa.ticketscanner.settings.RuntimeSettings;
import com.moa.ticketscanner.ui.widgets.ScanInputView;
import com.octo.android.robospice.persistence.exception.SpiceException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class TicketCreditHistoryActivity extends MoaBaseActivity {
	private BaseAdapter listAdapter;
	private ListView listView;
	private List<CreditHistoryListItem> creditHistory;
	private ScanInputView ticketScanInput;
	private TextView noHistoryText;
	private static DateFormat DATE_FORMATTER = new SimpleDateFormat("MM-dd-yyyy @ hh:mm a");
	private String scannedTicket;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ticket_credit_history);

		ActionBar actionBar = getActionBar();
		actionBar.setTitle("Credit History");

		listView = (ListView) findViewById(R.id.creditHistoryListView);
		ticketScanInput = (ScanInputView) findViewById(R.id.creditHistoryScanInput);
		noHistoryText = (TextView) findViewById(R.id.ticketCreditNoHistory);
		
		// setup done button for ticket
		ticketScanInput.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					scannedTicket = v.getText().toString();
					if(v.getText().toString().length() < 16)
					{
						showErrorDialog("Invalid Ticket",scannedTicket);
					}
					else
					{
						if(v.getText().toString().length() == 17)
						{
							showErrorDialog("Credit History Not Found", scannedTicket);
						}
						else
						{
							getCreditHistory(v.getText().toString());
						}
					}

				}
				return false;
			}
		});
	}
	
	@Override
	public void onScanIntent(Intent intent) {
		if (ticketScanInput != null) {
			String data = intent.getStringExtra(DATA_STRING_TAG);
			if (data != null) {
				scannedTicket = data;
				ticketScanInput.setText(data);
				if( data.length() < 16)
				{
					showErrorDialog("Invalid Ticket",scannedTicket);
				}
				else
				{
					if(data.length() == 17)
					{
						showErrorDialog("Credit History Not Found", scannedTicket);
					}
					else
					{
						getCreditHistory(data);
					}

				}

			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ticket_credit_history, menu);
		return true;
	}

	public void ticketScan(View view) {
		finish();
	}

	public void ticketHistory(View view) {
		Intent intent = new Intent(this, TicketHistoryActivity.class);
		startActivity(intent);
		finish();
	}
	
	private void getCreditHistory(String ticketId) {
		HeaderDto headerDto = HeaderDto.Create(context, TicketLookupRequestDto.MESSAGE_TYPE);
		headerDto.setSessionID(RuntimeSettings.getInstance().getGatewaySessionId());
		TicketLookupRequestDto requestBody = new TicketLookupRequestDto();
		requestBody.setVisualID(ticketId);
		GalaxyApiSpiceRequest<TicketLookupResponseDto> request = 
				new GalaxyApiSpiceRequest<TicketLookupResponseDto>(TicketLookupResponseDto.class, new BaseGalaxyRequestDto<TicketLookupRequestDto>(headerDto, requestBody));
		getSpiceManager().execute(request, new TicketCreditHistoryRequestListener(this));
	}
	
	private void populateListData(TicketLookupResponseDto ticketData) {
		creditHistory = new ArrayList<CreditHistoryListItem>();
		UserRole role = RuntimeSettings.getInstance().getUserRole();
		
		for(LookupDto lookup : ticketData.getLookups()) {
			Collection<UsageDto> usages;
			if(role == UserRole.ADMINISTRATOR || role == UserRole.SUPERVISOR) {
				usages = lookup.getAdminFilteredReversals();
			} else {
				Ride currentRide = RuntimeSettings.getInstance().getCurrentRide();
				List<Integer> acpIds = Arrays.asList(currentRide.getPrimaryAcpId(), currentRide.getSecondaryAcpId(), currentRide.getTertiaryAcpId(),currentRide.getOfflineAcpId());
				usages = lookup.getFilteredReversals(acpIds);
			}
			
			for(UsageDto usage : usages) {
				String pointsDateText = String.format("%s pts refunded for %s", usage.getPonits(), DATE_FORMATTER.format(usage.getUseTime()));
				CreditHistoryListItem item = new CreditHistoryListItem(lookupRideName(usage.getAcp()), pointsDateText);
				creditHistory.add(item);
			}
		}
		
		if(creditHistory.size() > 0) {
			// Add empty item for scrolling
			creditHistory.add(new CreditHistoryListItem("", ""));
			listView.setVisibility(View.VISIBLE);
			noHistoryText.setVisibility(View.GONE);
		} else {
			if(role == UserRole.ADMINISTRATOR || role == UserRole.SUPERVISOR) {
				noHistoryText.setText(R.string.ticket_credit_no_history_unfiltered);
			} else {
				noHistoryText.setText(R.string.ticket_credit_no_history_filtered);
			}
			noHistoryText.setVisibility(View.VISIBLE);
			listView.setVisibility(View.GONE);
		}
		
		buildListView();
	}
	
	private void buildListView() {
		listAdapter = new CreditHistoryListViewAdapter(this, creditHistory);
		listView.setAdapter(listAdapter);
	}
	
	private String lookupRideName(int acpId) {
		List<RideRotationDto> rideRotations = RuntimeSettings.getInstance().getRideRotations();
		for(RideRotationDto rotation : rideRotations) {
			for(RideDto ride : rotation.getRides()) {
				if(ride.getPrimaryAcpId() == acpId || ride.getSecondaryAcpId() == acpId || ride.getTertiaryAcpId() == acpId || ride.getOfflineAcpId() == acpId ) {
					return ride.getName();
				}
			}
		}
		
		return "Unknown Ride";
	}

	public void prepareList() {

		creditHistory = new ArrayList<CreditHistoryListItem>();
		creditHistory.add(new CreditHistoryListItem("Justin's Ride", "20 pts refunded 01/01/2014 @ 10:00:00 AM"));
		creditHistory.add(new CreditHistoryListItem("Tim's Ride", "20 pts refunded 01/01/2014 @ 10:00:00 AM"));
		creditHistory.add(new CreditHistoryListItem("Jerry's Ride", "20 pts refunded 01/01/2014 @ 10:00:00 AM"));
		creditHistory.add(new CreditHistoryListItem("Scout's Ride", "20 pts refunded 01/01/2014 @ 10:00:00 AM"));
		creditHistory.add(new CreditHistoryListItem("Nathan's Ride", "20 pts refunded 01/01/2014 @ 10:00:00 AM"));
		creditHistory.add(new CreditHistoryListItem("Jordan's Ride", "20 pts refunded 01/01/2014 @ 10:00:00 AM"));
		creditHistory.add(new CreditHistoryListItem("Nadia's Ride", "20 pts refunded 01/01/2014 @ 10:00:00 AM"));
		creditHistory.add(new CreditHistoryListItem("Ned's Ride", "20 pts refunded 01/01/2014 @ 10:00:00 AM"));
		creditHistory.add(new CreditHistoryListItem("Joffery's Ride", "20 pts refunded 01/01/2014 @ 10:00:00 AM"));
		
		// add a blank one so we can read everything
		creditHistory.add(new CreditHistoryListItem("", ""));

	}
	
	private class TicketCreditHistoryRequestListener extends BaseGalaxyRequestListener<TicketLookupResponseDto> {

		public TicketCreditHistoryRequestListener(MoaBaseActivity initiatingActivity) {
			super(initiatingActivity);
		}

		@Override
		public void handleSuccess(TicketLookupResponseDto result) {
			try{
				LookupDto firstLookup = result.getLookups().get(0);
				if(firstLookup != null)
				{
					populateListData(result);
				}
				else
				{
					showErrorDialog("Credit History Not Found", scannedTicket);
				}
			}
			catch(Exception ex)
			{
				showErrorDialog("Credit History Not Found", scannedTicket);
			}
		}
		@Override
		public void onRequestFailure(SpiceException spiceException)
		{
			showErrorDialog("Credit History Not Found", scannedTicket);
		}
	}
}
