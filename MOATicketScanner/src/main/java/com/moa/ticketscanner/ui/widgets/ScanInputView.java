package com.moa.ticketscanner.ui.widgets;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView.OnEditorActionListener;

import com.moa.ticketscanner.R;
import com.moa.ticketscanner.ui.widgets.ScanInputPageFragment.ScanInputScanListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ScanInputView extends LinearLayout {

	/**
	 * The pager widget, which handles animation and allows swiping horizontally
	 * to access previous and next wizard steps.
	 */
	private ViewPager mPager;

	/**
	 * The pager adapter, which provides the pages to the view pager widget.
	 */
	private ScreenSlidePagerAdapter mPagerAdapter;
	private List<ScanInputPageFragment> _fragments = new ArrayList<ScanInputPageFragment>();
	private List<ScanInputScanListener> _scanListeners = new ArrayList<ScanInputScanListener>();
	private List<TextWatcher> _textChangeListeners = new ArrayList<TextWatcher>();
	private OnEditorActionListener _editorActionListener = null;
	private int _inputType = InputType.TYPE_CLASS_TEXT;
	private String _inputText;
	
	public ScanInputView(Context context, AttributeSet attrs) {
		super(context, attrs);

		int backgroundValue = attrs.getAttributeResourceValue(
				"http://schemas.android.com/apk/res/android", "background",
				R.drawable.left_bottom_blue_bar);
		int hintValue = attrs.getAttributeResourceValue(
				"http://schemas.android.com/apk/res/android", "hint",
				R.string.default_hint);
		int textAppearenceValue = attrs.getAttributeResourceValue(
				"http://schemas.android.com/apk/res/android", "textAppearance",
				R.style.EntryText);
		int hintColorValue = attrs.getAttributeResourceValue(
				"http://schemas.android.com/apk/res/android", "textColorHint",
				R.color.White);
		int heightValue = attrs.getAttributeResourceValue(
				"http://schemas.android.com/apk/res/android", "layout_height",
				LayoutParams.WRAP_CONTENT);
		int widthValue = attrs.getAttributeResourceValue(
				"http://schemas.android.com/apk/res/android", "layout_width",
				LayoutParams.WRAP_CONTENT);
		boolean alignRightValue = attrs.getAttributeBooleanValue(
				"http://schemas.android.com/apk/res/android",
				"layout_alignParentRight", false);

		ScanInputParameters viewParams = new ScanInputParameters(
				backgroundValue, hintValue, textAppearenceValue, heightValue,
				widthValue, hintColorValue, alignRightValue);

		setOrientation(LinearLayout.VERTICAL);
		setGravity(Gravity.CENTER_VERTICAL);

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.view_scan_input, this, true);
		setBackgroundColor(getResources().getColor(R.color.White));
		mPager = (ViewPager) findViewWithTag("PagerTag");
		mPager.setId(generateMyViewId());
		mPagerAdapter = new ScreenSlidePagerAdapter(context,
				((FragmentActivity) context).getSupportFragmentManager(),
				viewParams);
		mPager.setAdapter(mPagerAdapter);
		mPager.setBackgroundColor(getResources().getColor(R.color.White));
		android.view.ViewGroup.LayoutParams params = mPager.getLayoutParams();
		params.height = 80;
	}

	private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);

	public static int generateMyViewId() {
		for (;;) {
			final int result = sNextGeneratedId.get();
			// aapt-generated IDs have the high byte nonzero; clamp to the range
			// under that.
			int newValue = result + 1;
			if (newValue > 0x00FFFFFF)
				newValue = 1; // Roll over to 1, not 0.
			if (sNextGeneratedId.compareAndSet(result, newValue)) {
				return result;
			}
		}
	}

	public ScanInputView(Context context) {
		this(context, null);
	}

	public void setText(String text) {
		_inputText = text;
		for (ScanInputPageFragment fragment : _fragments) {
			fragment.setText(text);
		}
	}

	public String getText() {
		ScanInputPageFragment textFragment = _fragments.get(1);
		return textFragment.getText();
	}
	
	public void setInputType(int inputType) {
		_inputType = inputType;
	}

	public void addScanListener(
			ScanInputPageFragment.ScanInputScanListener listener) {
		_scanListeners.add(listener);

		if (_fragments.size() < 1) {
			return;
		}

		ScanInputPageFragment buttonFragment = _fragments.get(0);
		if (buttonFragment != null) {
			buttonFragment.addScanListener(listener);
		}
	}

	public void setOnEditorActionListener(OnEditorActionListener listener) {
		_editorActionListener = listener;
		
		if (_fragments.size() < 2) {
			return;
		}
		
		ScanInputPageFragment textFragment = _fragments.get(1);
		textFragment.setOnEditorActionListener(listener);
	}
	
	public void setHint(int hint) {
		if(mPagerAdapter != null) {
			mPagerAdapter.setHint(hint);
		}
	}
	
	public void addTextChangedListener(TextWatcher watcher) {
		_textChangeListeners.add(watcher);
		if (_fragments.size() < 2) {
			return;
		}
		
		ScanInputPageFragment textFragment = _fragments.get(1);
		textFragment.addTextChangedListener(watcher);
	}
	
	/**
	 * A simple pager adapter that represents 5 {@link ScreenSlidePageFragment}
	 * objects, in sequence.
	 */
	private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
		private ScanInputParameters _params;
		private Context _context;

		public ScreenSlidePagerAdapter(Context context, FragmentManager fm,
				ScanInputParameters params) {
			super(fm);
			_params = params;
			_context = context;
		}
		
		public void setHint(int hint) {
			_params.setHint(hint);
		}

		@Override
		public Fragment getItem(int position) {
			ScanInputPageFragment fragment = ScanInputPageFragment.create(
					position, _context, _params);
			if(position == 0) {
				for (ScanInputScanListener listener : _scanListeners) {
					fragment.addScanListener(listener);
				}
			} else {
				for(TextWatcher watcher : _textChangeListeners) {
					fragment.addTextChangedListener(watcher);
				}
				fragment.setOnEditorActionListener(_editorActionListener);
				fragment.addTextChangedListener(new TextWatcher() {

					@Override
					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
						// No-op
					}

					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count) {
						// No-op
					}

					@Override
					public void afterTextChanged(Editable s) {
						if(_fragments.size() > 1) {
							String value = s.toString();
							ScanInputPageFragment buttonFragment = _fragments.get(0);
							buttonFragment.setText(value);
						}
						
					}
					
				});
			}
			fragment.setText(_inputText);
			fragment.setInputType(_inputType);
			_fragments.add(position, fragment);

			return fragment;
		}

		@Override
		public int getCount() {
			return 2;
		}

		@Override
		public void startUpdate(ViewGroup container) {
			super.startUpdate(container);
			Intent i2 = new Intent();
			i2.setAction("com.motorolasolutions.emdk.datawedge.api.ACTION_SOFTSCANTRIGGER");
			i2.putExtra(
					"com.motorolasolutions.emdk.datawedge.api.EXTRA_PARAMETER",
					"STOP_SCANNING");
			_context.sendBroadcast(i2);
		}
	}

}