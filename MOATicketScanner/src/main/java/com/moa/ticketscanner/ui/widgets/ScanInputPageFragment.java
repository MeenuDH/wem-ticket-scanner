package com.moa.ticketscanner.ui.widgets;

import android.content.Context;
import android.content.Intent;
import android.graphics.LightingColorFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.moa.ticketscanner.R;

import java.util.ArrayList;
import java.util.List;

public class ScanInputPageFragment extends Fragment {
    /**
     * The argument key for the page number this fragment represents.
     */
    public static final String ARG_PAGE = "page";
    public static final String ARG_SCAN_PARAMS = "scanparams";

    /**
     * The fragment's page number, which is set to the argument value for {@link #ARG_PAGE}.
     */
    private int _pageNumber;
    private boolean isButton = true;
    private ScanInputParameters _params;
    private Context _context;
    private TextView _inputView;
    private List<ScanInputScanListener> _scanListeners = new ArrayList<ScanInputScanListener>();
    private List<TextWatcher> _textChangeListeners = new ArrayList<TextWatcher>();
    private OnEditorActionListener _editorActionListener = null;
    private int _inputType = -1;
    private String _inputText;
    
    /**
     * Factory method for this fragment class. Constructs a new fragment for the given page number.
     */
    public static ScanInputPageFragment create(int pageNumber, Context context, ScanInputParameters params) {
    	ScanInputPageFragment fragment = new ScanInputPageFragment(context, params);
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        fragment.setArguments(args);
        return fragment;
    }
    public ScanInputPageFragment()
    {

    }
    private ScanInputPageFragment(Context context, ScanInputParameters params) {
    	_params = params;
    	_context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _pageNumber = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	if(_pageNumber == 0) {
    		return buildButton(inflater, container);
    	}
    	
    	return buildEditText(inflater, container);
    }

    /**
     * Returns the page number represented by this fragment object.
     */
    public int getPageNumber() {
        return _pageNumber;
    }
    
    public void setText(String text) {
    	_inputText = text;
    	if(_inputView != null) {
    		_inputView.setText(text);
    	}
    }
    
    public String getText() {
    	if(_inputView != null) {
    		return _inputView.getText().toString();
    	}
    	
    	return null;
    }
    
    public void addScanListener(ScanInputScanListener listener) {
    	_scanListeners.add(listener);
    }
    
    public void addTextChangedListener(TextWatcher watcher) {
    	_textChangeListeners.add(watcher);
    }
    
    public void setOnEditorActionListener(OnEditorActionListener listener) {
    	_editorActionListener = listener;
    }
    
    public void setInputType(int inputType) {
    	_inputType = inputType;
    }
    
    private View buildEditText(LayoutInflater inflater, ViewGroup container) {
    	isButton = false;
    	ViewGroup rootView = (ViewGroup) inflater
                .inflate(R.layout.fragment_scaninput_text, container, false);
        
    	_inputView = (EditText)rootView.findViewWithTag("TextField");
    	if(_params.isAlignRight()) {
	    	_inputView.setPadding(25, 0, 0, 0);
    	} else {
	    	View spacerView = rootView.findViewWithTag("SpacerView");
	    	spacerView.setVisibility(View.GONE);
	    	_inputView.setPadding(90, 0, 0, 0);
    	}
    	_inputView.setText(_inputText);
    	_inputView.setBackgroundResource(_params.getBackground());
    	_inputView.setTextAppearance(_context, _params.getTextAppearence());
    	String hintValue = getResources().getString(_params.getHint());
    	String hintFormat = getResources().getString(R.string.scan_input_txt_text_format);
    	_inputView.setHint(String.format(hintFormat, hintValue));
    	_inputView.setInputType(_inputType);
    	if(_inputType == InputType.TYPE_TEXT_VARIATION_PASSWORD) {
    		_inputView.setTransformationMethod(new PasswordTransformationMethod());
    	}
    	((EditText)_inputView).setHintTextColor(getResources().getColor(_params.getHintTextColor()));
    	((EditText)_inputView).setOnEditorActionListener(_editorActionListener);
    	((EditText)_inputView).setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				// Fix for long click crashing the app.
				return true;
			}
    		
    	});
    	
    	((EditText)_inputView).addTextChangedListener(new TextWatcher() {

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				for(TextWatcher watcher : _textChangeListeners) {
		    		watcher.beforeTextChanged(s, start, count, after);
		    	}
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				for(TextWatcher watcher : _textChangeListeners) {
		    		watcher.onTextChanged(s, start, before, count);
		    	}
			}

			@Override
			public void afterTextChanged(Editable s) {
				for(TextWatcher watcher : _textChangeListeners) {
		    		watcher.afterTextChanged(s);
		    	}
			}
    		
    	});

        return rootView;
    }
    
    private View buildButton(LayoutInflater inflater, ViewGroup container) {
    	ViewGroup rootView = (ViewGroup) inflater
                .inflate(R.layout.fragment_scaninput_button, container, false);
        
    	//_inputView = (Button)rootView.findViewById(R.id.scan_view_input);
    	_inputView = (Button)rootView.findViewWithTag("ButtonTag");
    	if(_params.isAlignRight()) {
	    	_inputView.setPadding(25, 0, 0, 0);
    	} else {
	    	View spacerView = rootView.findViewWithTag("SpacerView");
	    	spacerView.setVisibility(View.GONE);
	    	_inputView.setPadding(90, 0, 0, 0);
    	}
    	_inputView.setText(_inputText);
    	_inputView.setBackgroundResource(_params.getBackground());
    	_inputView.setTextAppearance(_context, _params.getTextAppearence());
    	if(_inputType == InputType.TYPE_TEXT_VARIATION_PASSWORD) {
    		_inputView.setTransformationMethod(new PasswordTransformationMethod());
    	}
    	((Button)_inputView).setHintTextColor(getResources().getColor(_params.getHintTextColor()));
    	String hintValue = getResources().getString(_params.getHint());
    	String hintFormat = getResources().getString(R.string.scan_input_btn_text_format);
    	_inputView.setHint(String.format(hintFormat, hintValue));
        
    	_inputView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // 0x6D6D6D sets how much to darken - tweak as desired
                        setColorFilter(v, 0x6D6D6D);
                        initiateStartScan();
                        break;
                    // remove the filter when moving off the button
                    // the same way a selector implementation would 
                    case MotionEvent.ACTION_MOVE:
                        Rect r = new Rect();
                        v.getLocalVisibleRect(r);
                        if (!r.contains((int) event.getX(), (int) event.getY())) {
                            setColorFilter(v, null);
                        }
                        break;
                    case MotionEvent.ACTION_OUTSIDE:
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP:
                    	setColorFilter(v, null);
                    	initiateStopScan();
                        break;
                }
                return false;
            }

            private void setColorFilter(View v, Integer filter) {
                if (filter == null) v.getBackground().clearColorFilter();
                else {
                    // To lighten instead of darken, try this:
                    // LightingColorFilter lighten = new LightingColorFilter(0xFFFFFF, filter);
                    LightingColorFilter darken = new LightingColorFilter(filter, 0x000000);
                    v.getBackground().setColorFilter(darken);
                }
                // required on Android 2.3.7 for filter change to take effect (but not on 4.0.4)
                v.getBackground().invalidateSelf();
            }
            
            private void initiateStartScan() {
            	for(ScanInputScanListener listener : _scanListeners) {
            		listener.onScanStarted();
            	}
                Intent i = new Intent();
                i.setAction("com.motorolasolutions.emdk.datawedge.api.ACTION_SOFTSCANTRIGGER");
                i.putExtra("com.motorolasolutions.emdk.datawedge.api.EXTRA_PARAMETER", "START_SCANNING");
                _context.sendBroadcast(i);
            }
            
            private void initiateStopScan() {
            	for(ScanInputScanListener listener : _scanListeners) {
            		listener.onScanStopped();
            	}
            	
                Intent i2 = new Intent();
                i2.setAction("com.motorolasolutions.emdk.datawedge.api.ACTION_SOFTSCANTRIGGER");
                i2.putExtra("com.motorolasolutions.emdk.datawedge.api.EXTRA_PARAMETER", "STOP_SCANNING");
                _context.sendBroadcast(i2);
            }
        });

        return rootView;    	
    }
    
    public interface ScanInputScanListener {
    	void onScanStarted();
    	void onScanStopped();
    }
}