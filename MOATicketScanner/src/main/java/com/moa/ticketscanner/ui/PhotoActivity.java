package com.moa.ticketscanner.ui;

/**
 * Created by vvemula on 3/1/2016.
 */

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import com.moa.ticketscanner.R;
import com.moa.ticketscanner.dto.configuration.PassPhotoDto;
import com.moa.ticketscanner.settings.AppSettings;
import com.moa.ticketscanner.settings.RuntimeSettings;
import com.moa.ticketscanner.ui.dialogs.DialogUploadStatus;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import net.gotev.uploadservice.BinaryUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;




public class PhotoActivity extends MoaBaseActivity {

    // Activity request codes
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;

    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final String TAG = "upload";
    String uploadurl = AppSettings.MOA_API_DOMAIN+AppSettings.UPLOAD_PHOTO_URI;
    // directory name to store captured images and videos
    private static final String IMAGE_DIRECTORY_NAME = "TicketScanner";
    private Bitmap upPhoto;
    private Uri fileUri; // file url to store image/video
    private TextView txtTitle;
    private ImageView imgPreview;

    private Button btnCapturePicture;
    private Button btnGoBack;
    private Button btnUpload;
    private Button btnPhotoDecline;
    private RuntimeSettings runtimeSettings = RuntimeSettings.getInstance();
    private PassPhotoDto passPhotoDto = runtimeSettings.getPassPhoto();
    private String upVisualId = passPhotoDto.getVisualId().trim();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        txtTitle = (TextView) findViewById(R.id.dialogText);
        imgPreview = (ImageView) findViewById(R.id.imgPreview);

        btnCapturePicture = (Button) findViewById(R.id.btnCapturePicture);
        btnGoBack = (Button) findViewById(R.id.btnGoBack);
        btnUpload = (Button) findViewById(R.id.btnUpload);
        btnPhotoDecline = (Button) findViewById(R.id.btnDecline);

        upVisualId = passPhotoDto.getVisualId().trim();
        btnCapturePicture.setEnabled(true);
        btnUpload.setEnabled(false);
        btnGoBack.setEnabled(false);
		/*
		 * Capture image button click event
		 */
        btnCapturePicture.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // capture picture
                captureImage();
            }
        });

        btnUpload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                try{
                    //uploadImage();
                    uploadImageUsingService();
                }
                catch(Exception ex){

                    Toast.makeText(getApplicationContext(),
                            "Network Error: "+ex.getMessage(),
                            Toast.LENGTH_LONG).show();
                    showUploadStatus(false);
                }

            }
        });

        btnGoBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                runtimeSettings.setPhotoActivityStatus("Done");
                //transferToTicketScan();
                Intent doneintent = getIntent();
                setResult(RESULT_OK, doneintent);
                finish();
            }
        });
        btnPhotoDecline.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                transferToPhotoDeclineActivity();

            }
        });

        // Checking camera availability
        if (!isDeviceSupportCamera()) {
            Toast.makeText(getApplicationContext(),
                    "Sorry! Your device doesn't support camera",
                    Toast.LENGTH_LONG).show();
            // will close the app if the device does't have camera
            finish();
        }
    }

    private UploadNotificationConfig getNotificationConfig(String filename) {
        if (false) return null;

        return new UploadNotificationConfig()
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(filename)
                .setInProgressMessage("In progress")
                .setCompletedMessage("Uploaded")
                .setErrorMessage("Error Uploading")
                .setAutoClearOnSuccess(true)
                .setClickIntent(new Intent(this, PhotoActivity.class))
                .setClearOnAction(true)
                .setRingToneEnabled(true);

    }
    private void uploadImageUsingService()
    {

        String serverUrlString = uploadurl+upVisualId;
        //Handler mHandler = new Handler();
        String fileToUpload = fileUri.getPath();

        try{

            String uploadID = new BinaryUploadRequest(this, serverUrlString)
                    .addHeader("file-name", upVisualId)
                    .setFileToUpload(fileToUpload)
                    .setNotificationConfig(getNotificationConfig("Upload"))
                    .setCustomUserAgent("")
                    .setAutoDeleteFilesAfterSuccessfulUpload(true)
                    .setUsesFixedLengthStreamingMode(true)
                    .setMaxRetries(2)
                    .startUpload();


         /*  mHandler.postDelayed(new Runnable() {
               public void run() {
                   showUploadStatus(true);
                   btnGoBack.setEnabled(true);
               }
           }, 2000);
            Log.d("Test2", uploadID);*/
            if(uploadID != "")
            {
                showUploadStatus(true);
                btnGoBack.setEnabled(true);
                btnCapturePicture.setEnabled(false);
                btnUpload.setEnabled(false);
            }
            else
            {
                showUploadStatus(false);
            }

        }catch (FileNotFoundException exc) {

            showUploadStatus(false);

        } catch (IllegalArgumentException exc) {
            showUploadStatus(false);

        } catch (MalformedURLException exc) {
            showUploadStatus(false);

        }catch (Exception exc) {
            showUploadStatus(false);

        }

    }

    private void showUploadStatus(boolean uploadstatus)
    {
        final DialogUploadStatus dialog = new DialogUploadStatus(this, uploadstatus);

        dialog.show();
    }

    /**
     * Checking device has camera hardware or not
     * */
    private boolean isDeviceSupportCamera() {
        if (getApplicationContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }
    private void transferToPhotoDeclineActivity() {

        Intent intent = new Intent(PhotoActivity.this, PhotoDeclineActivity.class);
        intent.putExtra("visualId", upVisualId);
        startActivityForResult(intent, 200);
        //finish();
    }

    /*
     * Capturing Camera Image will lauch camera app requrest image capture
     */
    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /*
     * Here we store the file url as it will be null after returning from camera
     * app
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }


    /**
     * Receiving activity result method will be called after closing the camera
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image

        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // successfully captured the image
                // display it in image view
                previewCapturedImage();
                btnUpload.setEnabled(true);
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture

                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image

                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        } else
        {
            if(resultCode == 200)
            {

                Intent doneintent = getIntent();
                setResult(RESULT_OK, doneintent);
                finish();

            }
        }
    }

    /*
     * Display image from a path to ImageView
     */
    private void previewCapturedImage() {
        try {

            txtTitle.setText("Preview Guest Photo");
            imgPreview.setVisibility(View.VISIBLE);

            // bimatp factory
            BitmapFactory.Options options = new BitmapFactory.Options();
//
//            // downsizing image as it throws OutOfMemory Exception for larger
//            // images
            options.inSampleSize = 12;
//
            Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
                    options);
            //Bitmap bitmap = ShrinkBitmap(fileUri.getPath(),100,100);
            //bitmap = Bitmap.createScaledBitmap(bitmap,100,100,false) ;
            /*ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,bytes);
            File f = new File(fileUri.getPath());
            try
            {
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                fo.write(bytes.toByteArray());
                fo.close();
            }
            catch (Exception ex)
            {

            }*/

//            final Bitmap bitmap = ShrinkBitmap(fileUri.getPath(),100,100);
            /*File folder = new File(
                    Environment
                            .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    IMAGE_DIRECTORY_NAME);
            File[] listOfFiles = folder.listFiles();
            String fileName = listOfFiles[0].getName();
            File resultFile = new File(folder.getName()+'/'+fileName);*/



           /* for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    System.out.println("File " + listOfFiles[i].getName());
                } else if (listOfFiles[i].isDirectory()) {
                    System.out.println("Directory " + listOfFiles[i].getName());
                }
            }*/
           /* Bitmap rotatedbitmap = bitmap;
            if(bitmap.getWidth() > bitmap. getHeight())
            {
                Matrix matrix = new Matrix();
                matrix.postRotate(90);
                rotatedbitmap = Bitmap.createBitmap(bitmap , 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            }*/
            imgPreview.setImageBitmap(bitmap);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }



    /**
     * ------------ Helper Methods ----------------------
     * */

	/*
	 * Creating file uri to store image/video
	 */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    Bitmap ShrinkBitmap(String file, int width, int height){

        BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
        bmpFactoryOptions.inJustDecodeBounds = true;


        int heightRatio = (int)Math.ceil(bmpFactoryOptions.outHeight/(float)height);
        int widthRatio = (int)Math.ceil(bmpFactoryOptions.outWidth/(float)width);

        if (heightRatio > 1 || widthRatio > 1)
        {
            if (heightRatio > widthRatio)
            {
                bmpFactoryOptions.inSampleSize = heightRatio;
            } else {
                bmpFactoryOptions.inSampleSize = widthRatio;
            }
        }

        bmpFactoryOptions.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
        return bitmap;
    }


    /*
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

}

