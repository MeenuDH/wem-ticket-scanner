package com.moa.ticketscanner.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;

import android.widget.ImageView;
import android.widget.TextView;

import com.moa.ticketscanner.R;
import com.moa.ticketscanner.settings.AppSettings;
import com.moa.ticketscanner.settings.RuntimeSettings;

public class DialogUploadStatus extends Dialog implements OnClickListener {

	private Activity activity;
	private Handler mHandler = new Handler();

	private boolean forceDismiss = false;
	private boolean uploadSuccess = false;
	private RuntimeSettings runtimeSettings = RuntimeSettings.getInstance();
	private TextView dialogTextView;

	private ImageView img;
	public DialogUploadStatus(Activity activity, boolean uploadSuccess) {
		super(activity);
		this.activity = activity;
		this.uploadSuccess = uploadSuccess;
	}



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_upload_status);
		dialogTextView = (TextView) findViewById(R.id.dialogText);
		if(uploadSuccess) {
			dialogTextView.setText("Photo Saved!");

		}
		else
			dialogTextView.setText("Try Again!");
		img = (ImageView)findViewById(R.id.img);
		if(uploadSuccess)
			img.setImageResource(R.drawable.preview_full_success);
		else
			img.setImageResource(R.drawable.preview_full_fail);

		mHandler.postDelayed(new Runnable() {
			public void run() {
				postWait();
			}
		}, AppSettings.ACCEPTED_SCREEN_TIMEOUT);

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_VOLUME_DOWN || keyCode == KeyEvent.KEYCODE_VOLUME_MUTE) {
			return true;
		}

		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(View v) {
		this.dismiss();
	}

	private void postWait() {
		this.dismiss();
	}
}