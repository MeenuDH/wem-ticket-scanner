package com.moa.ticketscanner.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;

import com.moa.ticketscanner.imageservices.CustomVolleyRequest;
import com.moa.ticketscanner.R;
import com.android.volley.toolbox.NetworkImageView;
import com.moa.ticketscanner.settings.AppSettings;
import com.moa.ticketscanner.ui.OfflineTicketScannerActivity;

import com.moa.ticketscanner.settings.RuntimeSettings;

import com.octo.android.robospice.SpiceManager;



public class DialogPhotoDisplay extends Dialog implements OnClickListener {

	public Activity activity;
	//Replace this with the Appsettings Uri
	private RuntimeSettings runtimeSettings;
	SpiceManager spiceManager;
	private boolean canOverride;
	private String photoUrlStr;
	private Button cancelButton;
	private Button okButton;
	private View.OnClickListener cancelListener;
	private View.OnClickListener okListener;
	//private ImageView img;
	private Bitmap bitmap;
	private NetworkImageView imageView;
	private ImageLoader imageLoader;
	private TextView title;


	public DialogPhotoDisplay(Activity activity, String photoUrl) {
		super(activity);
		this.activity = activity;
		this.canOverride = true;
		this.photoUrlStr = photoUrl;
		runtimeSettings = RuntimeSettings.getInstance();

	}

	private void loadImage(){
		//photoUrlStr = "null";
		String url = AppSettings.MOA_API_DOMAIN+photoUrlStr;
		//String url = editTextUrl.getText().toString().trim();
		if(url.equals("")){
			//Toast.makeText(this, "Please enter a URL", Toast.LENGTH_LONG).show();
			return;
		}
		if(photoUrlStr != "null")
		{
			imageLoader = CustomVolleyRequest.getInstance(this.getContext())
					.getImageLoader();
			imageLoader.get(url, ImageLoader.getImageListener(imageView,
					R.drawable.loading, R.drawable.preview_full_fail));
			imageView.setImageUrl(url, imageLoader);

		}
		else
		{
			title.setText("No Photo");
			imageView.setVisibility(View.INVISIBLE);
		}

	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_photo_display);
		title = (TextView) findViewById(R.id.dialogText);
		okButton = (Button) findViewById(R.id.dialogOk);
		cancelButton = (Button) findViewById(R.id.dialogCancel);
		cancelButton.setVisibility(canOverride ? View.VISIBLE : View.GONE);
		//photoUrlStr = "/Photos/profile-029000001.jpg";
		//String photoUrl1 = AppSettings.MOA_API_DOMAIN+photoUrlStr;
		imageView = (NetworkImageView) findViewById(R.id.img);
		//img = (ImageView)findViewById(R.id.img);
		//img.setImageBitmap(runtimeSettings.getPhotoBitmap());
		//img.setImageURI(Uri.parse(photoUrl1));

		loadImage();
		okButton.setOnClickListener(okListener == null ? this : okListener);
		cancelButton.setOnClickListener(cancelListener == null ? this : cancelListener);

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_VOLUME_DOWN || keyCode == KeyEvent.KEYCODE_VOLUME_MUTE) {
			return true;
		}

		return super.onKeyDown(keyCode, event);
	}

	public void setCancelListener(View.OnClickListener listener) {
		cancelListener = listener;
		handleOffline();
	}

	public void setOkListener(View.OnClickListener listener) {
		okListener = listener;
		handleOffline();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.dialogOk:
			this.dismiss();
			break;
		case R.id.dialogCancel:
			this.dismiss();
			break;
		}

	}
	protected void transferToActivity(Class<?> activityCls) {
		transferToActivity(activityCls, null);
	}
	public void handleOffline()
	{
		//wirelessReceiver = new ReceiveWifiStatus();
		runtimeSettings.setCurrentActivity(this.getClass());
		ConnectivityManager conMngr = (ConnectivityManager) activity.getSystemService(this.getContext().CONNECTIVITY_SERVICE);
		android.net.NetworkInfo wifi = conMngr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		if (wifi.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {
			runtimeSettings.setOnline(true);

			if (AppSettings.debug)
				Log.d("WEM", "Resume Set Online");
		}
		// we are offline
		else {
			runtimeSettings.setOnline(false);
			if (AppSettings.debug)
				Log.d("WEM", "Resume Set Offline");
		}

		// if we are not online and we should finish in offline mode, do so
		if (!runtimeSettings.isOnline() && runtimeSettings.getCurrentActivity() != OfflineTicketScannerActivity.class) {
			transferToActivity(OfflineTicketScannerActivity.class);
		}


	}
	protected void transferToActivity(Class<?> activityCls, Bundle extras) {
		Intent intent = new Intent(activity, activityCls);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		if (extras != null) {
			intent.putExtras(extras);
		}
		activity.startActivity(intent);
	}

}
