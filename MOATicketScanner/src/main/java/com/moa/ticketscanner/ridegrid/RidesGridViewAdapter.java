package com.moa.ticketscanner.ridegrid;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.moa.ticketscanner.R;
import com.moa.ticketscanner.domain.Ride;
import com.moa.ticketscanner.domain.UserRole;

import java.util.ArrayList;
import java.util.Locale;

public class RidesGridViewAdapter extends ArrayAdapter<Ride> implements Filterable {
	private Context context;
	private int layoutResourceId;
	private ArrayList<Ride> data = new ArrayList<Ride>();
	private ArrayList<Ride> privData;
	private RideGridFilter rideFilter;
	private UserRole userRole;

	public RidesGridViewAdapter(Context context, int layoutResourceId, ArrayList<Ride> data, UserRole role) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
		privData = data;
		this.userRole = role;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		RecordHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new RecordHolder();
			holder.rideName = (TextView) row.findViewById(R.id.grid_text);
			holder.rideImage = (ImageView) row.findViewById(R.id.grid_image);
			holder.selected = (ImageView) row.findViewById(R.id.grid_selected);
			row.setTag(holder);
		} else {
			holder = (RecordHolder) row.getTag();
		}

		Ride gridElement = data.get(position);
		holder.rideName.setText(gridElement.getRideName());
		holder.rideImage.setImageBitmap(gridElement.getRideImage());

		// if item is selected this mark it so
		if (gridElement.getSelected() == true) {
			holder.selected.setAlpha((float) 1);
		}
		// else if it's not selected don't mark it
		else {
			holder.selected.setAlpha((float) 0);
		}

		return row;

	}

	@Override
	public int getCount() {

		return data.size();
	}

	@Override
	public Ride getItem(int position) {

		return data.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	static class RecordHolder {
		TextView rideName;
		ImageView rideImage;
		ImageView selected;
	}

	@Override
	public Filter getFilter() {
		if (rideFilter == null)
			rideFilter = new RideGridFilter();

		return rideFilter;
	}

	public class RideGridFilter extends Filter {

		
		
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			FilterResults results = new FilterResults();
			// We implement here the filter logic
			if (constraint == null || constraint.length() == 0) {
				// No filter implemented we return all the list
				results.values = privData;
				results.count = privData.size();
			} else {
				// We perform filtering operation
				ArrayList<Ride> nRideList = new ArrayList<Ride>();

				for (Ride ride : privData) {
					if(addRide(ride, constraint.toString())) {
						nRideList.add(ride);
					}
				}

				results.values = nRideList;
				results.count = nRideList.size();

			}
			return results;
		}
		
		private boolean addRide(Ride ride, String constraint) {
			String upperCaseConstraint = constraint.toUpperCase(Locale.getDefault());
			if(userRole == UserRole.USER) {
				return (ride.getScanId() == null) ? false : ride.getScanId().equalsIgnoreCase(upperCaseConstraint);
			} else {
				return ((ride.getRideName() == null) ? false : ride.getRideName().toUpperCase(Locale.getDefault()).contains(upperCaseConstraint)) ||
						((ride.getScanId() == null) ? false : ride.getScanId().toUpperCase(Locale.getDefault()).contains(upperCaseConstraint));
			}
		}

		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {

			// Now we have to inform the adapter about the new list filtered
			if (results.count == 0) {
				data = privData;
				notifyDataSetInvalidated();
			}
			else {
				data = (ArrayList<Ride>) results.values;
				notifyDataSetChanged();
			}

		}

	}

}
