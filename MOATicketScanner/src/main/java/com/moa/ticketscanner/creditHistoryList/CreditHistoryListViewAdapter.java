package com.moa.ticketscanner.creditHistoryList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.moa.ticketscanner.R;

import java.util.List;

public class CreditHistoryListViewAdapter extends BaseAdapter {
	Context context;
	List<CreditHistoryListItem> rowItems;

	public CreditHistoryListViewAdapter(Context context, List<CreditHistoryListItem> items) {
		this.context = context;
		this.rowItems = items;
	}

	/* private view holder class */
	private class ViewHolder {
		TextView rideName;
		TextView pointsRefunded;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.partial_credit_history_list_view_item, null);
			holder = new ViewHolder();
			holder.rideName = (TextView) convertView.findViewById(R.id.creditHistoryListItemRideName);
			holder.pointsRefunded = (TextView) convertView.findViewById(R.id.creditHistoryListItemRefundInfo);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		CreditHistoryListItem rowItem = (CreditHistoryListItem) getItem(position);

		holder.rideName.setText(rowItem.getRideName());
		holder.pointsRefunded.setText(rowItem.getRefundInfo());

		// do some cool background color change 
		if (position % 2 != 0) {
			convertView.findViewById(R.id.creditHistoryListLayout).setBackgroundColor(context.getResources().getColor(R.color.MoaListGrayLight));
		} else {
			convertView.findViewById(R.id.creditHistoryListLayout).setBackgroundColor(context.getResources().getColor(R.color.MoaListGrayDark));
		}

		return convertView;
	}

	@Override
	public int getCount() {
		return rowItems.size();
	}

	@Override
	public Object getItem(int position) {
		return rowItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return rowItems.indexOf(getItem(position));
	}
}