package com.moa.ticketscanner.creditHistoryList;

public class CreditHistoryListItem {

	private String rideName;
	private String refundInfo;
	
	public CreditHistoryListItem(String rideName, String refundInfo) {
		this.rideName = rideName;
		this.refundInfo = refundInfo;
	}

	public String getRideName() {
		return rideName;
	}

	public void setRideName(String rideName) {
		this.rideName = rideName;
	}

	public String getRefundInfo() {
		return refundInfo;
	}

	public void setRefundInfo(String refundInfo) {
		this.refundInfo = refundInfo;
	}
	
	
	
}
