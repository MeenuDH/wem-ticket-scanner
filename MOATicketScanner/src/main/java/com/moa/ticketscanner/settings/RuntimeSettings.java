package com.moa.ticketscanner.settings;


import android.util.Log;

import com.moa.ticketscanner.communication.MoaSpiceService;
import com.moa.ticketscanner.domain.Ride;
import com.moa.ticketscanner.domain.UserRole;
import com.moa.ticketscanner.dto.configuration.PassPhotoDto;
import com.moa.ticketscanner.dto.configuration.TicketStatusCodeDto;
import com.moa.ticketscanner.dto.rides.RideRotationDto;
import com.moa.ticketscanner.dto.tickets.SubTicketDto;
import com.moa.ticketscanner.dto.tickets.SuperTicketDto;
import com.octo.android.robospice.SpiceManager;

import java.util.List;

public class RuntimeSettings {

	private static RuntimeSettings runtimeSettings;

	private boolean isRideLocked = false;
	private Ride currentRide;
	private String gatewaySessionId;
	private boolean isOnline = false;
	private String userId;
	private Class<?> currentActivity;
	
	private UserRole userRole;

	private SpiceManager spiceManager = new SpiceManager(MoaSpiceService.class);
	private List<RideRotationDto> rideRotations;
	private List<TicketStatusCodeDto> ticketStatusCodes;
	private PassPhotoDto passPhoto;
	private String photoActivityStatus = "";
	private boolean isServerReachable = true;
	private String selectedSubTicket = "";
	private String deviceId = "";
	private RuntimeSettings() {

	}

	public static RuntimeSettings getInstance() {
		if (runtimeSettings == null) {
			runtimeSettings = new RuntimeSettings();
		}
		return runtimeSettings;
	}

	public Ride getCurrentRide() {
		return currentRide;
	}
	public PassPhotoDto getPassPhoto() {
		return passPhoto;
	}
	public String getSelectedSubTicket() {
		return selectedSubTicket;
	}
	public void setPassPhoto(PassPhotoDto passPhoto) {
		this.passPhoto = passPhoto;
	}
	public void setSelectedSubTicket(String subTicket) {
		this.selectedSubTicket = subTicket;
	}
	public String getPhotoActivityStatus()
	{
		return photoActivityStatus;
	}
	public void setPhotoActivityStatus(String photoActivityStatus)
	{
		this.photoActivityStatus = photoActivityStatus;
	}

	public void setCurrentRide(Ride currentRide) {
		this.currentRide = currentRide;
	}

	public boolean isRideLocked() {
		return isRideLocked;
	}

	public void setRideLocked(boolean isRideLocked) {
		this.isRideLocked = isRideLocked;
	}

	public String getGatewaySessionId() {
		return gatewaySessionId;
	}

	public void setGatewaySessionId(String gatewaySessionId) {
		this.gatewaySessionId = gatewaySessionId;
	}

	public boolean isOnline() {
		return isOnline;
	}
	public boolean isServerReachable() {
		return isServerReachable;
	}
	public void setOnline(boolean isOnline) {
		this.isOnline = isOnline;
		if (AppSettings.debug) {
			Log.d("WEM", "Set Online: " + isOnline);
		}
	}
	public void setServerReachable(boolean isServerReachable) {
		this.isServerReachable = isServerReachable;
		if (AppSettings.debug) {
			Log.d("WEM", "Set Online: " + isServerReachable);
		}
	}
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Class<?> getCurrentActivity() {
		return currentActivity;
	}

	public void setCurrentActivity(Class<?> currentActivity) {
		this.currentActivity = currentActivity;
	}

	public UserRole getUserRole() {
		return userRole;
	}

	public List<RideRotationDto> getRideRotations() {
		return rideRotations;
	}

	public void setRideRotations(List<RideRotationDto> rideRotations) {
		this.rideRotations = rideRotations;
	}

	public List<TicketStatusCodeDto> getTicketStatusCodes() {
		return ticketStatusCodes;
	}

	public void setUserRole(UserRole role) {
		userRole = role;
	}

	public void setTicketStatusCodes(List<TicketStatusCodeDto> ticketStatusCodes) {
		this.ticketStatusCodes = ticketStatusCodes;
	}
	public void setDeviceId(String macId) {
		this.deviceId = macId;
	}
	public String getDeviceId(){
		return deviceId;
	}
}
