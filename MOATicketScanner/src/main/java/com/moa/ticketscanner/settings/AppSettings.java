package com.moa.ticketscanner.settings;

public class AppSettings {

	public static final boolean debug = false;
	public static final int debugCount = 5;
	public static final String debugCode = "0000";
	public static final boolean debug_screens_available = true;
	
	/* environmental constants */
//	public static final String GALAXY_API_DOMAIN = "http://172.20.1.5:3051";
	//WEM Test Servers
	//public static final String GALAXY_API_DOMAIN = "http://10.1.3.41:3051";//
	//public static final String MOA_API_DOMAIN = "http://10.1.3.85";

	//WEM Production Servers
	public static final String GALAXY_API_DOMAIN = "http://10.1.3.40:3051";
	//public static final String MOA_API_DOMAIN = "http://acpservice.wem.local";
	public static final String MOA_API_DOMAIN = "http://10.1.3.85:90";

	public static final long ACCEPTED_SCREEN_TIMEOUT = 2000;
	public static final String OFFLINE_MODE_FILE_NAME = "OFFLINE_MODE_DATE.json";
	
	/* MOA Api Uri's */
	public static final String USER_ROLE_URI_FORMAT = "/v1/users/%s/role";
	public static final String RIDES_BY_ROTATION_URI = "/v1/rotations/rides";
	public static final String STATUS_CODE_URI = "/v1/statusCodes";
	public static final String OFFLINE_UPLOAD_URI = "/v1/offlineScan";
//	public static final String CHECK_PHOTO_URI = "/Photos/";
	public static final String CHECK_PHOTO_URI = "/v1/passes/getpassphoto/";
	public static final String UPLOAD_PHOTO_URI = "/v1/passes/updatepassphoto/";
	public static final String CHECK_CONNECTION = "/v1/healthcheck/";
	//public static final String GET_SUB_TICKETS = "/api/values/";
	public static final String GET_SUB_TICKETS = "/v1/superticket/getsuperticket/";
	public static final String UPLOAD_DECLINED_PHOTO_URI = "/v1/passes/declinephoto/";
	public static final String UPLOAD_DEVICE_INFO_URI = "/v1/deviceinfo/update/";
	/* Keys */
	public static final String USER_ID_KEY = "userId";
	public static final String USER_PASSWORD_KEY = "userPassword";
	public static final String USER_ROLE_KEY = "userRole";
	public static final String RIDE_NAME_CACHE = "rideCache";
	public static final String TICKET_STATUS_CODE_CACHE = "ticketStatusCache";
	public static final String PHOTO_PASS_CACHE = "photoCache";
	public static final String APPLICATION_NAME = "MOA - Ticket Scanner";
	
	private int debugClick;	
	
	public AppSettings() {
		debugClick = 0;
	}
	
	public void addClick() {
		debugClick++;
	}
	
	public int getClick() {
		return debugClick;
	}

}
