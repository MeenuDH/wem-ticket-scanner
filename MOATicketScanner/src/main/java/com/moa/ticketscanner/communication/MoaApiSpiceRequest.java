package com.moa.ticketscanner.communication;

import com.moa.ticketscanner.settings.AppSettings;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;


public class MoaApiSpiceRequest<T> extends SpringAndroidSpiceRequest<T> {
	private Class<T> clazz;
	private MoaApiRequestDetails requestDetails;

	public MoaApiSpiceRequest(Class<T> responseType, MoaApiRequestDetails requestDetails) {
		super(responseType);
		clazz = responseType;
		this.requestDetails = requestDetails;
	}

	@Override
	public T loadDataFromNetwork() throws Exception {
		if(requestDetails.getHttpMethod() == HttpMethod.GET) {
			return performGet();
		}
		
		return performNonGet();
	}
	
	private T performGet() {
		String url = AppSettings.MOA_API_DOMAIN.concat(requestDetails.getUri());
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<T> entity = null;
		entity = getRestTemplate().getForEntity(url, clazz);

		T envelope = entity.getBody();

		return envelope;
	}
	
	private T performNonGet() {
		String url = AppSettings.MOA_API_DOMAIN.concat(requestDetails.getUri());
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity requestEntity = new HttpEntity(requestDetails.getRequestBody());
		HttpEntity<T> entity = null;
		entity = getRestTemplate().exchange(url, requestDetails.getHttpMethod(), requestEntity, clazz);

		T envelope = entity.getBody();

		return envelope;
	}

}
