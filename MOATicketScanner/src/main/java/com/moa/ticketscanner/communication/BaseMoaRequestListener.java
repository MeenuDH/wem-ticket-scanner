package com.moa.ticketscanner.communication;

import com.moa.ticketscanner.ui.MoaBaseActivity;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

public abstract class BaseMoaRequestListener<T> implements RequestListener<T> {

	private MoaBaseActivity initiatingActivity;
	
	public BaseMoaRequestListener(MoaBaseActivity initiatingActivity) {
		this.initiatingActivity = initiatingActivity;
	}
	
	@Override
	public void onRequestFailure(SpiceException spiceException) {
		Throwable cause = spiceException.getCause();
		
		if(cause instanceof HttpClientErrorException) {
			HttpClientErrorException exception = (HttpClientErrorException) cause;
			if(exception.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
				handleResourceNotFoundError(exception);
			}
		} else {
			showCommunicationError();
		}
	}

	@Override
	public void onRequestSuccess(T result) {
		handleSuccess(result);
	}
	
	protected abstract void handleSuccess(T result);
	
	protected void handleResourceNotFoundError(HttpClientErrorException exception) {
		showCommunicationError();
	}
	
	protected void showCommunicationError() {
		initiatingActivity.showErrorDialog("Communication Error", "An error occurred calling the service");
	}
}
