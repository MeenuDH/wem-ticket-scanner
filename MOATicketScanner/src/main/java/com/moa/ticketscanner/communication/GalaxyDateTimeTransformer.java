package com.moa.ticketscanner.communication;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.simpleframework.xml.transform.Transform;

public class GalaxyDateTimeTransformer implements Transform<DateTime>{
	private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("yyy-MM-dd HH:mm:ss");
	
	@Override
	public DateTime read(String date) throws Exception {
		return FORMATTER.parseDateTime(date);
	}

	@Override
	public String write(DateTime date) throws Exception {
		return FORMATTER.print(date);
	}

}
