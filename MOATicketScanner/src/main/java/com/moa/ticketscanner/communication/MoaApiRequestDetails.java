package com.moa.ticketscanner.communication;

import org.springframework.http.HttpMethod;

public class MoaApiRequestDetails {
	private HttpMethod httpMethod;
	private String uri;
	private Object requestBody;
	
	public HttpMethod getHttpMethod() {
		return httpMethod;
	}
	public void setHttpMethod(HttpMethod httpMethod) {
		this.httpMethod = httpMethod;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public Object getRequestBody() {
		return requestBody;
	}
	public void setRequestBody(Object requestBody) {
		this.requestBody = requestBody;
	}
}