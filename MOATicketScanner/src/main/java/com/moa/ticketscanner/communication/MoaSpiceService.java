package com.moa.ticketscanner.communication;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.DateTime;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.transform.RegistryMatcher;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.http.converter.xml.SimpleXmlHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import android.app.Application;
import android.util.Log;

import com.octo.android.robospice.SpringAndroidSpiceService;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.persistence.springandroid.json.jackson.JacksonObjectPersisterFactory;

public class MoaSpiceService extends SpringAndroidSpiceService {
	private static final int WEBSERVICES_TIMEOUT = 1000;

	@Override
	public RestTemplate createRestTemplate() {
		RestTemplate restTemplate = new RestTemplate();

		//Vivek VPN Check

		HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		httpRequestFactory.setReadTimeout( WEBSERVICES_TIMEOUT );
		httpRequestFactory.setConnectTimeout( WEBSERVICES_TIMEOUT );
		restTemplate.setRequestFactory( httpRequestFactory );
		//End
		SimpleXmlHttpMessageConverter xmlConverter = new SimpleXmlHttpMessageConverter();
		RegistryMatcher matcher = new RegistryMatcher();
		matcher.bind(Date.class, new GalaxyDateTransformer());
		matcher.bind(DateTime.class, new GalaxyDateTimeTransformer());
		Serializer serializer = new Persister(matcher);
		xmlConverter.setSerializer(serializer);
		MappingJacksonHttpMessageConverter jsonConverter = new MappingJacksonHttpMessageConverter();
		final List<HttpMessageConverter<?>> listHttpMessageConverters = restTemplate
				.getMessageConverters();
		ObjectMapper jsonMapper = new ObjectMapper();
		jsonMapper.setDateFormat(new SimpleDateFormat("yyy-MM-dd HH:mm:ss"));
		jsonConverter.setObjectMapper(jsonMapper);

		listHttpMessageConverters.add(xmlConverter);
		listHttpMessageConverters.add(jsonConverter);
		restTemplate.setMessageConverters(listHttpMessageConverters);
		return restTemplate;
	}

	@Override
	public CacheManager createCacheManager(Application application)
			throws CacheCreationException {
		CacheManager cacheManager = new CacheManager();
		JacksonObjectPersisterFactory jacksonObjectPersisterFactory = new JacksonObjectPersisterFactory(
				application);
		cacheManager.addPersister(jacksonObjectPersisterFactory);
		return cacheManager;
	}
}