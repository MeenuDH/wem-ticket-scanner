package com.moa.ticketscanner.communication;

import com.moa.ticketscanner.dto.BaseGalaxyRequestDto;
import com.moa.ticketscanner.dto.BaseGalaxyResponseDto;
import com.moa.ticketscanner.settings.AppSettings;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import com.octo.android.robospice.retry.DefaultRetryPolicy;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

@SuppressWarnings("rawtypes")
public class GalaxyApiSpiceRequest<T extends BaseGalaxyResponseDto> extends SpringAndroidSpiceRequest<T> {
	private BaseGalaxyRequestDto requestBody;
	private Class<T> clazz;

	public GalaxyApiSpiceRequest(Class<T> genericClassType, BaseGalaxyRequestDto requestBody) {
		super(genericClassType);
		clazz = genericClassType;
		this.requestBody = requestBody;
		this.setRetryPolicy(new DefaultRetryPolicy(1, DefaultRetryPolicy.DEFAULT_DELAY_BEFORE_RETRY, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
	}

	@Override
	public T loadDataFromNetwork() throws Exception {

		String url = AppSettings.GALAXY_API_DOMAIN;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_XML);
		HttpEntity<BaseGalaxyRequestDto> requestEntity = new HttpEntity<BaseGalaxyRequestDto>(requestBody);

		HttpEntity<T> entity = getRestTemplate().exchange(url, HttpMethod.POST, requestEntity, clazz);
		T responseDto = entity.getBody();
		
		return responseDto;
	}

}
