package com.moa.ticketscanner.communication;

import com.moa.ticketscanner.dto.BaseGalaxyResponseDto;
import com.moa.ticketscanner.settings.RuntimeSettings;
import com.moa.ticketscanner.ui.LoginActivity;
import com.moa.ticketscanner.ui.MoaBaseActivity;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

public abstract class BaseGalaxyRequestListener<T extends BaseGalaxyResponseDto> implements RequestListener<T> {

	private MoaBaseActivity initiatingActivity;
	
	public BaseGalaxyRequestListener(MoaBaseActivity initiatingActivity) {
		this.initiatingActivity = initiatingActivity;
	}
	
	@Override
	public void onRequestFailure(SpiceException spiceException) {
		Throwable cause = spiceException.getCause();
		if(cause instanceof HttpClientErrorException) {
			HttpClientErrorException exception = (HttpClientErrorException) cause;
			if(exception.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
				handleResourceNotFoundError(exception);
			}
		}
		showCommunicationError();
	}
	
	@Override
	public void onRequestSuccess(T result) {
		if(result.hasErrors()) {
			handleResponseErrors(result);
		} else if(result.hasInvalidStatus()) {
			handleInvalidResponseStatus(result);
		} else {
			handleSuccess(result);
		}
	}
	
	protected abstract void handleSuccess(T result);
	
	protected void handleResponseErrors(T result) {
		if(result.getErrorCode().equalsIgnoreCase("110") || result.getErrorCode().equalsIgnoreCase("111")) {
			handleInvalidLogonError(result);
		} else {
			showCommunicationError();
		}
	}
	
	protected void handleInvalidResponseStatus(T result) {
		if(result.getStatusCode().equals("1001")) {
			handleAuthenticationError(result);
		} else {
			showCommunicationError();
		}
	}
	
	protected void handleAuthenticationError(T result) {
		RuntimeSettings.getInstance().setGatewaySessionId(null);
		initiatingActivity.showErrorDialogAndTransfer("Session Expired", "Your session has expired, please log in again", LoginActivity.class);
	}
	
	protected void handleInvalidLogonError(T result)  {
		showCommunicationError();
	}
	
	protected void handleResourceNotFoundError(HttpClientErrorException exception) {
		showCommunicationError();
	}
	
	protected void showCommunicationError() {
		initiatingActivity.showErrorDialog("Communication Error", "An error occurred calling the service");
	}
}
