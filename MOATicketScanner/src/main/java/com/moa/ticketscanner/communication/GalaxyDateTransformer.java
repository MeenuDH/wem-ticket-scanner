package com.moa.ticketscanner.communication;

import org.simpleframework.xml.transform.Transform;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GalaxyDateTransformer implements Transform<Date>{
	private static final DateFormat FORMATTER = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
	
	@Override
	public Date read(String date) throws Exception {
		return FORMATTER.parse(date);
	}

	@Override
	public String write(Date date) throws Exception {
		return FORMATTER.format(date);
	}

}
