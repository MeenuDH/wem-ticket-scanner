package com.moa.ticketscanner.debug;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import com.moa.ticketscanner.R;
import com.moa.ticketscanner.ui.MoaBaseActivity;
import com.moa.ticketscanner.ui.RideSelectionActivity;
import com.moa.ticketscanner.ui.TicketScanningActivity;

public class DebugShortcutsActivity extends MoaBaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_debug_shortcuts);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.debug_shortcuts, menu);
		return true;
	}
	
	
	public void goToRideSelectionActivity(View view) {
		Intent intent = new Intent(this, RideSelectionActivity.class);
		startActivity(intent);
	}
	
	public void goToTicketScanning(View view) {
		Intent intent = new Intent(this, TicketScanningActivity.class);
		startActivity(intent);
	}
}
