package com.moa.ticketscanner.debug;

import android.app.Notification;
import android.app.NotificationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import com.moa.ticketscanner.R;
import com.moa.ticketscanner.domain.AcceptanceAudio;
import com.moa.ticketscanner.ui.MoaBaseActivity;
import com.moa.ticketscanner.ui.dialogs.DialogTicketAcceptance;
import com.moa.ticketscanner.ui.dialogs.DialogTicketRejection;

public class DebugTicketDialogTesterActivity extends MoaBaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_debug_ticket_dialog_tester);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.debug_ticket_acceptance_tester, menu);
		return true;
	}

	public void accepted(View view) {
		DialogTicketAcceptance dialog = new DialogTicketAcceptance(this, AcceptanceAudio.Valid);
		dialog.show();
	}

	public void rejected(View view) {
		DialogTicketRejection dialog = new DialogTicketRejection(this, "The ticket was rejected", null, false);
		dialog.show();
	}

	public void success(View view) {

		// build notification
		Notification n = new Notification.Builder(this).setContentTitle("Upload Complete")
				.setContentText("Offline Data Successfully Uploaded.")
				.setSmallIcon(R.drawable.ic_launcher)
				.setAutoCancel(true).build();

		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		notificationManager.notify(0, n);
	}

	public void fail(View view) {
		// build notification
		Notification n = new Notification.Builder(this).setContentTitle("Upload Failed")
				.setContentText("Offline Data Failed To Upload.")
				.setSmallIcon(R.drawable.ic_launcher)
				.setAutoCancel(true).build();

		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		notificationManager.notify(0, n);
	}

}
