package com.moa.ticketscanner.debug;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import com.moa.ticketscanner.R;
import com.moa.ticketscanner.ui.MoaBaseActivity;

public class DebugCommunicationActivity extends MoaBaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_debug_communication);

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.debug_communication, menu);
		return true;
	}

	
	public void login(View view) {
		Intent intent = new Intent(this, DebugLoginRequestActivity.class);
		startActivity(intent);
	}
	
}
