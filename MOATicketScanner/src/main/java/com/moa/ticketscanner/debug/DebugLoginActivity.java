package com.moa.ticketscanner.debug;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.widget.EditText;

import com.moa.ticketscanner.R;
import com.moa.ticketscanner.settings.AppSettings;
import com.moa.ticketscanner.ui.MoaBaseActivity;

public class DebugLoginActivity extends MoaBaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_debug_login);

		// get passcode object
		EditText passcode = (EditText) findViewById(R.id.debug_login_passcode);

		// listen for change
		passcode.addTextChangedListener(new TextWatcher() {
			private String current = "";

			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				if (!s.toString().equals(current)) {
					if (s.toString().equals(AppSettings.debugCode)) {
						Intent intent = new Intent(getBaseContext(), DebugActivity.class);
						startActivity(intent);
						finish();
					}
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.debug_login, menu);
		return true;
	}

}
