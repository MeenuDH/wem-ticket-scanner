package com.moa.ticketscanner.debug;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import com.moa.ticketscanner.R;
import com.moa.ticketscanner.ui.MoaBaseActivity;

public class DebugActivity extends MoaBaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_debug);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.development, menu);
		return true;
	}

	public void debugCommunication(View view) {
		Intent intent = new Intent(this, DebugCommunicationActivity.class);
		startActivity(intent);
	}
	
	public void  robospice(View view) {
		Intent intent = new Intent(this, DebugRobosliceExampleActivity.class);
		startActivity(intent);
	}
	
	public void debugScan(View view) {
		Intent intent = new Intent(this, DebugScanActivity.class);
		startActivity(intent);
	}
	
	public void acceptRejectTest(View view) {
		Intent intent = new Intent(this, DebugTicketDialogTesterActivity.class);
		startActivity(intent);
	}
	
	public void shortcuts(View view) {
		Intent intent = new Intent(this, DebugShortcutsActivity.class);
		startActivity(intent);
	}
}
