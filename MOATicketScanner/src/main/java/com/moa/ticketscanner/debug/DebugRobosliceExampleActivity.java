package com.moa.ticketscanner.debug;



import com.moa.ticketscanner.ui.MoaBaseActivity;


public class DebugRobosliceExampleActivity extends MoaBaseActivity {

//    private static final String KEY_LAST_REQUEST_CACHE_KEY = "lastRequestCacheKey";
//
//    private ArrayAdapter<String> followersAdapter;
//	
//	 private String lastRequestCacheKey;
//
//	
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
//		setContentView(R.layout.activity_debug_roboslice_example);
//		
//		initUIComponents();
//	}
//
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.debug_communication, menu);
//		return true;
//	}
//	
//    @Override
//    protected void onStart() {
//        super.jsonSpiceManager.start(this);
//        super.onStart();
//    }
//
//    @Override
//    protected void onStop() {
//   	 super.jsonSpiceManager.shouldStop();
//        super.onStop();
//    }
//
//	
//	 private void initUIComponents() {
//	        Button searchButton = (Button) findViewById(R.id.search_button);
//	        final EditText searchQuery = (EditText) findViewById(R.id.search_field);
//	        ListView followersList = (ListView) findViewById(R.id.search_results);
//
//	        followersAdapter = new ArrayAdapter<String>(this,
//	            android.R.layout.simple_list_item_1, android.R.id.text1);
//	        followersList.setAdapter(followersAdapter);
//
//	        searchButton.setOnClickListener(new View.OnClickListener() {
//	            @Override
//	            public void onClick(View view) {
//	                performRequest(searchQuery.getText().toString());
//	                // clear focus
//	                LinearLayout linearLayout = (LinearLayout) findViewById(R.id.search_layout);
//	                linearLayout.requestFocus();
//	                // hide keyboard
//	                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//	                imm.hideSoftInputFromWindow(searchQuery.getWindowToken(), 0);
//	            }
//	        });
//	    }
//	 
//	 private void performRequest(String user) {
//	        this.setProgressBarIndeterminateVisibility(true);
//
//	        FollowersRequest request = new FollowersRequest(user);
//	        lastRequestCacheKey = request.createCacheKey();
//	        super.jsonSpiceManager.execute(request, lastRequestCacheKey,
//	            DurationInMillis.ONE_MINUTE, new ListFollowersRequestListener());
//	    }
//	 
//	 private class ListFollowersRequestListener implements RequestListener<FollowerList> {
//     
//     @Override
//     public void onRequestFailure(SpiceException e) {
//         DebugRobosliceExampleActivity.this.setProgressBarIndeterminateVisibility(false);
//         Toast.makeText(DebugRobosliceExampleActivity.this,
//                 "Error during request: " + e.getLocalizedMessage(), Toast.LENGTH_LONG)
//                 .show();
//     }
//
//     @Override
//     public void onRequestSuccess(FollowerList listFollowers) {
//
//         // listFollowers could be null just if contentManager.getFromCache(...)
//         // doesn't return anything.
//         if (listFollowers == null) {
//             return;
//         }
//
//         followersAdapter.clear();
//
//         for (Follower follower : listFollowers) {
//             followersAdapter.add(follower.getLogin());
//         }
//
//         followersAdapter.notifyDataSetChanged();
//
//         DebugRobosliceExampleActivity.this.setProgressBarIndeterminateVisibility(false);
//     }
//
//		
//
//
// }
	
}
