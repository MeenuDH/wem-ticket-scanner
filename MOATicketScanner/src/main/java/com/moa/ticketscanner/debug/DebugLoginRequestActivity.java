package com.moa.ticketscanner.debug;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.moa.ticketscanner.R;
import com.moa.ticketscanner.communication.GalaxyApiSpiceRequest;
import com.moa.ticketscanner.communication.MoaApiRequestDetails;
import com.moa.ticketscanner.communication.MoaApiSpiceRequest;
import com.moa.ticketscanner.dto.BaseGalaxyRequestDto;
import com.moa.ticketscanner.dto.HeaderDto;
import com.moa.ticketscanner.dto.logon.LogoffRequestDto;
import com.moa.ticketscanner.dto.logon.LogoffResponseDto;
import com.moa.ticketscanner.dto.logon.LogonRequestDto;
import com.moa.ticketscanner.dto.logon.LogonResponseDto;
import com.moa.ticketscanner.dto.logon.UserRoleDto;
import com.moa.ticketscanner.dto.tickets.ReverseUsageRequestDto;
import com.moa.ticketscanner.dto.tickets.ReverseUsageResponseDto;
import com.moa.ticketscanner.dto.tickets.TicketLookupRequestDto;
import com.moa.ticketscanner.dto.tickets.TicketLookupResponseDto;
import com.moa.ticketscanner.settings.AppSettings;
import com.moa.ticketscanner.ui.MoaBaseActivity;
import com.octo.android.robospice.exception.NetworkException;
import com.octo.android.robospice.exception.NoNetworkException;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.springframework.http.HttpMethod;

public class DebugLoginRequestActivity extends MoaBaseActivity {

	private TextView output;
	private String sessionId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_debug_login_request);
		output = (TextView) this.findViewById(R.id.debugLoginOutput);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.debug_login_request, menu);
		return true;
	}

	public void testLogoff(View view) {
		Log.d("WEM", "Starting Test");
		output.append("Starting Request...\n");

		// create header
		HeaderDto headerDto = new HeaderDto();
		headerDto.setMessageId(0);
		headerDto.setMessageType(LogoffRequestDto.MESSAGE_TYPE);
		headerDto.setSourceId("TestMessage");
		headerDto.setTimeStamp("2014-02-12 13:22:00");
		headerDto.setSessionID(sessionId);
		GalaxyApiSpiceRequest<LogoffResponseDto> request = new GalaxyApiSpiceRequest<LogoffResponseDto>(LogoffResponseDto.class, new BaseGalaxyRequestDto<LogoffRequestDto>(headerDto, null));
		
		getSpiceManager().execute(request, new LogoffRequestListener());
	}

	public void testLogon(View view) {
		Log.d("WEM", "Starting Test");
		output.append("Starting Request...\n");

		// create header
		HeaderDto headerDto = new HeaderDto();
		headerDto.setMessageId(0);
		headerDto.setMessageType(LogonRequestDto.MESSAGE_TYPE);
		headerDto.setSourceId("TestMessage");
		headerDto.setTimeStamp("2014-02-12 13:22:00");
		
		// create logonBody
		LogonRequestDto logonBody = new LogonRequestDto();
		logonBody.setFacility(1);
		logonBody.setOperation(10);
		logonBody.setUsername("2002347");
		logonBody.setPassword("Ost6789");

		GalaxyApiSpiceRequest<LogonResponseDto> request = new GalaxyApiSpiceRequest<LogonResponseDto>(LogonResponseDto.class, new BaseGalaxyRequestDto<LogonRequestDto>(headerDto, logonBody));
		getSpiceManager().execute(request, new LogonRequestListener());
	}
	
	public void testTicketLookup(View view) {
		Log.d("WEM", "Starting Ticket Lookup Test");
		output.append("Starting Request...\n");
		
		// create header
		HeaderDto headerDto = new HeaderDto();
		headerDto.setMessageId(1);
		headerDto.setMessageType(TicketLookupRequestDto.MESSAGE_TYPE);
		headerDto.setSourceId("TestMessage");
		headerDto.setTimeStamp("2014-02-12 13:22:00");
		headerDto.setSessionID(sessionId);
		
		// Create lookup
		TicketLookupRequestDto ticketLookupBodyDto = new TicketLookupRequestDto();
		ticketLookupBodyDto.setVisualID("5");
		
		// Create the Ticket Lookup Body
		GalaxyApiSpiceRequest<TicketLookupResponseDto> request = new GalaxyApiSpiceRequest<TicketLookupResponseDto>(TicketLookupResponseDto.class, new BaseGalaxyRequestDto<TicketLookupRequestDto>(headerDto, ticketLookupBodyDto));
		getSpiceManager().execute(request, new TicketLookupRequestListener());
	}
	
	public void testUsageReversal(View view) {
		Log.d("WEM", "Starting Usage Reversal Test");
		output.append("Starting Request...\n");
		
		// create header
		HeaderDto headerDto = new HeaderDto();
		headerDto.setMessageId(1);
		headerDto.setMessageType(ReverseUsageRequestDto.MESSAGE_TYPE);
		headerDto.setSourceId("TestMessage");
		headerDto.setTimeStamp("2014-02-12 13:22:00");
		headerDto.setSessionID(sessionId);
		
		// Create lookup
		ReverseUsageRequestDto reverseUsageBodyDto = new ReverseUsageRequestDto();
		reverseUsageBodyDto.setUsageID(102);
		
		// Create the Ticket Lookup Body
		GalaxyApiSpiceRequest<ReverseUsageResponseDto> request = new GalaxyApiSpiceRequest<ReverseUsageResponseDto>(ReverseUsageResponseDto.class, new BaseGalaxyRequestDto<ReverseUsageRequestDto>(headerDto, reverseUsageBodyDto));
		getSpiceManager().execute(request, new ReverseUsageRequestListener());
	}

	public void getUserRole(View view) {
		String uri = String.format(AppSettings.USER_ROLE_URI_FORMAT, "2002345");
		MoaApiRequestDetails requestDetails = new MoaApiRequestDetails();
		requestDetails.setHttpMethod(HttpMethod.GET);
		requestDetails.setUri(uri);
		MoaApiSpiceRequest<UserRoleDto> request = new MoaApiSpiceRequest<UserRoleDto>(UserRoleDto.class, requestDetails);
		getSpiceManager().execute(request, new UserRoleRequestListener());
	}

	private class LogonRequestListener implements RequestListener<LogonResponseDto> {

		@Override
		public void onRequestFailure(SpiceException spiceException) {
			Log.d("WEM", "Failed Logon Request");

			output.append("Logon Failed\n");
			output.append(spiceException.getMessage());
			output.append("\n");
		}

		@Override
		public void onRequestSuccess(LogonResponseDto result) {
			Log.d("WEM", "Success LogOn Request");
			
			sessionId = result.getHeader().getSessionID();
			
			output.append("Logon Succesful\n");
			output.append(result.toString());
			output.append("\n");
		}

	}
	
	private class LogoffRequestListener implements RequestListener<LogoffResponseDto> {

		@Override
		public void onRequestFailure(SpiceException spiceException) {
			Log.d("WEM", "Failed LogOff Request");

			output.append("Logoff Failed\n");
			output.append(spiceException.getMessage());
			output.append("\n");
		}

		@Override
		public void onRequestSuccess(LogoffResponseDto result) {
			Log.d("WEM", "Success LogOff Request");
			if(result.hasErrors()) {
				output.append("Error Logging Off" );
			}
			output.append("Logoff Succesful\n");
			output.append(result.toString());
			output.append("\n");
		}
	}

	private class UserRoleRequestListener implements RequestListener<UserRoleDto> {

		@Override
		public void onRequestFailure(SpiceException spiceException) {
			Log.d("WEM", "Failed Request");

			output.append("User Role Failed\n");
			output.append(spiceException.getMessage());
			output.append("\n");
		}

		@Override
		public void onRequestSuccess(UserRoleDto result) {
			Log.d("WEM", "Success User Role Request");
			
			output.append("User Role Succesful\n");
			output.append(result.getRole());
			output.append("\n");
		}
		
	}
	
	private class TicketLookupRequestListener implements RequestListener<TicketLookupResponseDto> {
		@Override
		public void onRequestFailure(SpiceException spiceException) {
			Log.d("WEM", "Failed Request");

			output.append("Ticket lookup Failed\n");
			output.append(spiceException.getMessage());
			output.append("\n");
		}

		@Override
		public void onRequestSuccess(TicketLookupResponseDto result) {
			Log.d("WEM", "Success Ticket Lookup Request");
			if(result.hasErrors()) {
				output.append("Error looking up ticket" );
			}
			output.append("Ticket lookup Successful\n");
			output.append(result.toString());
			output.append("\n");
		}
	}
	
	private class ReverseUsageRequestListener implements RequestListener<ReverseUsageResponseDto> {
		@Override
		public void onRequestFailure(SpiceException spiceException) {
			Log.d("WEM", "Failed Request");

			output.append("Reverse usage failed\n");
			output.append(spiceException.getMessage());
			output.append("\n");
			
			// User not connected
            if (spiceException instanceof NoNetworkException) {
                // Display not connected view

            }
            // Server unavailable
            else if (spiceException instanceof NetworkException) {
                // Display server unreachable view

            }

		}

		@Override
		public void onRequestSuccess(ReverseUsageResponseDto result) {
			Log.d("WEM", "Success Reverse Usage Request");
			if(result.hasErrors()) {
				String errorCode = result.getErrorCode();
				if(errorCode.equals(ReverseUsageResponseDto.ERRORCODE_ALREADYREVERSED)) {
					output.append("Usage already reversed\n");
				} else if(errorCode.equals(ReverseUsageResponseDto.ERRORCODE_INVALIDUSAGEID)) {
					output.append("Usage id is invalid\n");
				} else {
					output.append("Error reversing usage\n");
				}
			} else {
				output.append("Reverse usage succesful\n");
				output.append(result.toString());
				output.append("\n");
			}
		}
	}
}
