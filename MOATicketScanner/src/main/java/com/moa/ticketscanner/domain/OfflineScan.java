package com.moa.ticketscanner.domain;

import java.util.Date;

public class OfflineScan {
	private String scanId;
	private Date scanDate;
	private String scanOperator;
	private String visualId;
	
	public String getScanId() {
		return scanId;
	}
	public void setScanId(String scanId) {
		this.scanId = scanId;
	}
	public Date getScanDate() {
		return scanDate;
	}
	public void setScanDate(Date scanDate) {
		this.scanDate = scanDate;
	}
	public String getScanOperator() {
		return scanOperator;
	}
	public void setScanOperator(String scanOperator) {
		this.scanOperator = scanOperator;
	}
	public String getVisualId() {
		return visualId;
	}
	public void setVisualId(String visualId) {
		this.visualId = visualId;
	}
}
