package com.moa.ticketscanner.domain;

import com.moa.ticketscanner.R;

import java.util.HashMap;
import java.util.Map;

public class RideImageMapping {
	private static Map<Integer, Integer> rideImageMap = new HashMap<Integer, Integer>();
	static
	{
		rideImageMap.put(10,  R.drawable.ride_10);
		rideImageMap.put(11,  R.drawable.ride_11);
		rideImageMap.put(12,  R.drawable.ride_12);
		rideImageMap.put(13,  R.drawable.ride_13);
		rideImageMap.put(14,  R.drawable.ride_14);
		rideImageMap.put(15,  R.drawable.ride_15);
		rideImageMap.put(16,  R.drawable.ride_16);
		rideImageMap.put(17,  R.drawable.ride_17);
		rideImageMap.put(18,  R.drawable.ride_18);
		rideImageMap.put(19,  R.drawable.ride_19);
		rideImageMap.put(20,  R.drawable.ride_20);
		rideImageMap.put(21,  R.drawable.ride_21);
		rideImageMap.put(22,  R.drawable.ride_22);
		rideImageMap.put(23,  R.drawable.ride_23);
		rideImageMap.put(24,  R.drawable.ride_24);
		rideImageMap.put(25,  R.drawable.ride_25);
		rideImageMap.put(29,  R.drawable.ride_29);
		rideImageMap.put(30,  R.drawable.ride_30);
		rideImageMap.put(32,  R.drawable.ride_32);
		rideImageMap.put(36,  R.drawable.ride_36);
		rideImageMap.put(37,  R.drawable.ride_37);
		rideImageMap.put(38,  R.drawable.ride_38);
		rideImageMap.put(39,  R.drawable.ride_39);
		rideImageMap.put(40,  R.drawable.ride_40);
		rideImageMap.put(41,  R.drawable.ride_41);
		rideImageMap.put(42,  R.drawable.ride_42);
		rideImageMap.put(44,  R.drawable.ride_44);
		rideImageMap.put(45,  R.drawable.ride_45);
		rideImageMap.put(46,  R.drawable.ride_46);
		rideImageMap.put(47,  R.drawable.ride_47);
		rideImageMap.put(48,  R.drawable.ride_48);
		rideImageMap.put(49,  R.drawable.ride_49);
		rideImageMap.put(50,  R.drawable.ride_50);
		rideImageMap.put(51,  R.drawable.ride_51);
		rideImageMap.put(52,  R.drawable.ride_52);
		rideImageMap.put(53,  R.drawable.ride_53);
		rideImageMap.put(54,  R.drawable.ride_54);
		rideImageMap.put(55,  R.drawable.ride_55);
		rideImageMap.put(56,  R.drawable.ride_56);
		rideImageMap.put(57,  R.drawable.ride_57);
		rideImageMap.put(58,  R.drawable.ride_58);
		rideImageMap.put(59,  R.drawable.ride_59);
		rideImageMap.put(60,  R.drawable.ride_60);
		rideImageMap.put(61,  R.drawable.ride_61);
		rideImageMap.put(62,  R.drawable.ride_62);
		rideImageMap.put(63,  R.drawable.ride_63);
		rideImageMap.put(64,  R.drawable.ride_64);
		rideImageMap.put(65,  R.drawable.ride_65);
		rideImageMap.put(66,  R.drawable.ride_66);
		rideImageMap.put(67,  R.drawable.ride_67);
		rideImageMap.put(68,  R.drawable.ride_68);
		rideImageMap.put(69,  R.drawable.ride_69);
		rideImageMap.put(70,  R.drawable.ride_70);
		rideImageMap.put(71,  R.drawable.ride_71);
		rideImageMap.put(72,  R.drawable.ride_72);
		rideImageMap.put(73,  R.drawable.ride_73);
		rideImageMap.put(74,  R.drawable.ride_74);
		rideImageMap.put(75,  R.drawable.ride_75);
		rideImageMap.put(76,  R.drawable.ride_76);
		rideImageMap.put(77,  R.drawable.ride_77);
		rideImageMap.put(78,  R.drawable.ride_78);
		rideImageMap.put(79,  R.drawable.ride_79);
		rideImageMap.put(80,  R.drawable.ride_80);
		rideImageMap.put(81,  R.drawable.ride_81);
		rideImageMap.put(82,  R.drawable.ride_82);
		rideImageMap.put(83,  R.drawable.ride_83);
		rideImageMap.put(84,  R.drawable.ride_84);
		rideImageMap.put(85,  R.drawable.ride_85);
		rideImageMap.put(86,  R.drawable.ride_86);
		rideImageMap.put(87,  R.drawable.ride_87);
		rideImageMap.put(88,  R.drawable.ride_88);
		rideImageMap.put(89,  R.drawable.ride_89);
		rideImageMap.put(90,  R.drawable.ride_90);
		rideImageMap.put(91,  R.drawable.ride_91);
		rideImageMap.put(92,  R.drawable.ride_92);
		rideImageMap.put(93,  R.drawable.ride_93);
		rideImageMap.put(94,  R.drawable.ride_94);
		rideImageMap.put(95,  R.drawable.ride_95);
		rideImageMap.put(1,  R.drawable.ride_1);
		rideImageMap.put(2,  R.drawable.ride_2);
		rideImageMap.put(3,  R.drawable.ride_3);
		rideImageMap.put(4,  R.drawable.ride_4);
		rideImageMap.put(5,  R.drawable.ride_5);
		rideImageMap.put(6,  R.drawable.ride_6);
		rideImageMap.put(7,  R.drawable.ride_7);
		rideImageMap.put(8,  R.drawable.ride_8);
		rideImageMap.put(9,  R.drawable.ride_9);
	}
			
	
	public static Integer getRideImageId(int rideId) {
		if(!rideImageMap.containsKey(rideId)) {
			return null;
		}
		
		return rideImageMap.get(rideId);
	}
}
