package com.moa.ticketscanner.domain;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public enum UserRole {
	//TEAM_LEAD("sup", "team"),
	SUPERVISOR("sup"),
	ADMINISTRATOR("sa"),
	USER("rideop", "slidepat","ts","lifeguard");
	
	private List<String> roles;
	UserRole(String... roleParams) {
		roles = Arrays.asList(roleParams);
	}
	
	public static UserRole getByRoleValue(String roleValue) {
		for(UserRole userRole : UserRole.values()) {
			if(userRole.roles.contains(roleValue.toLowerCase(Locale.getDefault()))) {
				return userRole;
			}
		}
		
		return null;
	}
}
