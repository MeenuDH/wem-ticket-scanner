package com.moa.ticketscanner.domain;

import com.moa.ticketscanner.R;

public enum AcceptanceAudio {
	Valid("valid.wav", R.raw.valid),
	Points("points.wav", R.raw.points),
	APWin("apwin.wav", R.raw.apwin),
	Chaperon("chaperone.wav",R.raw.chaperone);

	private String audioName;
	private int audioResource;
	AcceptanceAudio(String audioName, int audioResource) {
		this.audioName = audioName;
		this.audioResource = audioResource;
	}
	
	public int getResourceId() {
		return audioResource;
	}
	
	public static AcceptanceAudio findByAudioName(String audioName) {
		for(AcceptanceAudio value : AcceptanceAudio.values()) {
			if(value.audioName.equalsIgnoreCase(audioName)) {
				return value;
			}
		}
		
		return null;
	}
}
