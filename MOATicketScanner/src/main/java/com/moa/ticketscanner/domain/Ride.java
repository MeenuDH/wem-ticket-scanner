package com.moa.ticketscanner.domain;

import android.graphics.Bitmap;

public class Ride {
	private String rideName;
	private Bitmap rideImage;
	private int facilityId;
	private int operationId;
	private int primaryAcpId;
	private int secondaryAcpId;
	private int tertiaryAcpId;
	//Vivek
	private int offlineAcpId;
	private String scanId;
	private boolean selected = false;
	private boolean selectable = true;
	private String rideRules;
	
	public Ride(String rideName, Bitmap rideImage, boolean isSelectable) {
		this.rideName = rideName;
		this.rideImage = rideImage;
		selectable = isSelectable;
	}
	
	public Ride(String rideName, Bitmap rideImage) {
		this(rideName, rideImage, true);
	}

	public boolean isSelectable() {
		return selectable;
	}
	
	public String getRideName() {
		return rideName;
	}

	public void setRideName(String rideName) {
		this.rideName = rideName;
	}

	public Bitmap getRideImage() {
		return rideImage;
	}

	public void setRideImage(Bitmap rideImage) {
		this.rideImage = rideImage;
	}

	public boolean getSelected() {
		return selected;
	}
	
	public void setSelected(boolean isSelected) {
		this.selected = isSelected;
	}

	public int getOperationId() {
		return operationId;
	}

	public void setOperationId(int operationId) {
		this.operationId = operationId;
	}

	public int getPrimaryAcpId() {
		return primaryAcpId;
	}

	public void setPrimaryAcpId(int acpId) {
		this.primaryAcpId = acpId;
	}

	public int getFacilityId() {
		return facilityId;
	}

	public void setFacilityId(int facilityId) {
		this.facilityId = facilityId;
	}

	public int getSecondaryAcpId() {
		return secondaryAcpId;
	}

	public void setSecondaryAcpId(int secondaryAcpId) {
		this.secondaryAcpId = secondaryAcpId;
	}

	public int getTertiaryAcpId() {
		return tertiaryAcpId;
	}

	public void setTertiaryAcpId(int tertiaryAcpId) {
		this.tertiaryAcpId = tertiaryAcpId;
	}
//Vivek
	public int getOfflineAcpId() {
	return offlineAcpId;
}

	public void setOfflineAcpId(int offlineAcpId) {
		this.offlineAcpId = offlineAcpId;
	}
	public String getScanId() {
		return scanId;
	}

	public void setScanId(String scanId) {
		this.scanId = scanId;
	}
	public void setRideRules(String rideRules) {
		this.rideRules = rideRules;
	}
	public String getRideRules() {
		return rideRules;
	}
}
